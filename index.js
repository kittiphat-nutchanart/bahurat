require('dotenv').config();

const Knex = require('knex');
const { Model } = require('objection');

const serverPromise = require('./server');
const knexConfigs = require('./knexfile.js');
const logger = require('./logger');

async function main() {
    // give the knex instance to objection
    const knex = Knex(knexConfigs.default);
    Model.knex(knex);

    process.on('unhandledRejection', (err) => {

        logger.fatal(err, 'unhandled rejection');
        process.exit(1);
    });

    const server = await serverPromise;
    await server.start();
    logger.info('Server running on %s', server.info.uri);
}

main();
