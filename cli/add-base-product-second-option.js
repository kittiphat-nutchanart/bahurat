const Joi = require('@hapi/joi');

const { BaseProductSecondOption } = require('../models');

exports.command = 'add-base-product-second-option';

exports.describe = 'add new base_product_second_option';

exports.builder = (yargs) => {
    yargs.example([
        [
            // '\\\n' -> '\\' is backslash , '\n' is newline
            '$0 add-base-product-second-option \\\n' +
                '--base_product_id=1 \\\n' +
                '--value=green'
        ]
    ]);
};

exports.handler = async (argv) => {
    const schema = Joi.object({

        base_product_id: Joi.number(),
        value: Joi.string().allow('')

    }).prefs({ presence: 'required', stripUnknown: true });

    const newOption = Joi.attempt(argv, schema);

    const option = await BaseProductSecondOption.query()
          .insertAndFetch(newOption);

    return option;
};
