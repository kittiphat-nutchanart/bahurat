const Joi = require('@hapi/joi');

const { User } = require('../models');
const UserService = require('../services/user');

exports.command = 'resend-confirmation-email <email>';

exports.describe = 'resend-confirmation-email';

exports.builder = (yargs) => {
    yargs.example([
        [
            '$0 resend-confirmation-email user@mail.com'
        ]
    ]);
};

exports.handler = async (argv) => {
    const schema = Joi.object({
        email: Joi.string().email(),
    }).prefs({ presence: 'required', stripUnknown: true });

    const validated_argv = Joi.attempt(argv, schema);

    const user = await User.query()
          .findOne({ email: validated_argv.email })
          .throwIfNotFound();
    await UserService.sendAccountConfirmationEmail(
        user.confirmation_token,
        user.email
    );

    return user;
};
