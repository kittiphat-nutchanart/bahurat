const assert = require('assert').strict;
const path = require('path');
const fs = require('fs');

const Joi = require('@hapi/joi');
const probeImageSize = require('probe-image-size');

const Storage = require('../storage');
const { BaseProduct, BaseProductColor } = require('../models');

exports.command = 'add-base-product-color';

exports.describe = 'add new base_product_color';

exports.builder = (yargs) => {
    yargs.example([
        [
            // '\\\n' -> '\\' is backslash , '\n' is newline
            '$0 add-base-product-color \\\n' +
                '--base_product_id=1 \\\n' +
                '--name=green \\\n' +
                '--hex=#123456 \\\n' +
                '--background=/path/to/local_bg.jpg'
        ]
    ]);
};

exports.handler = async (argv) => {
    const schema = Joi.object({

        base_product_id: Joi.number(),
        name: Joi.string().allow(''),
        hex: Joi.string().allow(''),

        background: Joi.string() // /path/to/local/background.jpg


    }).prefs({ presence: 'required', stripUnknown: true });

    const validated_argv = Joi.attempt(argv, schema);

    const file = fs.createReadStream(validated_argv.background);
    const img = await probeImageSize(file);

    const baseProduct = await BaseProduct.query()
          .findById(validated_argv.base_product_id);

    assert(baseProduct, 'base product does exist');
    assert.strictEqual(
        img.width,
        img.height,
        'bg\'s width equals bg\'s height'
    );
    assert.strictEqual(
        img.width,
        baseProduct.bg_px_side_length,
        'bg size matches with bg_px_side_length'
    );

    const filepath = validated_argv.background;
    const original_filename = path.basename(validated_argv.background);

    const uploadImg = await Storage.uploadImageFromLocal(
        filepath,
        original_filename,
        null,
        'products'
    );

    const color = await BaseProductColor.query()
          .insertAndFetch({
              base_product_id: validated_argv.base_product_id,
              name: validated_argv.name,
              hex: validated_argv.hex,
              background_id: uploadImg.id
          });

    return color;
};
