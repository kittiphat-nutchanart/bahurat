const tap = require('tap');
const Knex = require('knex');
const objection = require('objection');
const yargs = require('yargs');

const knexConfigs = require('../../knexfile.js');
const SampleRecords = require('../../models/sample.js');
const { BaseProduct } = require('../../models');
const cliModule = require('../add-base-product-second-option.js');
const TestHelper = require('../../services/testHelper');

async function main() {
    tap.test('new option is added to the db', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });

        await BaseProduct.query().insert({
            ...SampleRecords.BaseProduct,
            id: 1,
            name: 'product_1',
            can_choose_color: true,
            has_second_option: true,
            bg_px_side_length: 100,
            frame_percent_left: 0,
            frame_percent_top: 0,
            frame_percent_width: 0,
            frame_mm_width: 0,
            frame_mm_height: 0,
        });

        const parser = yargs.command({...cliModule, handler: () => {}}).help();
        const cmd = 'add-base-product-second-option ' +
              "--base_product_id=1 " +
              "--value='some option'";
        const argv = await parser.parse(cmd);

        // when
        const option = await cliModule.handler(argv);

        // then
        // output matches input
        t.equal(option.value, 'some option');
    });

}

main();
