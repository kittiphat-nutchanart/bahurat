const tap = require('tap');
const Knex = require('knex');
const objection = require('objection');
const yargs = require('yargs');

const knexConfigs = require('../../knexfile.js');
const cliModule = require('../add-base-product.js');
const TestHelper = require('../../services/testHelper');

async function main() {
    tap.test('new base_product is added to the db', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });

        const parser = yargs.command({...cliModule, handler: () => {}}).help();
        const cmd = 'add-base-product ' +
              '--name=product_1 ' +
              '--can_choose_color=false ' +
              '--has_second_option=false ' +
              '--bg_px_side_length=1000 ' +
              '--frame_percent_left=0 ' +
              '--frame_percent_top=0 ' +
              '--frame_percent_width=0 ' +
              '--frame_mm_width=0 ' +
              '--frame_mm_height=0';
        const argv = await parser.parse(cmd);

        // when
        const baseProduct = await cliModule.handler(argv);

        // then
        // output matches input
        t.equal(baseProduct.name, 'product_1');
        t.notOk(baseProduct.can_choose_color);
    });

}

main();
