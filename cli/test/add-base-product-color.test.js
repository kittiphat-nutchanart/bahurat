const path = require('path');

const tap = require('tap');
const Knex = require('knex');
const objection = require('objection');
const yargs = require('yargs');
const sinon = require('sinon');

const Storage = require('../../storage');
const knexConfigs = require('../../knexfile.js');
const SampleRecords = require('../../models/sample.js');
const { BaseProduct } = require('../../models');
const cliModule = require('../add-base-product-color.js');
const TestHelper = require('../../services/testHelper');

async function main() {
    tap.test('new color is added to the db', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });

        const sinonSandbox = sinon.createSandbox();

        const fake_uploadImg = { id: 1 };
        sinonSandbox.stub(Storage, 'uploadImageFromLocal')
            .returns(fake_uploadImg);

        t.teardown(() => {
            sinonSandbox.restore();
        });

        await BaseProduct.query().insert({
            ...SampleRecords.BaseProduct,
            id: 1,
            name: 'product_1',
            can_choose_color: true,
            has_second_option: true,
            bg_px_side_length: 1000,
            frame_percent_left: 0,
            frame_percent_top: 0,
            frame_percent_width: 0,
            frame_mm_width: 0,
            frame_mm_height: 0,
        });

        const parser = yargs.command({...cliModule, handler: () => {}}).help();
        const background = path.join(__dirname, './fixtures/bg-1000x1000.png');
        const cmd = 'add-base-product-color ' +
              '--base_product_id=1 ' +
              '--name=white ' +
              '--hex=#ffffff ' +
              '--background=' + background;
        const argv = await parser.parse(cmd);

        // when
        const color = await cliModule.handler(argv);

        // then
        // output matches input
        t.equal(color.name, 'white');
        t.equal(color.hex, '#ffffff');
        t.equal(color.background_id, fake_uploadImg.id);
    });

}

main();
