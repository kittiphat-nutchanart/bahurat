const child_process = require('child_process');
const util = require('util');
const exec = util.promisify(child_process.exec);

const Knex = require('knex');
const { nanoid } = require('nanoid');

const knexConfigs = require('../knexfile.js');


exports.command = 'export-db-schema';

exports.describe = 'export db schema';

exports.builder = (yargs) => {

    yargs.example([
        [
            '$0 export-db-schema',
            'will output schema to stdout'
        ],
        [
            '$0 export-db-schema | vim -',
            'pipe schema to vim'
        ]
    ]);
};

exports.handler = async (argv) => {
    // set up database
    const knexMaster = Knex(knexConfigs.pgTesting);
    const database = `test_${nanoid()}`;
    const knexOpts = {
        ...knexConfigs.pgTesting,
        connection: {
            ...knexConfigs.pgTesting.connection,
            database
        }
    };
    const knex = Knex(knexOpts);

    await knexMaster.raw(`create database "${database}"`);
    await knex.migrate.latest();

    // export db here
    const dumpCommand = `pg_dump -s "${database}"`;
    const { stdout, stderr } = await exec(dumpCommand);

    await knex.destroy();
    await knexMaster.raw(`drop database "${database}"`);
    await knexMaster.destroy();

    if (stderr) return stderr;
    if (stdout) return stdout;
};
