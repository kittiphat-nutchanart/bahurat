const Joi = require('@hapi/joi');

const { BaseProduct } = require('../models');

exports.command = 'add-base-product';

exports.describe = 'add new base_product';

exports.builder = (yargs) => {

    yargs.example([
        [
            // "\\\n" -> "\\" is backslash , "\n" is newline
            '$0 add-base-product \\\n' +
                '--name=product_1 \\\n' +
                '--can_choose_color=false \\\n' +
                '--has_second_option=false \\\n' +
                '--bg_px_side_length=0 \\\n' +
                '--frame_percent_left=0 \\\n' +
                '--frame_percent_top=0 \\\n' +
                '--frame_percent_width=0 \\\n' +
                '--frame_mm_width=0 \\\n' +
                '--frame_mm_height=0'
        ]
    ]);
};

exports.handler = async (argv) => {

    const schema = Joi.object({

        name: Joi.string(),

        can_choose_color: Joi.boolean(),
        has_second_option: Joi.boolean(),

        // bg must be 1000 px x 1000 px
        bg_px_side_length: Joi.number().valid(1000),
        frame_percent_left: Joi.number(),
        frame_percent_top: Joi.number(),
        frame_percent_width: Joi.number(),
        frame_mm_width: Joi.number(),
        frame_mm_height: Joi.number()

    }).prefs({ presence: 'required', stripUnknown: true });

    const newBaseProduct = Joi.attempt(argv, schema);

    const baseProduct = await BaseProduct.query().insert(newBaseProduct);

    return baseProduct;
};
