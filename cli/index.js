#!/usr/bin/env node
require('dotenv').config({path: require('path').join(__dirname, '../.env') });

const Knex = require('knex');
const objection = require('objection');

const knexConfigs = require('../knexfile.js');

// init knex and tell objection.js to use it
const knex = Knex(knexConfigs.default);
objection.Model.knex(knex);


require('yargs')
    .scriptName("cli")
    .usage('$0 <cmd> [args]')
    .command(require('./add-base-product.js'))
    .command(require('./add-base-product-color.js'))
    .command(require('./add-base-product-second-option.js'))
    .command(require('./export-db-schema.js'))
    .command(require('./resend-confirmation-email.js'))
    .onFinishCommand(async (result) => {
        console.log(result);
        await knex.destroy();
    })
    .demandCommand()
    .help()
    .argv;
