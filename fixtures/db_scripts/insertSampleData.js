require('dotenv').config({path: require('path').join(__dirname, '../../.env') });

const Knex = require('knex');
const objection = require('objection');

const {
    User,
    BaseProduct,
    Artwork,
    OverlaidProduct,
    Order
} = require('../../models');

const knexConfigs = require('../../knexfile.js');


async function insertData() {

    /**
     * login with
     * username: mail@email.com
     * password: password
     */
    await User.query().insertGraph({
        username: 'kangbomber',
        email: 'mail@email.com',
        i_am: 'admin',
        banner: {
            original_filename: 'awesome-banner.png',
            filesize: 999999,
            path: 'awesome-banner.png',
            width: 999,
            height: 999,
            type: 'png',
            mime: 'image/png',
        },
        bcrypt_hash: '$2a$12$51Fpo8bAij2MtdlF0SfUTuu12rZ3M0DBo54zsYISE2901BRyvRS4a',
        confirmation_token: '16fd2706-8baf-433b-82eb-8c7fada847da',
        confirmed_at: new Date().toISOString()
    });

    // for account activation test
    await User.query().insertGraph({
        username: 'unactivatedAccount',
        email: 'anotheruser@email.com',
        i_am: '',
        bcrypt_hash: '$2a$12$51Fpo8bAij2MtdlF0SfUTuu12rZ3M0DBo54zsYISE2901BRyvRS4a',
        confirmation_token: '6f501e2e-3323-4134-8f97-f87a0daadca0'
    });

    await BaseProduct.query().insertGraph({
        name: 'pink T-shirt',
        can_choose_color: true,
        has_second_option: true,
        bg_px_side_length: 1000,
        frame_percent_left: .324,
        frame_percent_top: .192,
        frame_percent_width: .352,
        frame_mm_width: 250,
        frame_mm_height: 300,
        enabled: true,

        base_product_colors: [
            {
                name: 'blue',
                hex: '#0000ff',
                enabled: true,
                is_default: true,
                background: {
                    original_filename: 'just-T-shirt-pink.jpg',
                    filesize: 999999,
                    path: 'just-T-shirt-pink.jpg',
                    width: 1000,
                    height: 1000,
                    type: 'jpg',
                    mime: 'image/jpeg',
                }
            }
        ],

        base_product_second_options: [
            {
                value: 'second_option',
                enabled: true,
            }
        ],

        base_product_variants: [
            {
                base_product_id: 1,
                base_product_color_id: 1,
                base_product_second_option_id: 1,
                base_cost: 10000,
                quantity: 999
            }
        ]
    });

    await Artwork.query().insertGraph({
        //id: 1,
        author_id: 1,
        title: 'artwork_title',
        image: {
            original_filename: 'artwork.jpg',
            filesize: 999999,
            path: 'artwork.png',
            width: 1989,
            height: 2459,
            type: 'png',
            mime: 'image/png',
        },
        visible: true
    });

    await OverlaidProduct.query().insert({
        //id: 1,
        base_product_id: 1,
        artwork_id: 1,
        author_id: 1,
        artwork_mm_width: 200,
        artwork_mm_top: 0,
        artwork_mm_left: 25,
        default_base_product_color_id: 1,
        profit: 0,
        visible: true
    });


    await Order.query().insertGraph({
        session_id: '16fd2706-8baf-433b-82eb-8c7fada847da',
        access_token: 'hello',
        checkout_complete: true,

        payment_method: 'credit_card',
        paid_at: new Date().toISOString(),

        shipping_full_name: 'Kittiphat Nutchanart',
        shipping_address: '53 m.8 Huakhao',
        shipping_district: 'DoembangNangbuat',
        shipping_province: 'Suphanburi',
        shipping_postal_code: '72120',
        shipping_country_code: 'th',

        phone: '095-6488767',
        email: 'hello@mail.com',

        line_total: 100,
        shipping_cost: 0,
        total: 100,
        lines: [
            {
                author_id: 1,
                author_unit_profit: 100,
                author_unit_tax: 100,
                author_total_profit: 100,
                author_total_tax: 100,
                author_total_earnings: 100,
                unit_price: 100,
                total_price: 100,
                base_product_id: 1,
                base_product_color_id: 1,
                base_product_second_option_id: 1,
                artwork_id: 1,
                artwork_mm_width: 100,
                artwork_mm_top: 100,
                artwork_mm_left: 100,
                quantity: 1,
            }
        ],
        logs: [
            {
                //user_id: 1,
                description: 'first_log'
            }
        ]
    });

}


async function main() {
    const knexMaster = Knex(knexConfigs.pgTesting);
    await knexMaster.raw(
        `drop database if exists "${knexConfigs.default.connection.database}"`
    );
    await knexMaster.raw(
        `create database "${knexConfigs.default.connection.database}"`
    );
    await knexMaster.destroy();

    // init knex and tell objection.js to use it
    const knex = Knex(knexConfigs.default);
    objection.Model.knex(knex);

    try {
        await knex.initialize();
        await knex.migrate.latest();
        await insertData();
    } catch(e) {
        console.error(e);
    } finally {
        await knex.destroy();
    }
}

if (require.main === module) {
    main();
}

module.exports = insertData;
