const tap = require('tap');
const Joi = require('@hapi/joi');
const Knex = require('knex');
const objection = require('objection');

const knexConfigs = require('../../../knexfile.js');
const insertSampleData = require('../insertSampleData.js');
const TestHelper = require('../../../services/testHelper');

async function main() {
    tap.test('can insert sample data', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });

        // when
        await insertSampleData();

        // then
    });

};

main();
