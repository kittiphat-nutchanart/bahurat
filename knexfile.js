const path = require('path');
const fs = require('fs');
const dotenv = require('dotenv');

const migrations = {
    tableName: 'knex_migrations',
    directory: path.join(__dirname, 'migrations')
};

const envPath = path.join(__dirname, '.env');
const envBuffer = fs.readFileSync(envPath);
const envConfig = dotenv.parse(envBuffer);

module.exports = {
    testing: {
        client: 'sqlite3',
        useNullAsDefault: true,
        connection: ':memory:',
        migrations,
        // debug: true
    },
    pgTesting: {
        client: 'postgresql',
        connection: {
            host: envConfig.PGHOST,
            user: 'bahurat_test',
            password: 'password',
            database: 'bahurat_test',
        },
        pool: {
            min: 2,
            max: 10
        },
        migrations
    },
    default: {
        client: 'postgresql',
        connection: {
            host: process.env.PGHOST,
            user: process.env.PGUSER,
            password: process.env.PGPASSWORD,
            database: process.env.PGDATABASE,
        },
        pool: {
            min: 2,
            max: 10
        },
        migrations
    }
};
