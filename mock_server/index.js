const os = require('os');

const express = require('express');
const path = require('path');
const multer  = require('multer');
const upload = multer({ dest: os.tmpdir() });
const app = express();
const port = 3000;

app.get('/api/overlaid_product/:id', (req, res) => {
    const fake_return = require("../routes/getPublicOverlaidProductItem/fake_return.js");
    res.json(fake_return());
});

app.get('/api/overlaid_products', (req, res) => {
    const fake_return = require("../routes/getPublicOverlaidProductList/fake_return.js");
    res.json(fake_return());
});

app.get('/api/me/artwork/:id/overlaid_products', (req, res) => {
    const fake_return = require("../routes/getMyOverlaidProductListByArtwork/fake_return.js");
    res.json(fake_return());
});

app.get('/api/user/:username', (req, res) => {
    const fake_return = require("../routes/getPublicUserProfileByUsername/fake_return.js");
    res.json(fake_return());
});

app.get('/api/artworks', (req, res) => {
    const fake_return = require("../routes/getPublicArtworkList/fake_return.js");
    res.json(fake_return());
});

app.get('/api/artwork/:id', (req, res) => {
    const fake_return = require("../routes/getPublicArtworkItem/fake_return.js");
    res.json(fake_return());
});

app.post('/api/artwork/:id/like', (req, res) => {
    res.sendStatus(200);
});

app.post('/api/artwork/:id/comment', (req, res) => {
    const fake_return = require("../routes/addCommentToArtwork/fake_return.js");
    res.json(fake_return());
});

app.get('/api/artwork/:id/comments', (req, res) => {
    const fake_return = require("../routes/getCommentsByArtwork/fake_return.js");
    res.json(fake_return());
});

app.get('/api/me/artworks', (req, res) => {
    const fake_return = require("../routes/getMyArtworkList/fake_return.js");
    res.json(fake_return());
});

app.get('/api/me/artwork/:id', (req, res) => {
    const fake_return = require("../routes/getMyArtworkItem/fake_return.js");
    res.json(fake_return());
});

app.get('/api/me/overlaid_product/:id', (req, res) => {
    const fake_return = require("../routes/getMyOverlaidProductItem/fake_return.js");
    res.json(fake_return());
});

app.get('/api/me/account', (req, res) => {
    const fake_return = require("../routes/getMyAccount/fake_return.js");
    res.json(fake_return());
});

app.get('/api/me/cart', (req, res) => {
    const FakeCartService = require('../services/cart/fake.js');
    res.json(FakeCartService.getCart());
});

app.get('/api/me/sold_items', (req, res) => {
    const fake_return = require("../routes/getMySoldItemList/fake_return.js");
    res.json(fake_return());
});

app.get('/api/me/seller_transactions', (req, res) => {
    const fake_return = require("../routes/getMySellerTransactionList/fake_return.js");
    res.json(fake_return());
});

app.get('/api/order/:id', (req, res) => {
    const fake_return = require("../routes/getPublicOrder/fake_return.js");
    res.json(fake_return());
});

app.get('/api/admin/base_products', (req, res) => {
    const fake_return = require("../routes/getAdminBaseProductList/fake_return.js");
    res.json(fake_return());
});

app.get('/api/admin/base_product/:id', (req, res) => {
    const fake_return = require("../routes/getAdminBaseProductItem/fake_return.js");
    res.json(fake_return());
});

app.get('/api/admin/order/:id', (req, res) => {
    const fake_return = require("../routes/getAdminOrder/fake_return.js");
    res.json(fake_return());
});

app.get('/api/admin/orders', (req, res) => {
    const fake_return = require("../routes/getAdminOrderList/fake_return.js");
    res.json(fake_return());
});

// '/api/printer/order/:token'

//********************************************************
// UPDATE/CREATE below
//********************************************************

app.post('/api/me/artwork', upload.single('file'), async (req, res) => {
    await new Promise(res => setTimeout(res, 500));
    res.status(200).json({
        artwork: {
            id: 1
        }
    });
});

app.patch('/api/me/account/banner', async (req, res) => {
    await new Promise(res => setTimeout(res, 500));
    res.sendStatus(200);
});

app.patch('/api/me/account/avatar', async (req, res) => {
    await new Promise(res => setTimeout(res, 500));
    res.sendStatus(200);
});

app.patch('/api/me/artwork/:id', async (req, res) => {
    await new Promise(res => setTimeout(res, 500));
    res.sendStatus(200);
});

app.delete('/api/me/artwork/:id', async (req, res) => {
    await new Promise(res => setTimeout(res, 500));
    res.sendStatus(200);
});

app.patch('/api/me/overlaid_product/:id', async (req, res) => {
    await new Promise(res => setTimeout(res, 500));
    res.sendStatus(200);
});

app.patch('/api/me/account', async (req, res) => {
    await new Promise(res => setTimeout(res, 500));
    res.sendStatus(200);
});

app.post('/api/me/cart/line', async (req, res) => {
    await new Promise(res => setTimeout(res, 500));
    res.sendStatus(200);
});

app.patch('/api/me/cart/line/:id', async (req, res) => {
    await new Promise(res => setTimeout(res, 500));
    const FakeCartService = require('../services/cart/fake.js');
    res.json(FakeCartService.getCart());
});

app.delete('/api/me/cart/line/:id', async (req, res) => {
    await new Promise(res => setTimeout(res, 500));
    const FakeCartService = require('../services/cart/fake.js');
    res.json(FakeCartService.getCart());
});

app.patch('/api/admin/base_product/:id', async (req, res) => {
    await new Promise(res => setTimeout(res, 500));
    res.sendStatus(200);
});

app.patch('/api/admin/base_product_color/:id', async (req, res) => {
    await new Promise(res => setTimeout(res, 500));
    res.sendStatus(200);
});

app.patch('/api/admin/base_product_second_option/:id', async (req, res) => {
    await new Promise(res => setTimeout(res, 500));
    res.sendStatus(200);
});

app.patch('/api/admin/base_product_variant/:id', async (req, res) => {
    await new Promise(res => setTimeout(res, 500));
    res.sendStatus(200);
});

// - create checkout
app.post('/api/checkout', async (req, res) => {
    const FakeCartService = require('../services/cart/fake.js');
    res.json(FakeCartService.getCheckout());
});

// - update address/payment checkout
app.patch('/api/checkout/:id', async (req, res) => {
    const FakeCartService = require('../services/cart/fake.js');
    await new Promise(res => setTimeout(res, 500));
    res.json(FakeCartService.getCheckout());
});

// - complete
app.post('/api/checkout/:id/complete', async (req, res) => {
    res.sendStatus(200);
});


//********************************************************
// authentication below
//********************************************************

app.post('/api/login', async (req, res) => {
    res.sendStatus(200);
});

app.post('/api/logout', async (req, res) => {
    res.sendStatus(200);
});

app.post('/api/user/signup', async (req, res) => {
    res.sendStatus(200);
});

app.post('/api/user/forgot_password', async (req, res) => {
    res.sendStatus(200);
});

app.post('/api/user/reset_password', async (req, res) => {
    res.sendStatus(200);
});

app.use('/static', express.static(path.join(__dirname, 'public')));

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`);
});
