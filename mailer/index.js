const fs = require('fs');
const path = require('path');
const Handlebars = require('handlebars');
const htmlToText = require('html-to-text');
const nodemailer = require('nodemailer');

// create reusable transporter object using the default SMTP transport
const transporter = nodemailer.createTransport({
    host: process.env.EMAIL_SERVER,
    port: process.env.EMAIL_PORT,
    secure: false, // true for 465, false for other ports
    auth: {
        user: process.env.EMAIL_USER, // generated ethereal user
        pass: process.env.EMAIL_PASS, // generated ethereal password
    },
});

module.exports = class Mailer {
    static transporter = transporter;

    static async prepareTemplate(templateName, data) {
        const templatePath = path.resolve(
            __dirname,
            'templates/' + templateName + '.html'
        );
        const content = await fs.promises.readFile(templatePath, { encoding: 'utf8' });

        // use handlebars to render the email template
        // handlebars allows more complex templates with conditionals and nested objects, etc.
        // this way we have much more options to customize the templates based on given data
        const template = Handlebars.compile(content);
        const html = template(data);

        // generate a plain-text version of the same email
        const text = htmlToText.fromString(html);

        return {
            html,
            text
        };
    }

    /**
     * send email out
     * @param {sring} templateName - name of template in templates folder e.g. 'sample'
     * @param {Object} data - data to fill in template e.g. { 'name': 'hello' }
     * @param {string} to - recipient e.g. 'kittiphat <kang@mail.com>'
     * @param {string} subject - email's subject e.g. 'email confirmation'
     */
    static async send(templateName, data, to, subject) {
        const { html, text } = await this.prepareTemplate(templateName, data);
        const mailOptions = {
            to,
            from: process.env.EMAIL_FROM_ADDRESS,
            subject,
            html,
            text
        };

        try {
            await this.transporter.sendMail(mailOptions);
        } catch (err) {
            console.error(err);
        }
    };
};
