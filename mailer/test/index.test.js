const tap = require('tap');
const sinon = require('sinon');

const Mailer = require('..');

async function main() {

    tap.test('send() works as expected', async (t) => {
        // given
        const sinonSandbox = sinon.createSandbox();
        t.teardown(async () => {
            sinonSandbox.restore();
        });

        sinonSandbox.stub(process, 'env')
            .value({
                EMAIL_FROM_ADDRESS: 'sender@mail.com'
            });
        const stub = sinonSandbox.stub(Mailer.transporter, 'sendMail');

        const template = 'example';
        const data = {
            name: 'Somsak'
        };
        const to = 'Somsak Jengsmart <sjengsmart@email.com>';
        const subject = 'secret spicey fried chicken recipe';

        // when
        await Mailer.send(template, data, to, subject);

        const mailOpts = stub.firstCall.args[0];


        //then
        t.equal(mailOpts.from, 'sender@mail.com');
        t.equal(mailOpts.to, to);
        t.equal(mailOpts.subject, subject);
        t.ok(mailOpts.text.includes(data.name));

    });

}


main();
