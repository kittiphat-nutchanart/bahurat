require('dotenv').config({path: require('path').join(__dirname, '../.env') });
const Mailer = require('.');

async function main() {
    const template = 'account-confirmation';
    const data = {
        url: 'https://www.wadlen.co/confirm?token=eidkdie20949=23'
    };
    const to = 'firstname lastname <mail@email.com>';
    const subject = template;

    await Mailer.send(template, data, to, subject);
}


main().catch(console.error);
