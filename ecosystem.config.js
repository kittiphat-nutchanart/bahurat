module.exports = {
    apps : [
        {
            name: 'bahurat-frontend',
            exec_mode: 'cluster',
            instances: 'max', // Or a number of instances
            script: './node_modules/nuxt/bin/nuxt.js',
            args: 'start'
        },
        {
            name: 'bahurat-api',
            script: 'index.js'
        }
    ],
};
