const { User } = require('../models');

module.exports = class {
    /**
     * @param {Object} user - fake user
     * @param {number} user.id - id for the fake user
     * @param {string[]} scope - user's scope e.g. ['admin']
     */
    static mockCredentials(user, scope = []) {
        return {
            user: { id: user.id },
            scope
        };
    }

    static async sessionValidator(request, session) {
        const res = { valid: false };
        const user = await User.query().findById(session.user.id);

        const curLogoutTime = user.logout_all_at
              && new Date(user.logout_all_at).getTime();
        const sessLogoutTime = session.user.logout_all_at
              && new Date(session.user.logout_all_at).getTime();

        const curLogoutTimeIsNil = !curLogoutTime;
        const logoutTimesMatch = curLogoutTime
              && sessLogoutTime
              && curLogoutTime === sessLogoutTime;
        if (curLogoutTimeIsNil || logoutTimesMatch) {
            res.valid = true;
        }

        return res;
    }
};
