const Joi = require('@hapi/joi');

module.exports = Joi.object({
    user: Joi.object({
        id: Joi.number()
    }),
    scope: Joi.array().items(Joi.string())
});
