const tap = require('tap');
const Joi = require('@hapi/joi');
const objection = require('objection');
const Iron = require('@hapi/iron');

const Auth = require('../index.js');
const schema = require('../credentials_schema.js');
const Knex = require('knex');
const { CartLine, User } = require('../../models');
const SampleRecords = require('../../models/sample.js');
const knexConfigs = require('../../knexfile.js');
const serverPromise = require('../../server');
const TestHelper = require('../../services/testHelper');

async function main() {
    const server = await serverPromise;

    tap.test('mockCredentials() obeys the schema', async (t) => {
        // given
        const fakeUser = { id: 1 };

        // when
        const mockCredentials = Auth.mockCredentials(fakeUser);

        // then
        Joi.assert(
            mockCredentials,
            schema,
            'obeys the schema',
            { presence: 'required' }
        );
    });

    tap.test('sessionValidator() works', async (t) => {
        t.test('validation passes', async (t) => {
            // given
            const teardown = await TestHelper.setupDB();
            t.teardown(async () => {
                await teardown();
            });

            const user = await User.query().insert({
                ...SampleRecords.User,
                id: 1,
            });

            const session = await Iron.seal(
                { user, scope: [] },
                '11111111111111111111111111111111',
                Iron.defaults
            );

            // when
            const res = await server.inject({
                method: 'GET',
                url: '/api/me/artworks?page=1&sort=new',
                headers: {
                    Cookie: `sid=${session}`
                }
            });

            //then
            t.equal(res.statusCode, 200);
        });

        t.test('validation fails b/c logout all', async (t) => {
            // given
            const teardown = await TestHelper.setupDB();
            t.teardown(async () => {
                await teardown();
            });

            const user = await User.query().insert({
                ...SampleRecords.User
            });

            await User.query().findById(user.id).patch({
                logout_all_at: new Date().toISOString()
            });

            const session = await Iron.seal(
                { user, scope: [] },
                '11111111111111111111111111111111',
                Iron.defaults
            );

            // when
            const res = await server.inject({
                method: 'GET',
                url: '/api/me/artworks?page=1&sort=new',
                headers: {
                    Cookie: `sid=${session}`
                }
            });

            //then
            t.equal(res.statusCode, 401);
        });
    });
}

main();
