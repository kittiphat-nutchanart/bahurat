const Hapi = require('@hapi/hapi');

const logger = require('../logger');
const Auth = require('../authentication');

// adding routes
// "require(...)" routes early to prevent unexpected bahavior
const routes = [
    require('../routes/signup/route.js'),
    require('../routes/login/route.js'),
    require('../routes/logout/route.js'),
    require('../routes/forgotPassword/route.js'),
    require('../routes/activateAccount/route.js'),
    require('../routes/resetPassword/route.js'),

    require('../routes/getAdminOrderList/route.js'),
    require('../routes/getAdminOrder/route.js'),
    require('../routes/getAdminBaseProductList/route.js'),
    require('../routes/getAdminBaseProductItem/route.js'),
    require('../routes/updateAdminBaseProduct/route.js'),
    require('../routes/updateAdminBaseProductColor/route.js'),
    require('../routes/updateAdminBaseProductSecondOption/route.js'),
    require('../routes/updateAdminBaseProductVariant/route.js'),
    require('../routes/updateAdminOrder/route.js'),

    require('../routes/getMySoldItemList/route.js'),
    require('../routes/getMySellerTransactionList/route.js'),
    require('../routes/getMyAccount/route.js'),
    require('../routes/updateMyAccount/route.js'),
    require('../routes/uploadArtwork/route.js'),
    require('../routes/uploadMyBanner/route.js'),
    require('../routes/uploadAvatar/route.js'),
    require('../routes/getMyArtworkItem/route.js'),
    require('../routes/getMyArtworkList/route.js'),
    require('../routes/updateMyArtwork/route.js'),
    require('../routes/deleteMyArtwork/route.js'),
    require('../routes/getMyOverlaidProductItem/route.js'),
    require('../routes/getMyOverlaidProductListByArtwork/route.js'),
    require('../routes/getMyCart/route.js'),
    require('../routes/addItemToMyCart/route.js'),
    require('../routes/updateItemInMyCart/route.js'),
    require('../routes/deleteItemInMyCart/route.js'),
    require('../routes/getPublicOverlaidProductItem/route.js'),
    require('../routes/getPublicOverlaidProductList/route.js'),
    require('../routes/getPublicUserProfileByUsername/route.js'),
    require('../routes/getPublicArtworkList/route.js'),
    require('../routes/getPublicArtworkItem/route.js'),
    require('../routes/likeArtwork/route.js'),
    require('../routes/addCommentToArtwork/route.js'),
    require('../routes/getCommentsByArtwork/route.js'),

    require('../routes/createCheckout/route.js'),
    require('../routes/updateCheckout/route.js'),
    require('../routes/completeCheckout/route.js'),
    require('../routes/getPublicOrder/route.js'),

    require('../routes/updateMyOverlaidProduct/route.js'),
];


const server = Hapi.server({
    port: 3000,
    host: 'localhost'
    //debug: process.env.NODE_ENV === 'production' ? false : { request: ['error'] }
});

server.events.on('request', (request, event, tags) => {
    // according to https://hapi.dev/api?v=20.2.0#-serverevents
    // this when event.channel === 'error' the error should be 500 error
    if (event.channel === 'error') {
        logger.error(event.error, 'uncaught error');
    }
});

module.exports = (async () => {

    // authentication set-up
    await server.register(require('@hapi/cookie'));

    await server.register({
        plugin: require('@hapi/yar'),
        options: {
            cookieOptions: {
                password: process.env.NODE_ENV === 'production' ? process.env.COOKIE_PASSWORD : '11111111111111111111111111111111',
                isSecure: process.env.NODE_ENV === 'production',
                ttl: 365 * 24 * 60 * 60 * 1000,
                path: '/'
            }
        }
    });

    server.auth.strategy('session', 'cookie', {
        cookie: {
            name: 'sid',
            password: process.env.NODE_ENV === 'production' ? process.env.COOKIE_PASSWORD : '11111111111111111111111111111111',
            isSecure: process.env.NODE_ENV === 'production',
            ttl: 365 * 24 * 60 * 60 * 1000,
            path: '/'
        },
        validateFunc: Auth.sessionValidator
    });

    routes.forEach(route => server.route(route));

    //await server.initialize();
    return server;
})();
