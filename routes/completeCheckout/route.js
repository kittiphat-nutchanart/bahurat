const Joi = require('@hapi/joi');
const Boom = require('@hapi/boom');

const { CartLine, Order } = require('../../models');
const CartService = require('../../services/cart');
const Payment = require('../../payment');

module.exports = {
    method: 'POST',
    path: '/api/checkout/{id}/complete',
    options: {
        validate: {
            params: Joi.object({
                id: Joi.number().integer().min(1)
            })
        }
    },
    handler: async (request, h) => {
        const orderId = request.params.id;

        const order = await Order.query()
              .findById(orderId)
              .throwIfNotFound();

        if (request.yar.id !== order.session_id) {
            throw Boom.unauthorized();
        }

        if (
            !order.can_complete_checkout
                || order.checkout_complete
        ) {
            throw Boom.badRequest();
        }

        order.checkout_complete = true;

        await Order.query()
              .update(order)
              .where({
                  id: orderId,
                  checkout_complete: false // prevent double checkout
              })
              .throwIfNotFound(); // throw if no update

        await CartLine.query()
            .delete()
            .where({ session_id: request.yar.id });

        if (order.payment_method === 'credit_card') {
            await CartService.imposeFeesOnAuthors(orderId);

            let charge;
            try {
                charge = await Payment.charge(
                    order.payment_gateway,
                    order.payment_card_id,
                    order.total
                );
            } catch (err) {
                // fix
                // notify error to user that the card can't be charged
                throw err;
            }

            await order.$query().patch({
                paid_at: new Date().toISOString(),
                payment_charge_data: JSON.stringify(charge)
            });
        }

        await CartService.sendOrderConfirmationEmail(orderId);

        return h.continue;
    }
};
