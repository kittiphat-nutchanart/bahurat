const Boom = require('@hapi/boom');
const { nanoid } = require('nanoid');

const { CartLine, Order } = require('../../models');
const CartService = require('../../services/cart');

module.exports = {
    method: 'POST',
    path: '/api/checkout',
    options: {
    },
    handler: async (request, h) => {

        const session_id = request.yar.id;

        const cartLines = await CartLine.query()
              .where({
                  session_id
              })
              .withGraphFetched(`[
                  overlaid_product.[
                      base_product,
                      artwork
                  ],
                  base_product_color,
                  base_product_second_option,
                  base_product_variant,
              ]`);

        const cartNotEmpty = cartLines.length > 0;
        const allLinesInStock = cartLines.every((line) => {
            return line.base_product_variant.quantity > 0;
        });
        const canCheckout = cartNotEmpty && allLinesInStock;
        if (!canCheckout) {
            const error = Boom.badRequest('not ready to checkout');
            //error.output.payload.error = 'custom';
            throw error;
        }

        const lines = cartLines.map((line) => {
            const author_id = line.overlaid_product.author_id;
            const author_unit_profit = line.overlaid_product.profit;
            const author_unit_tax = Math.floor(author_unit_profit * CartService.VAT);
            const author_total_profit = author_unit_profit * line.quantity;
            const author_total_tax = author_unit_tax * line.quantity;
            const author_total_earnings = author_total_profit - author_total_tax;
            const unit_price =
                  line.overlaid_product.profit + line.base_product_variant.base_cost;
            const total_price = unit_price * line.quantity;
            const base_product_id = line.base_product_id;
            const base_product_color_id = line.base_product_color_id;
            const base_product_second_option_id = line.base_product_second_option_id;
            const artwork_id = line.overlaid_product.artwork_id;
            const artwork_mm_width = line.overlaid_product.artwork_mm_width;
            const artwork_mm_top = line.overlaid_product.artwork_mm_top;
            const artwork_mm_left = line.overlaid_product.artwork_mm_left;
            const quantity = line.quantity;

            return {
                author_id,
                author_unit_profit,
                author_unit_tax,
                author_total_profit,
                author_total_tax,
                author_total_earnings,
                unit_price,
                total_price,
                base_product_id,
                base_product_color_id,
                base_product_second_option_id,
                artwork_id,
                artwork_mm_width,
                artwork_mm_top,
                artwork_mm_left,
                quantity
            };
        });

        const line_total = lines.reduce((acc, line) => {
            return acc + line.total_price;
        }, 0);
        const total = line_total;

        const newOrder = {
            session_id,
            access_token: nanoid(),
            line_total,
            total,
            lines
        };

        const order = await Order.transaction(async (trx) => {
            const order = await Order.query(trx).insertGraph(newOrder);
            return order;
        });

        const checkout = await CartService.getCheckout(order.id);
        return checkout;
    }
};
