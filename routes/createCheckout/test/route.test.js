const tap = require('tap');
const Knex = require('knex');
const objection = require('objection');
const Iron = require('@hapi/iron');

const serverPromise = require('../../../server');
const knexConfigs = require('../../../knexfile.js');
const SampleRecords = require('../../../models/sample.js');
const {
    User,
    BaseProduct,
    BaseProductColor,
    BaseProductSecondOption,
    Artwork,
    OverlaidProduct,
    BaseProductVariant,
    CartLine,
    UploadImg
} = require('../../../models');
const TestHelper = require('../../../services/testHelper');

async function main() {
    const server = await serverPromise;

    tap.test('200 ok', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });

        await User.query().insert({
            ...SampleRecords.User,
            id: 1,
            username: 'kangbomber',
            email: 'hello@mail.com',
        });

        await BaseProduct.query().insert({
            ...SampleRecords.BaseProduct,
            id: 1,
            name: 'product_1',
            can_choose_color: true,
            has_second_option: true,
            bg_px_side_length: 0,
            frame_percent_left: 0,
            frame_percent_top: 0,
            frame_percent_width: 0,
            frame_mm_width: 0,
            frame_mm_height: 0,
        });

        await UploadImg.query().insert({
            ...SampleRecords.UploadImg,
            id: 1,
            original_filename: 'hello.jpg',
            filesize: 999999,
            path: 'path/to/img',
            width: 1000,
            height: 1000,
            type: 'jpg',
            mime: 'image/jpeg',
            owner_id: 1
        });
        await BaseProductColor.query().insert({
            ...SampleRecords.BaseProductColor,
            id: 1,
            base_product_id: 1,
            name: 'product_1_color_1',
            hex: '#111',
            background_id: 1
        });

        await BaseProductSecondOption.query().insert({
            ...SampleRecords.BaseProductSecondOption,
            id: 1,
            base_product_id: 1,
            value: 'product_1_option_1',
        });

        await BaseProductVariant.query().insert({
            ...SampleRecords.BaseProductVariant,
            base_product_id: 1,
            base_product_color_id: 1,
            base_product_second_option_id: 1,
            base_cost: 10000,
            quantity: 999
        });

        await Artwork.query().insert({
            ...SampleRecords.Artwork,
            id: 1,
            author_id: 1,
            title: 'artwork_1',
            image_id: 1
        });

        await OverlaidProduct.query().insert({
            ...SampleRecords.OverlaidProduct,
            id: 1,
            base_product_id: 1,
            artwork_id: 1,
            author_id: 1,
            artwork_mm_width: 0,
            artwork_mm_top: 0,
            artwork_mm_left: 0,
            default_base_product_color_id: 1,
            profit: 10000,
        });

        await CartLine.query().insert({
            ...SampleRecords.CartLine,
            session_id: '123e4567-e89b-12d3-a456-426614174000',
            overlaid_product_id: 1,
            base_product_id: 1,
            base_product_color_id: 1,
            base_product_second_option_id: 1,
            quantity: 1
        });

        const session = await Iron.seal(
            { id: '123e4567-e89b-12d3-a456-426614174000', _store: {} },
            '11111111111111111111111111111111',
            Iron.defaults
        );

        // when
        const res = await server.inject({
            method: 'POST',
            url: '/api/checkout',
            headers: {
                Cookie: `session=${session}`
            }
        });


        // then
        t.equal(res.statusCode, 200, 'gives 200 ok');
    });

    tap.test('400 cart not ready', async (t) => {
        // cart not ready by setting baseProductVariant.quantity = 0

        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });

        await BaseProduct.query().insert({
            ...SampleRecords.BaseProduct,
            id: 1,
            name: 'product_1',
            can_choose_color: true,
            has_second_option: true,
            bg_px_side_length: 0,
            frame_percent_left: 0,
            frame_percent_top: 0,
            frame_percent_width: 0,
            frame_mm_width: 0,
            frame_mm_height: 0,
        });

        await BaseProductColor.query().insert({
            ...SampleRecords.BaseProductColor,
            id: 1,
            base_product_id: 1,
            name: 'product_1_color_1',
            hex: '#111',
            background_id: 1
        });

        await BaseProductSecondOption.query().insert({
            ...SampleRecords.BaseProductSecondOption,
            id: 1,
            base_product_id: 1,
            value: 'product_1_option_1',
        });

        await BaseProductVariant.query().insert({
            ...SampleRecords.BaseProductVariant,
            base_product_id: 1,
            base_product_color_id: 1,
            base_product_second_option_id: 1,
            base_cost: 10000,
            quantity: 0
        });

        await CartLine.query().insert({
            ...SampleRecords.CartLine,
            session_id: '123e4567-e89b-12d3-a456-426614174000',
            overlaid_product_id: 1,
            base_product_id: 1,
            base_product_color_id: 1,
            base_product_second_option_id: 1,
            quantity: 1
        });

        const session = await Iron.seal(
            { id: 'abcdef', _store: {} },
            '11111111111111111111111111111111',
            Iron.defaults
        );

        // when
        const res = await server.inject({
            method: 'POST',
            url: '/api/checkout',
            headers: {
                Cookie: `session=${session}`
            }
        });


        // then
        t.equal(res.statusCode, 400, 'gives 400');
    });

}


main();
