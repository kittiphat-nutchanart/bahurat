const Joi = require('@hapi/joi');

// need to modify
// this is just a copy-paste
module.exports = Joi.object({

    checkout: Joi.object({

        id: Joi.number(),
        line_total: Joi.number(),
        shipping_cost: Joi.number(),
        total: Joi.number(),

        lines: Joi.array().items(
            Joi.object({

                base_product: Joi.object({
                    name: Joi.string()
                }),
                base_product_color: Joi.object({
                    name: Joi.string()
                }),
                base_product_second_option: Joi.object({
                    value: Joi.string()
                }),
                artwork: Joi.object({
                    title: Joi.string()
                }),

                unit_price: Joi.number(),
                total_price: Joi.number(),
                quantity: Joi.number(),

                // computed
                variant_name: Joi.string(),

                // computed
                overlaid_product_img_url: Joi.string()
            })
        )
    })

});
