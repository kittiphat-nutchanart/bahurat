// Load and instantiate Chance
const chance = require('chance').Chance();


module.exports = () => {
    const user_profile = {
        id: chance.integer(),
        username: chance.word(),
        about: chance.paragraph(),
        _banner_url: 'https://picsum.photos/800/300',
        avatar_url: 'https://picsum.photos/200',
        created_at: chance.timestamp()
    };

    return {
        user_profile
    };
};
