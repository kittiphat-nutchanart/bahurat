const Joi = require('@hapi/joi');

module.exports = Joi.object({
    user_profile: Joi.object({
        id: Joi.number(),
        username: Joi.string(),
        about: Joi.string().allow(''),

        _banner_url: Joi.string().allow(''), // computed
        avatar_url: Joi.string().allow(''), // computed

        created_at: Joi.date()
    })
});
