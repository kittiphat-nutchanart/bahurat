const tap = require('tap');
const Joi = require('@hapi/joi');
const Knex = require('knex');
const objection = require('objection');

const serverPromise = require('../../../server');
const schema = require("./../schema.js");
const knexConfigs = require('../../../knexfile.js');
const SampleRecords = require('../../../models/sample.js');
const {
    User,
} = require('../../../models');
const TestHelper = require('../../../services/testHelper');

async function main() {
    const server = await serverPromise;

    tap.test('route responds correctly', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });

        await User.query().insert({
            ...SampleRecords.User,
            id: 37,
            username: 'kangbomber',
            email: 'hello@mail.com',
        });


        // when
        const res = await server.inject({
            method: 'GET',
            url: '/api/user/kangbomber',
        });


        // then
        t.equal(res.statusCode, 200, 'returns 200 ok');
        Joi.assert(res.result, schema, 'the response obeys the schema', {presence: "required"});
        t.equal(res.result.user_profile.username, 'kangbomber', 'username matched');
    });


}

main();
