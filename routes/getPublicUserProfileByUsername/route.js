const Joi = require('@hapi/joi');

const { User } = require('../../models');
const schema = require('./schema.js');
const Storage = require('../../storage');


module.exports = {
    method: 'GET',
    path: '/api/user/{username}',
    options: {
        validate: {
            params: Joi.object({
                username: Joi.string()
            })
        }
    },
    handler: async (request, h) => {
        const user = await User.query()
              .findOne({
                  username: request.params.username
              })
              .throwIfNotFound()
              .withGraphFetched('[banner, avatar]');

        user._banner_url = user.banner ?
            Storage.getImgURL(user.banner.path, {
                maxWidth: Storage.DimenL,
                maxHeight: Storage.DimenL
            }) : '';

        user.avatar_url = user.avatar ?
            Storage.getImgURL(user.avatar.path, {
                maxWidth: Storage.DimenS,
                maxHeight: Storage.DimenS
            })
            : Storage.getLetterAvatarURL(user.username, 120);

        return Joi.attempt({ user_profile: user }, schema, {stripUnknown: true, presence: 'required'});
    }
};
