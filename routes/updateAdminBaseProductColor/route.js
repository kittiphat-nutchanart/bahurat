const Joi = require('@hapi/joi');

const ProductService = require('../../services/product');
const { BaseProductColor } = require('../../models');

module.exports = {
    method: 'PATCH',
    path: '/api/admin/base_product_color/{id}',
    options: {
        auth: {
            strategy: 'session',
            access: {
                scope: 'admin'
            }
        },
        validate: {
            params: Joi.object({
                id: Joi.number().integer().min(1)
            }),
            payload: Joi.object({
                enabled: Joi.boolean(),
                is_default: Joi.boolean()
            }).min(1)
        }
    },
    handler: async (request, h) => {

        await BaseProductColor.transaction(async trx => {
            const color = await BaseProductColor.query(trx)
                  .patchAndFetchById(request.params.id, request.payload)
                  .throwIfNotFound();

            // un-default the previous default
            if (request.payload.is_default) {
                await BaseProductColor.query(trx)
                    .patch({ is_default: false })
                    .where({ base_product_id: color.base_product_id })
                    .andWhereNot({ id: color.id });
            }

            await ProductService.assertBaseProductHasValidState(
                color.base_product_id,
                trx
            );
        });

        return h.continue;
    }
};
