const tap = require('tap');
const Knex = require('knex');
const objection = require('objection');

const serverPromise = require('../../../server');
const knexConfigs = require('../../../knexfile.js');
const SampleRecords = require('../../../models/sample.js');
const {
    BaseProduct,
    BaseProductColor,
    BaseProductSecondOption,
    UploadImg
} = require('../../../models');
const Auth = require('../../../authentication');
const TestHelper = require('../../../services/testHelper');

async function main() {
    const server = await serverPromise;

    tap.test('ok 200', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });

        await BaseProduct.query().insert({
            ...SampleRecords.BaseProduct,
            id: 1,
            name: 'product_1',
            can_choose_color: true,
            has_second_option: false,
            bg_px_side_length: 0,
            frame_percent_left: 0,
            frame_percent_top: 0,
            frame_percent_width: 0,

            frame_mm_width: 0,
            frame_mm_height: 0,
            enabled: true
        });

        await UploadImg.query().insert({
            ...SampleRecords.UploadImg,
            id: 1,
            original_filename: 'hello.jpg',
            filesize: 999999,
            path: 'path/to/img',
            width: 1000,
            height: 1000,
            type: 'jpg',
            mime: 'image/jpeg',
            owner_id: 1
        });
        await BaseProductColor.query().insert({
            ...SampleRecords.BaseProductColor,
            id: 1,
            base_product_id: 1,
            name: 'blue',
            hex: '#0000ff',
            background_id: 1,
            enabled: false,
            is_default: false
        });

        await UploadImg.query().insert({
            ...SampleRecords.UploadImg,
            id: 2,
            original_filename: 'hello.jpg',
            filesize: 999999,
            path: 'path/to/img',
            width: 1000,
            height: 1000,
            type: 'jpg',
            mime: 'image/jpeg',
            owner_id: 1
        });
        await BaseProductColor.query().insert({
            ...SampleRecords.BaseProductColor,
            id: 2,
            base_product_id: 1,
            name: 'blue',
            hex: '#0000ff',
            background_id: 2,
            enabled: false,
            is_default: true
        });

        await BaseProductSecondOption.query().insert({
            ...SampleRecords.BaseProductSecondOption,
            base_product_id: 1,
            value: 'second_option',
            enabled: true
        });

        // when
        const res = await server.inject({
            method: 'PATCH',
            url: '/api/admin/base_product_color/1',
            auth: {
                strategy: 'session',
                credentials: Auth.mockCredentials({ id: 1}, ['admin'])
            },
            payload: {
                enabled: true,
                is_default: true
            }
        });

        const color1 = await BaseProductColor.query().findById(1);
        const color2 = await BaseProductColor.query().findById(2);

        //then
        t.equal(res.statusCode, 204, 'returns 204 ok');

        t.ok(color1.enabled, 'enabled is set to true');

        t.notEqual(color2.is_default, undefined);
        t.notOk(color2.is_default, 'other\'s "is_default" status is set to false');
    });

    tap.test('invalid state occurs. missing default color', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });

        await BaseProduct.query().insert({
            ...SampleRecords.BaseProduct,
            id: 1,
            name: 'product_1',
            can_choose_color: true,
            has_second_option: false,
            bg_px_side_length: 0,
            frame_percent_left: 0,
            frame_percent_top: 0,
            frame_percent_width: 0,

            frame_mm_width: 0,
            frame_mm_height: 0,
            enabled: true
        });

        await UploadImg.query().insert({
            ...SampleRecords.UploadImg,
            id: 1,
            original_filename: 'hello.jpg',
            filesize: 999999,
            path: 'path/to/img',
            width: 1000,
            height: 1000,
            type: 'jpg',
            mime: 'image/jpeg',
            owner_id: 1
        });
        await BaseProductColor.query().insert({
            ...SampleRecords.BaseProductColor,
            base_product_id: 1,
            name: 'blue',
            hex: '#0000ff',
            background_id: 1,
            enabled: false,
            is_default: false
        });

        // when
        const res = await server.inject({
            method: 'PATCH',
            url: '/api/admin/base_product_color/1',
            auth: {
                strategy: 'session',
                credentials: Auth.mockCredentials({ id: 1}, ['admin'])
            },
            payload: {
                enabled: true
            }
        });

        //then
        t.ok(res.statusCode >= 400);
    });
}


main();
