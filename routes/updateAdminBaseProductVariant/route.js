const Joi = require('@hapi/joi');

const { BaseProductVariant } = require('../../models');

module.exports = {
    method: 'PATCH',
    path: '/api/admin/base_product_variant/{base_product_id}_{base_product_color_id}_{base_product_second_option_id}',
    options: {
        auth: {
            strategy: 'session',
            access: {
                scope: 'admin'
            }
        },
        validate: {
            params: Joi.object({
                base_product_id: Joi.number().integer().min(1),
                base_product_color_id: Joi.number().integer().min(1),
                base_product_second_option_id: Joi.number().integer().min(1)
            }).prefs({ presence: 'required' }),
            payload: Joi.object({
                quantity: Joi.number().integer().min(0).max(999),
                base_cost: Joi.number().integer().min(0),
            }).min(1)
        }
    },
    handler: async (request, h) => {
        const rowsUpdated = await BaseProductVariant.query()
            .patch(request.payload)
            .where(request.params);

        const updateHappens = rowsUpdated > 0;
        if (!updateHappens) {
            await BaseProductVariant.query().insert({
                ...request.params,
                ...request.payload
            });
        }

        return h.continue;
    }
};
