const tap = require('tap');
const Knex = require('knex');
const objection = require('objection');

const serverPromise = require('../../../server');

const knexConfigs = require('../../../knexfile.js');

const {
    BaseProductVariant
} = require('../../../models');
const Auth = require('../../../authentication');
const TestHelper = require('../../../services/testHelper');

async function main() {
    const server = await serverPromise;

    tap.test('ok 200', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });


        // when
        const res = await server.inject({
            method: 'PATCH',
            url: '/api/admin/base_product_variant/1_1_1',
            auth: {
                strategy: 'session',
                credentials: Auth.mockCredentials({ id: 1}, ['admin'])
            },
            payload: {
                quantity: 10
            }
        });

        const baseProductVariant = await BaseProductVariant.query().first();

        //then
        t.equal(res.statusCode, 204, 'returns 204 ok');
        t.equal(baseProductVariant.quantity, 10, 'quantity is set to 10');
    });
}


main();
