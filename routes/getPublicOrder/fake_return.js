// Load and instantiate Chance
const chance = require('chance').Chance();
const R = require('ramda');

module.exports = () => {
    const makeOrderItem = () => {
        const width = chance.integer({min: 50, max: 250});
        const height = chance.integer({min: 50, max: 250});
        return {
            id: chance.integer(),
            quantity: chance.integer({min: 1, max: 10}),
            unit_price: chance.integer({min: 5000, max: 50000}),

            _overlaid_product_img_url: `https://picsum.photos/${width}/${height}?random=${chance.integer()}`,
            _artwork_title: chance.sentence({words: 3}),
            _base_product_name: chance.sentence({words: 2}),
            _base_product_variant_name: chance.word()
        };
    };
    const lines = R.map(makeOrderItem, R.range(1, chance.integer({min: 2, max: 20})));
    return {
        order: {
            id: chance.integer({min: 0}),
            created_at: chance.timestamp(),
            line_total: chance.integer({min: 10000, max: 200000}),
            shipping_cost: chance.integer({min: 0, max: 10000}),
            total: chance.integer({min: 10000, max: 200000}),
            payment_method: 'credit_card',
            paid_at: chance.bool() ? chance.timestamp() : null,
            fulfillment_status: chance.pickone(['unfulfilled', 'fulfilled']),

            shipping_full_name: chance.name(),
            shipping_address: chance.address(),
            shipping_district: chance.city(),
            shipping_province: chance.city(),
            shipping_postal_code: chance.zip(),
            phone: chance.phone(),
            email: chance.email(),

            lines
        }
    };
};
