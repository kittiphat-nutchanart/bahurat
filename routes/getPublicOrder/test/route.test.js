const tap = require('tap');
const Joi = require('@hapi/joi');
const Knex = require('knex');
const objection = require('objection');

const serverPromise = require('../../../server');
const schema = require("./../schema.js");
const knexConfigs = require('../../../knexfile.js');
const SampleRecords = require('../../../models/sample.js');
const {
    BaseProduct,
    BaseProductColor,
    BaseProductSecondOption,
    Artwork,
    Order,
    OrderLine,
    UploadImg
} = require('../../../models');
const TestHelper = require('../../../services/testHelper');

async function main() {
    const server = await serverPromise;

    tap.test('can get a specific order for admin', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });


        await BaseProduct.query().insert({
            ...SampleRecords.BaseProduct,
            id: 1
        });

        await BaseProductColor.query().insert({
            ...SampleRecords.BaseProductColor,
            id: 1,
            background_id: (await UploadImg.query().insert({
                ...SampleRecords.UploadImg,
            }).then(i => i.id))
        });

        await BaseProductSecondOption.query().insert({
            ...SampleRecords.BaseProductSecondOption,
            id: 1
        });

        await Artwork.query().insert({
            ...SampleRecords.Artwork,
            id: 1,
            author_id: 1,
            title: 'artwork_title',
            image_id: (await UploadImg.query().insert({
                ...SampleRecords.UploadImg,
            }).then(i => i.id))
        });

        await Order.query().insert({
            ...SampleRecords.Order,
            id: 1,
            payment_method: 'credit_card',
            access_token: 'xyz1234'
        });

        await OrderLine.query().insert({
            ...SampleRecords.OrderLine,
            order_id: 1,
            base_product_id: 1,
            base_product_color_id: 1,
            base_product_second_option_id: 1,
            artwork_id: 1,
        });

        // when
        const res = await server.inject({
            method: 'GET',
            url: '/api/order/1?token=xyz1234',
        });

        // then
        t.equal(res.statusCode, 200);
        Joi.assert(res.result, schema, '', { presence: "required" });
    });
}


main();
