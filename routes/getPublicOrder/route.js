const Joi = require('@hapi/joi');
const Boom = require('@hapi/boom');

const Storage = require('../../storage');
const { Order } = require('../../models');
const ProductService = require('../../services/product');
const { project } = require('../../models/projection.js');

module.exports = {
    method: 'GET',
    path: '/api/order/{id}',
    options: {
        validate: {
            params: Joi.object({
                id: Joi.number().integer().min(1)
            }),
            query: Joi.object({
                token: Joi.string().required()
            })
        }
    },
    handler: async (request, h) => {

        const order = await Order
              .query()
              .findById(request.params.id)
              .withGraphFetched(`[
                  lines.[
                      artwork.[image],
                      base_product,
                      base_product_color.[background],
                      base_product_second_option
                  ]
              ]`)
              .throwIfNotFound();

        if (order.access_token !== request.query.token) {
            throw Boom.unauthorized();
        }

        order.lines.forEach((line) => {
            line._artwork_title = line.artwork.title;
            line._base_product_name = line.base_product.name;
            line._base_product_variant_name = ProductService
                  .computeVariantName(
                      line.base_product_color.name,
                      line.base_product_second_option.value
                  );

            line._overlaid_product_img_url = Storage.getOPImgURLFromOrderLine(
                line,
                { size: 200 }
            );
        });

        const schema = {
            order: {
                id: 1,
                created_at: 1,

                line_total: 1,
                shipping_cost: 1,
                total: 1,

                payment_method: 1,
                paid_at: 1,
                fulfillment_status: 1,

                shipping_full_name: 1,
                shipping_address: 1,
                shipping_district: 1,
                shipping_province: 1,
                shipping_postal_code: 1,

                phone: 1,
                email: 1,

                lines: {
                    id: 1,
                    unit_price: 1,
                    quantity: 1,

                    _artwork_title: 1,
                    _base_product_name: 1,
                    _base_product_variant_name: 1,
                    _overlaid_product_img_url: 1
                }
            }
        };

        return project({ order }, schema);
    }
};
