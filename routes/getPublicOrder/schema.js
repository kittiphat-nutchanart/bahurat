const Joi = require('@hapi/joi');


module.exports = Joi.object({
    order: Joi.object({
        id: Joi.number(),
        created_at: Joi.date(),

        line_total: Joi.number(),
        shipping_cost: Joi.number(),
        total: Joi.number(),

        paid_at: Joi.date().allow(null),
        payment_method: Joi.string(),
        fulfillment_status: Joi.string(),

        shipping_full_name: Joi.string().allow(''),
        shipping_address: Joi.string().allow(''),
        shipping_district: Joi.string().allow(''),
        shipping_province: Joi.string().allow(''),
        shipping_postal_code: Joi.string().allow(''),

        phone: Joi.string().allow(''),
        email: Joi.string().allow(''),


        lines: Joi.array().items(
            Joi.object({
                id: Joi.number(),
                unit_price: Joi.number(),
                quantity: Joi.number(),

                _overlaid_product_img_url: Joi.string(),
                _artwork_title: Joi.string(),
                _base_product_name: Joi.string(),
                _base_product_variant_name: Joi.string()
            })
        )
    })
});

