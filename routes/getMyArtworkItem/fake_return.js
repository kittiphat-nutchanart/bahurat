// Load and instantiate Chance
const chance = require('chance').Chance();
const R = require('ramda');

module.exports = () => {
    const tags = chance.bool() ?
          R.range(1, chance.integer({ min: 2, max: 6 }))
          .map(() => chance.word())
          : [];

    return {
        artwork: {
            id: chance.integer(),
            title: chance.sentence({words: 3}),
            description: chance.paragraph(),
            tags,
            blocked: chance.bool(),
            visible: chance.bool(),
            _url: "https://picsum.photos/200",
            author: {
                username: chance.word()
            },
            created_at: chance.date()
        }
    };
};
