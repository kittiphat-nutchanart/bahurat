const Joi = require('@hapi/joi');

module.exports = Joi.object({
    artwork: Joi.object({
        id: Joi.number(),
        title: Joi.string(),
        description: Joi.string().allow(''),
        tags: Joi.array().items(
            Joi.string()
        ),
        blocked: Joi.boolean().allow(1, 0),
        visible: Joi.boolean().allow(1, 0),
        _url: Joi.string(),
        author: Joi.object({
            username: Joi.string()
        }),
        created_at: Joi.date()
    })
});
