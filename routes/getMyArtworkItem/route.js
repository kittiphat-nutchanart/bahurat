const Joi = require('@hapi/joi');
const Boom = require('@hapi/boom');

const Storage = require('../../storage');
const { Artwork } = require('../../models');
const { project } = require('../../models/projection');

module.exports = {
    method: 'GET',
    path: '/api/me/artwork/{id}',
    options: {
        auth: 'session',
        validate: {
            params: Joi.object({
                id: Joi.number().integer().min(1)
            })
        }
    },
    handler: async (request, h) => {
        const artwork = await Artwork.query()
              .findById(request.params.id)
              .authorVisible()
              .withGraphFetched('[author, image, tags]')
              .throwIfNotFound();

        if (artwork.author_id !== request.auth.credentials.user.id)
            throw Boom.forbidden();

        artwork._url = Storage.getImgURL(artwork.path, {
            maxWidth: Storage.DimenS,
            maxHeight: Storage.DimenS
        });

        artwork.tags = artwork.tags.map(i => i.name);

        const schema = {
            id: 1,
            title: 1,
            description: 1,
            tags: 1,
            blocked: 1,
            visible: 1,
            _url: 1,
            author: (i) => project(i.author, {
                username: 1
            }),
            created_at: 1
        };
        const _artwork = project(artwork, schema);

        return { artwork: _artwork };
    }
};
