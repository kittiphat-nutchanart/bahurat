const tap = require('tap');
const Joi = require('@hapi/joi');
const Knex = require('knex');
const objection = require('objection');

const serverPromise = require('../../../server');
const schema = require("./../schema.js");
const knexConfigs = require('../../../knexfile.js');
const SampleRecords = require('../../../models/sample.js');
const {
    User,
    Artwork,
    UploadImg,
    Tag,
    ArtworkTag
} = require('../../../models');
const Auth = require('../../../authentication');
const TestHelper = require('../../../services/testHelper');

async function main() {
    const server = await serverPromise;

    tap.test('responds correctly 200', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });

        const user = await User.query().insert({
            ...SampleRecords.User,
            username: 'kangbomber',
            email: 'hello@mail.com',
            bank_account: '1234',
            about: '',
            i_am: ''
        });

        const artwork = await Artwork.query().insert({
            ...SampleRecords.Artwork,
            author_id: user.id,
            title: 'artwork_1',
            image_id: (await UploadImg.query().insert({
                ...SampleRecords.UploadImg
            }).then(i => i.id))
        });

        const tag = await Tag.query().insert({
            ...SampleRecords.Tag
        });

        await ArtworkTag.query().insert({
            artwork_id: artwork.id,
            tag_id: tag.id
        });

        // when
        const res = await server.inject({
            method: 'GET',
            url: `/api/me/artwork/${artwork.id}`,
            auth: {
                strategy: 'session',
                credentials: Auth.mockCredentials({ id: 1})
            }
        });

        //then
        t.equal(res.statusCode, 200, 'returns 200 equal');
        t.equal(
            res.result.artwork.author.username,
            user.username,
            'username in res mathches the given/inserted one'
        );
        Joi.assert(
            res.result, schema,
            'the res obeys the schema',
            { presence: 'required' }
        );
    });

    tap.test('responds 401 when not logged-in', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });


        // when
        const res = await server.inject({
            method: 'GET',
            url: '/api/me/artwork/1'
        });

        //then
        t.equal(res.statusCode, 401, 'returns 401 status');
    });

    tap.test('gives 403 when try to access other\'s artwork instead of yours', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });

        const artwork = await Artwork.query().insert({
            ...SampleRecords.Artwork,
            author_id: 5,
            title: 'artwork_1',
            image_id: 1
        });

        // when
        const res = await server.inject({
            method: 'GET',
            url: `/api/me/artwork/${artwork.id}`,
            auth: {
                strategy: 'session',
                credentials: Auth.mockCredentials({ id: 1})
            }
        });

        //then
        t.equal(res.statusCode, 403, 'returns 403 status');
    });

}


main();
