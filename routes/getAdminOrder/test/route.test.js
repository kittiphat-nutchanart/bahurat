const tap = require('tap');
const Joi = require('@hapi/joi');
const Knex = require('knex');
const objection = require('objection');
const sinon = require('sinon');

const Auth = require('../../../authentication');
const serverPromise = require('../../../server');
const schema = require("./../schema.js");
const knexConfigs = require('../../../knexfile.js');
const SampleRecords = require('../../../models/sample.js');
const Storage = require('../../../storage');
const {
    BaseProduct,
    BaseProductColor,
    BaseProductSecondOption,
    Artwork,
    Order,
    OrderLine,
    OrderLog,
    UploadImg
} = require('../../../models');
const TestHelper = require('../../../services/testHelper');

async function main() {
    // init knex and tell objection.js to use it
    const knex = Knex(knexConfigs.testing);
    objection.Model.knex(knex);
    const server = await serverPromise;

    tap.test('can get a specific order for admin', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });

        const sinonSandbox = sinon.createSandbox();

        // this is need b/c the actual call will
        // keep the process running before exit for a while
        // which we could make it faster by stubing the method
        sinonSandbox.stub(Storage.s3, 'getSignedUrl')
              .returns('http://fakesigned.com/123');

        await BaseProduct.query().insert({
            ...SampleRecords.BaseProduct,
            id: 1,
            name: 'product_1',
            can_choose_color: true,
            has_second_option: true,
            bg_px_side_length: 0,
            frame_percent_left: 0,
            frame_percent_top: 0,
            frame_percent_width: 0,
            frame_mm_width: 0,
            frame_mm_height: 0,
        });

        await BaseProductColor.query().insert({
            ...SampleRecords.BaseProductColor,
            id: 1,
            base_product_id: 1,
            name: 'blue',
            hex: '#0000ff',
            background_id: 1
        });

        await BaseProductSecondOption.query().insert({
            ...SampleRecords.BaseProductSecondOption,
            id: 1,
            base_product_id: 1,
            value: 'second_option',
        });

        await Artwork.query().insert({
            ...SampleRecords.Artwork,
            id: 1,
            author_id: 1,
            title: 'artwork_title',
            image_id: (await UploadImg.query().insert({
                ...SampleRecords.UploadImg,
            }).then(i => i.id))
        });

        await Order.query().insert({
            ...SampleRecords.Order,
            id: 1,
            session_id: '16fd2706-8baf-433b-82eb-8c7fada847da',
            payment_method: 'credit_card',
            email: 'hello@mail.com',
            line_total: 100,
            shipping_cost: 0,
            total: 100
        });

        await OrderLine.query().insert({
            ...SampleRecords.OrderLine,
            author_id: 1,
            author_unit_profit: 100,
            author_unit_tax: 100,
            author_total_profit: 100,
            author_total_tax: 100,
            author_total_earnings: 100,
            unit_price: 100,
            total_price: 100,
            base_product_id: 1,
            base_product_color_id: 1,
            base_product_second_option_id: 1,
            artwork_id: 1,
            artwork_mm_width: 100,
            artwork_mm_top: 100,
            artwork_mm_left: 100,
            quantity: 1,
            order_id: 1
        });

        await OrderLog.query().insert({
            ...SampleRecords.OrderLog,
            order_id: 1,
            //user_id: 1,
            description: 'first_log'
        });

        // when
        const res = await server.inject({
            method: 'GET',
            url: '/api/admin/order/1',
            auth: {
                strategy: 'session',
                credentials: Auth.mockCredentials({ id: 1}, ['admin'])
            }
        });

        // then
        t.equal(res.statusCode, 200, 'returns 200 ok');
        Joi.assert(res.result, schema, 'the res obeys the schema', { presence: "required" });
    });

}


main();
