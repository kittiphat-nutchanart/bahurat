const Joi = require('@hapi/joi');

const Storage = require('../../storage');
const { Order } = require('../../models');
const ProductService = require('../../services/product');
const { project } = require('../../models/projection.js');

module.exports = {
    method: 'GET',
    path: '/api/admin/order/{id}',
    options: {
        auth: {
            strategy: 'session',
            access: {
                scope: 'admin'
            }
        },
        validate: {
            params: Joi.object({
                id: Joi.number().integer().min(1)
            })
        }
    },
    handler: async (request, h) => {

        const order = await Order
              .query()
              .findById(request.params.id)
              .withGraphFetched(`[
                  lines.[
                      artwork.[image],
                      base_product,
                      base_product_color.[background],
                      base_product_second_option
                  ],
                  logs.user
              ]`)
              .throwIfNotFound();

        order.lines.forEach((line) => {
            line._variant_name = ProductService
                  .computeVariantName(
                      line.base_product_color.name,
                      line.base_product_second_option.value
                  );

            line._overlaid_product_img_url = Storage.getOPImgURLFromOrderLine(
                line,
                { size: 200 }
            );

            const signOpts = {
                Bucket: Storage.S3_BUCKET,
                Key: line.artwork.image.path,
                Expires: 60 * 60 * 24 * 7 // 7 days in seconds
            };
            line.artwork._url = Storage.s3.getSignedUrl(
                'getObject',
                signOpts
            );
        });

        const schema = {
            order: {
                id: 1,

                order_status: 1,
                payment_method: 1,
                payment_status: 1,
                fulfillment_status: 1,

                shipping_full_name: 1,
                shipping_address: 1,
                shipping_district: 1,
                shipping_province: 1,
                shipping_postal_code: 1,

                phone: 1,
                email: 1,

                line_total: 1,
                shipping_cost: 1,
                total: 1,

                created_at: 1,

                logs: {
                    description: 1,
                    user: {
                        username: 1
                    },
                    created_at: 1,
                },
                lines: {
                    id: 1,

                    unit_price: 1,
                    total_price: 1,

                    base_product: {
                        name: 1
                    },
                    base_product_color: {
                        name: 1
                    },
                    base_product_second_option: {
                        value: 1
                    },
                    artwork: {
                        title: 1,

                        // computed
                        _url: 1
                    },
                    artwork_mm_width: 1,
                    artwork_mm_left: 1,
                    artwork_mm_top: 1,
                    quantity: 1,
                    returns: 1,

                    // computed
                    _variant_name: 1,

                    // computed
                    _overlaid_product_img_url: 1
                }
            }
        };

        return project({ order }, schema);
    }
};
