const Joi = require('@hapi/joi');

module.exports = Joi.object({
    order: Joi.object({
        id: Joi.number(),

        order_status: Joi.string(),
        payment_method: Joi.string(),
        payment_status: Joi.string(),
        fulfillment_status: Joi.string(),

        shipping_full_name: Joi.string().allow(''),
        shipping_address: Joi.string().allow(''),
        shipping_district: Joi.string().allow(''),
        shipping_province: Joi.string().allow(''),
        shipping_postal_code: Joi.string().allow(''),

        phone: Joi.string().allow(''),
        email: Joi.string(),

        line_total: Joi.number(),
        shipping_cost: Joi.number(),
        total: Joi.number(),

        created_at: Joi.date(),

        logs: Joi.array().items(
            Joi.object({
                description: Joi.string(),
                user: Joi.object({
                    username: Joi.string()
                }).allow(null),
                created_at: Joi.date()
            })
        ),
        lines: Joi.array().items(
            Joi.object({
                id: Joi.number(),

                unit_price: Joi.number(),
                total_price: Joi.number(),

                base_product: Joi.object({
                    name: Joi.string()
                }),
                base_product_color: Joi.object({
                    name: Joi.string()
                }),
                base_product_second_option: Joi.object({
                    value: Joi.string()
                }),
                artwork: Joi.object({
                    title: Joi.string(),

                    // computed
                    _url: Joi.string()
                }),
                artwork_mm_width: Joi.number(),
                artwork_mm_left: Joi.number(),
                artwork_mm_top: Joi.number(),
                quantity: Joi.number(),
                returns: Joi.number(),

                // computed
                _variant_name: Joi.string(),

                // computed
                _overlaid_product_img_url: Joi.string()
            })
        )
    })
});
