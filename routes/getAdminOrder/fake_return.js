// Load and instantiate Chance
const chance = require('chance').Chance();
const R = require('ramda');


module.exports = () => {

    const makeLog = () => {
        return {
            description: chance.sentence({ words: 5 }),
            user: {
                username: chance.word()
            },
            created_at: chance.date()
        };
    };
    const logs = chance.bool({ likelihood: 50 })
        ? R.map(makeLog, R.range(1, chance.integer({ min: 2, max: 6 })))
        : [];

    const makeLine = () => {
        const artworkW = chance.integer({ min: 200, max: 500 });
        const artworkH = chance.integer({ min: 200, max: 500 });
        return {
            id: chance.integer(),

            unit_price: chance.integer({ min: 50, max: 300 }) * 100, // in satang
            total_price: chance.integer({ min: 50, max: 300 }) * 100, // in satang
            base_product: {
                name: chance.sentence({ words: 3 })
            },
            base_product_color: {
                name: chance.sentence({ format: 'name' })
            },
            base_product_second_option: {
                value: chance.word()
            },
            artwork: {
                title: chance.sentence({ words: 3 }),
                _url: `https://picsum.photos/${artworkW}/${artworkH}`,
            },
            artwork_mm_width: chance.integer({ min: 10, max: 100000 }),
            artwork_mm_left: chance.integer({ min: 10, max: 100000 }),
            artwork_mm_top: chance.integer({ min: 10, max: 100000 }),
            quantity: chance.integer({ min: 1, max: 3 }),
            returns: chance.integer({ min: 0, max: 1 }),
            _variant_name: chance.sentence({ words: 2 }),
            _overlaid_product_img_url: 'https://picsum.photos/300'
        };
    };
    const lines = R.map(makeLine, R.range(1, chance.integer({ min: 2, max: 6 })));

    return {
        order: {
            id: chance.integer(),
            order_status: chance.pickone(['open', 'archived']),
            payment_method: chance.pickone(['credit_cart', 'bank_transfer']),
            payment_status: chance.pickone(['paid', 'unpaid']),
            fulfillment_status: chance.pickone(['fulfilled', 'unfulfilled']),

            shipping_full_name: chance.name(),
            shipping_address: chance.pickone(['-', chance.address()]),
            shipping_district: chance.city(),
            shipping_province: chance.word(),
            shipping_postal_code: chance.zip(),

            phone: chance.pickone(['', chance.phone()]),
            email: chance.email(),
            line_total: chance.integer({ min: 100, max: 500 }) * 100, // in satang
            shipping_cost: chance.integer({ min: 100, max: 500 }) * 100, // in satang
            total: chance.integer({ min: 100, max: 500 }) * 100, // in satang
            created_at: chance.date(),
            logs,
            lines
        }
    };
};
