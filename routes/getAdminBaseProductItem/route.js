const Joi = require('@hapi/joi');
const { BaseProduct } = require('../../models');

module.exports = {
    method: 'GET',
    path: '/api/admin/base_product/{id}',
    options: {
        auth: {
            strategy: 'session',
            access: {
                scope: 'admin'
            }
        },
        validate: {
            params: Joi.object({
                id: Joi.number().integer().min(1)
            })
        }
    },
    handler: async (request, h) => {
        const base_product = await BaseProduct.query()
              .withGraphFetched(`[
                  base_product_colors,
                  base_product_second_options,
                  base_product_variants
              ]`)
              .findById(request.params.id);

        base_product.base_product_colors.forEach(color => {
            color._bg_url = "https://picsum.photos/500/500";
        });

        // admin can see everything
        return { base_product };
    }
};
