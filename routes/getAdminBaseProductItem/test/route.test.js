const tap = require('tap');
const Joi = require('@hapi/joi');
const Knex = require('knex');
const objection = require('objection');

const Auth = require('../../../authentication');
const serverPromise = require('../../../server');
const schema = require("./../schema.js");
const knexConfigs = require('../../../knexfile.js');
const SampleRecords = require('../../../models/sample.js');
const {
    BaseProduct,
    BaseProductColor,
    BaseProductSecondOption,
    BaseProductVariant
} = require('../../../models');
const TestHelper = require('../../../services/testHelper');

async function main() {
    const server = await serverPromise;

    tap.test('200 ok', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });


        await BaseProduct.query().insert({
            ...SampleRecords.BaseProduct,
            id: 1,
            name: 'product_1',
            can_choose_color: true,
            has_second_option: true,
            bg_px_side_length: 0,
            frame_percent_left: 0,
            frame_percent_top: 0,
            frame_percent_width: 0,
            frame_mm_width: 0,
            frame_mm_height: 0,
        });

        await BaseProductColor.query().insert({
            ...SampleRecords.BaseProductColor,
            base_product_id: 1,
            name: 'blue',
            hex: '#0000ff',
            background_id: 1
        });

        await BaseProductSecondOption.query().insert({
            ...SampleRecords.BaseProductSecondOption,
            base_product_id: 1,
            value: 'second_option',
        });

        await BaseProductVariant.query().insert({
            ...SampleRecords.BaseProductVariant,
            base_product_id: 1,
            base_product_color_id: 1,
            base_product_second_option_id: 1,
            base_cost: 10000,
            quantity: 999
        });


        // when
        const res = await server.inject({
            method: 'GET',
            url: '/api/admin/base_product/1',
            auth: {
                strategy: 'session',
                credentials: Auth.mockCredentials({ id: 1 }, ['admin'])
            }
        });

        // then
        t.equal(res.statusCode, 200, 'returns 200 ok');
        Joi.assert(
            res.result,
            schema,
            'the response obeys the schema',
            { presence: 'required', allowUnknown: true }
        );
    });
}


main();
