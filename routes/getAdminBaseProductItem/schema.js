const Joi = require('@hapi/joi');

module.exports = Joi.object({
    base_product: Joi.object({
        id: Joi.number(),
        name: Joi.string(),
        has_second_option: Joi.boolean().allow(1, 0),
        bg_px_side_length: Joi.number(),
        frame_percent_left: Joi.number(),
        frame_percent_top: Joi.number(),
        frame_percent_width: Joi.number(),
        frame_mm_width: Joi.number(),
        frame_mm_height: Joi.number(),
        enabled: Joi.boolean().allow(1, 0),
        base_product_colors: Joi.array().items(
            Joi.object({
                id: Joi.number(),
                enabled: Joi.boolean().allow(1, 0),
                name: Joi.string(),
                hex: Joi.string(),
                is_default: Joi.boolean().allow(1, 0),
                _bg_url: Joi.string(),
            })
        ),
        base_product_second_options: Joi.array().items(
            Joi.object({
                id: Joi.number(),
                enabled: Joi.boolean().allow(1, 0),
                value: Joi.string()
            })
        ),
        base_product_variants: Joi.array().items(
            Joi.object({
                id: Joi.number(),
                base_product_id: Joi.number(),
                base_product_color_id: Joi.number(),
                base_product_second_option_id: Joi.number().allow(null),
                base_cost: Joi.number(),
                quantity: Joi.number(),
            })
        ),
    })
});
