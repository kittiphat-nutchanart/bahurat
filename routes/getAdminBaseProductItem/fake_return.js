module.exports = () => ({
    base_product: {
        id: 1,
        name: 'awesome T-shirt',
        has_second_option: true,
        bg_px_side_length: 1000,
        frame_percent_left: .347, // .01 = 1% , 1 = 100%
        frame_percent_top: .211,
        frame_percent_width: .306,
        frame_mm_width: 210,
        frame_mm_height: 297,
        enabled: true,
        base_product_colors: [
            {
                id: 1,
                enabled: true,
                name: 'pink',
                hex: '#FFC0CB',
                is_default: true,
                _bg_url: '/static/just-T-shirt-pink.jpg'
            },
            {
                id: 2,
                enabled: false,
                name: 'blue',
                hex: '#0000ff',
                is_default: false,
                _bg_url: '/static/just-T-shirt-blue.jpg'
            }
        ],
        base_product_second_options: [
            {
                id: 1,
                enabled: true,
                value: 'm'
            },
            {
                id: 2,
                enabled: true,
                value: 'l'
            }
        ],
        base_product_variants: [
            {
                id: 123,
                base_product_id: 1,
                base_product_color_id: 1,
                base_product_second_option_id: 1,
                base_cost: 10000,
                quantity: 1
            }
        ],
    }
});
