const Joi = require('@hapi/joi');

module.exports = Joi.object({
    overlaid_product: Joi.object({
        id: Joi.number(),
        author: Joi.object({
            username: Joi.string(),
        }),
        artwork: Joi.object({
            id: Joi.number(),
            title: Joi.string(),
            _url: Joi.string(),
        }),
        base_product: Joi.object({
            id: Joi.number(),
            name: Joi.string(),
            base_product_colors: Joi.array().items(
                Joi.object({
                    id: Joi.number(),
                    name: Joi.string().allow(''),
                    hex: Joi.string(),
                    _overlaid_product_img_url: Joi.string(),
                    _is_default: Joi.boolean()
                })
            ),
            base_product_second_options: Joi.array().items(
                Joi.object({
                    id: Joi.number(),
                    value: Joi.string().allow('')
                })
            ),
            base_product_variants: Joi.array().items(
                Joi.object({
                    id: Joi.number(),
                    base_product_color_id: Joi.number(),
                    base_product_second_option_id: Joi.number().allow(null),
                    _price: Joi.number()
                })
            )
        }),

    })
});
