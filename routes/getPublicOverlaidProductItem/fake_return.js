// Load and instantiate Chance
const chance = require('chance').Chance();
const R = require('ramda');


module.exports = () => {

    const makeBaseProductVairants = ([
        base_product_color_id,
        base_product_second_option_id
    ]) => ({
        id: chance.integer({min: 0, max: 1000}),
        base_product_color_id,
        base_product_second_option_id,
        _price: chance.integer({min: 10000, max: 99900})
    });

    const variantsWithSecondOpts = R.map(makeBaseProductVairants, R.xprod(R.range(1, 4), R.range(1, 4)));
    const variantsNoSecondOpts = R.map(makeBaseProductVairants, R.xprod(R.range(1, 4), [null]));
    const base_product_variants = [...variantsWithSecondOpts, ...variantsNoSecondOpts];

    const makeColor = (id) => ({
        id,
        name: chance.word(),
        hex: chance.color(),
        _overlaid_product_img_url: `https://picsum.photos/200?random=${chance.integer()}`,
        _is_default: false
    });
    const base_product_colors = R.map(makeColor, R.range(1,chance.integer({min: 2, max: 5})));

    chance.pickone(base_product_colors)._is_default = true; // set default color



    const makeSecondOpt = (id) => ({
        id: id,
        value: chance.word()
    });
    const base_product_second_options = R.map(makeSecondOpt, R.range(1,3));

    // simulate product without colors and second_opts
    // color.name = ''
    // second_opt.value = ''
    if (chance.bool()) {
        base_product_colors.forEach(i => i.name = '');
        base_product_second_options.forEach(i => i.value = '');
    }

    return {
        overlaid_product: {
            id: chance.integer(),
            author: {
                username: chance.word(),
            },
            artwork: {
                id: chance.integer(),
                title: chance.sentence({words: 4}),
                _url: `https://picsum.photos/200?random=${chance.integer()}`,
            },
            base_product: {
                id: chance.integer(),
                name: chance.sentence({words: 2}),
                base_product_colors,
                base_product_second_options,
                base_product_variants
            }
        }
    };
};
