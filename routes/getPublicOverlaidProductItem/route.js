const { OverlaidProduct } = require('../../models');
const schema = require('./schema.js');
const Boom = require('@hapi/boom');
const Joi = require('@hapi/joi');

const Storage = require('../../storage');

module.exports = {
    method: 'GET',
    path: '/api/overlaid_product/{id}',
    options: {
        validate: {
            params: Joi.object({
                id: Joi.number().integer().min(1)
            })
        }
    },
    handler: async (request, h) => {
        const overlaid_product = await OverlaidProduct.query()
              .findById(request.params.id)
              .withGraphFetched(`[
                  artwork.image,
                  base_product.[
                      base_product_colors,
                      base_product_second_options,
                      base_product_variants
                  ],
                  default_base_product_color,
                  author
              ]`);

        if (!overlaid_product) throw Boom.notFound();

        overlaid_product.artwork._url = Storage.getImgURL(overlaid_product.artwork.path, {
            maxWidth: Storage.DimenM,
            maxHeight: Storage.DimenM
        });

        // compute and set imgURL's
        overlaid_product.base_product
            .base_product_colors.forEach((color) => {
                const _imgParams = {
                    ...overlaid_product.imgParams,
                    bg_path: color.bg_path
                };
                const opts = { size: Storage.DimenM };
                color._overlaid_product_img_url = Storage.getOverlaidProductImgURL(
                    _imgParams,
                    opts
                );
                color._is_default =
                    (overlaid_product.default_base_product_color_id === color.id);
            });


        // compute and set variant's price
        overlaid_product.base_product
            .base_product_variants.forEach((variant) => {
                variant._price = variant.base_cost + overlaid_product.profit;
            });

        return Joi.attempt(
            { overlaid_product },
            schema,
            { stripUnknown: true, presence: 'required' }
        );
    }
};
