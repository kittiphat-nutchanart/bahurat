const tap = require('tap');
const Joi = require('@hapi/joi');
const Knex = require('knex');
const objection = require('objection');
const Iron = require('@hapi/iron');

const serverPromise = require('../../../server');
const schema = require("./../schema.js");
const knexConfigs = require('../../../knexfile.js');
const { CartLine, User } = require('../../../models');
const SampleRecords = require('../../../models/sample.js');
const Auth = require('../../../authentication');
const TestHelper = require('../../../services/testHelper');

async function main() {
    const server = await serverPromise;

    tap.test('get item ok 200', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });

        await User.query().insert({
            ...SampleRecords.User,
            id: 1,
            username: 'kangbomber',
            email: 'hello@mail.com',
            bank_account: '',
            about: '',
            i_am: ''
        });

        await CartLine.query().insert({
            ...SampleRecords.CartLine,
            session_id: 'abcdef'
        });

        const session = await Iron.seal(
            { id: 'abcdef', _store: {} },
            '11111111111111111111111111111111',
            Iron.defaults
        );

        // when
        const res = await server.inject({
            method: 'GET',
            url: '/api/me/account',
            headers: {
                Cookie: `session=${session}`
            },
            auth: {
                strategy: 'session',
                credentials: Auth.mockCredentials({ id: 1})
            }
        });

        //then
        t.equal(res.statusCode, 200, 'returns 200 ok');
        Joi.assert(
            res.result,
            schema,
            'the res obeys the schema',
            { presence: 'required' }
        );
        t.ok(res.result.logged_in);
        t.ok(res.result.has_cart);
        t.equal(
            res.result.account.username,
            'kangbomber',
            'username in res mathches the given/inserted one'
        );

    });

    tap.test('still accessible when not authenticated', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });

        // when
        const res = await server.inject({
            method: 'GET',
            url: '/api/me/account'
        });

        //then
        t.equal(res.statusCode, 200);
        t.equal(res.result.has_cart, false);
        t.equal(res.result.logged_in, false);
        t.equal(res.result.account, null);
    });

}


main();
