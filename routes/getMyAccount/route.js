const Joi = require('@hapi/joi');

const schema = require('./schema.js');
const { CartLine, User } = require('../../models');
const Storage = require('../../storage');
const { project } = require('../../models/projection.js');

module.exports = {
    method: 'GET',
    path: '/api/me/account',
    options: {
        auth: {
            strategy: 'session',
            mode: 'optional'
        }
    },
    handler: async (request, h) => {
        const res = {
            account: null,
            logged_in: false
        };

        if (request.auth.isAuthenticated) {
            const user = await User.query()
                  .findById(request.auth.credentials.user.id)
                  .withGraphFetched('[banner, avatar]');

            user._banner_url = user.banner ?
                Storage.getImgURL(user.banner.path, {
                    maxWidth: Storage.DimenM,
                    maxHeight: Storage.DimenM
                }) : '';

            const avatar_size = 50;
            user.avatar_url = user.avatar ?
                Storage.getImgURL(user.avatar.path, {
                    maxWidth: avatar_size,
                    maxHeight: avatar_size
                })
                : Storage.getLetterAvatarURL(user.username, avatar_size);

            res.account = user;
            res.logged_in = true;
        }

        res.has_cart = await CartLine.query()
              .where({
                  session_id: request.yar.id
              })
              .first()
              .limit(1) ? true : false;

        const schema = {
            account: {
                id: 1,
                email: 1,
                username: 1,
                about: 1,
                _banner_url: 1,
                avatar_url: 1,
                bank_account: 1,
                created_at: 1
            },
            logged_in: 1,
            has_cart: 1
        };

        return project(res, schema);
    }
};
