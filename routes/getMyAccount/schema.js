const Joi = require('@hapi/joi');

module.exports = Joi.object({
    account: Joi.object({
        id: Joi.number(),
        email: Joi.string(),
        username: Joi.string(),
        about: Joi.string().allow(''),

        _banner_url: Joi.string().allow(''), // computed
        avatar_url: Joi.string(), // computed

        bank_account: Joi.string().allow(''),
        created_at: Joi.date()
    }).optional(),
    logged_in: Joi.boolean(),
    has_cart: Joi.boolean()
});
