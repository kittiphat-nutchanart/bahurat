// Load and instantiate Chance
const chance = require('chance').Chance();


module.exports = () => {
    const newUser = chance.bool();
    const account = {
        id: chance.integer(),
        email: chance.email(),
        username: chance.word(),
        about: newUser ? '' : chance.paragraph(),
        _banner_url: newUser ? '' : 'https://picsum.photos/800/300',
        avatar_url: 'https://picsum.photos/200/500',
        bank_account: newUser ? '' : chance.cc(),
        created_at: chance.timestamp()
    };

    return {
        account,
        logged_in: true,
        has_cart: chance.bool(),
    };
};
