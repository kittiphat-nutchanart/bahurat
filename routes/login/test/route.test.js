const tap = require('tap');
const Knex = require('knex');
const objection = require('objection');
const bcrypt = require('bcrypt');
const setCookie = require('set-cookie-parser');

const serverPromise = require('../../../server');
const knexConfigs = require('../../../knexfile.js');
const SampleRecords = require('../../../models/sample.js');
const {
    User
} = require('../../../models');
const TestHelper = require('../../../services/testHelper');

async function main() {
    const server = await serverPromise;

    tap.test('200 ok', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });


        const saltRounds = 10;

        const bcrypt_hash = await bcrypt.hash('password', saltRounds);

        await User.query().insert({
            ...SampleRecords.User,
            username: 'yoyo',
            email: 'hello@mail.com',
            bcrypt_hash,
            confirmed_at: new Date().toISOString()
        });


        // when
        const res = await server.inject({
            method: 'POST',
            url: '/api/login',
            payload: {
                email: 'hello@mail.com',
                password: 'password'
            }
        });

        const cookies = setCookie.parse(
            res.headers['set-cookie'],
            { map: true }
        );

        // then
        t.equal(res.statusCode, 200, 'returns 200 ok');
        t.ok(cookies.sid, 'session "sid" cookie is set');
        // assert that sid, auth credentials, has the correct schema ** user iron to parse

    });

    // tap.test 'incorect email'
    // tap.test 'incorrect pasword'

}


main();
