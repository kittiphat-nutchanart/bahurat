const Boom = require('@hapi/boom');
const Joi = require('@hapi/joi');
const bcrypt = require('bcrypt');

const { User } = require('../../models');
const { project } = require('../../models/projection.js');

module.exports = {
    method: 'POST',
    path: '/api/login',
    options: {
        validate: {
            payload: Joi.object({
                email: Joi.string().required(),
                password: Joi.string().required()
            })
        }
    },
    handler: async (request, h) => {
        const user = await User.query()
              .findOne('email', request.payload.email);

        if (!user) throw Boom.badRequest('EMAIL_DO_NOT_EXIST');

        const passwordIsCorrect = await bcrypt.compare(
            request.payload.password,
            user.bcrypt_hash
        );
        if (!passwordIsCorrect) throw Boom.badRequest('PASSWORD_IS_INCORRECT');

        if (!user.confirmed_at) {
            throw Boom.badRequest('EMAIL_HAS_NOT_BEEN_CONFIRMED');
        }

        const cookieUser = project(user, {
            id: 1,
            logout_all_at: 1
        });
        request.cookieAuth.set({ user: cookieUser, scope: user.i_am.split(' ') });

        return h.response().code(200);
    }
};
