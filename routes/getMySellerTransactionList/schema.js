const Joi = require('@hapi/joi');

module.exports = Joi.object({
    seller_transactions: Joi.array().items(
        Joi.object({
            id: Joi.number(),
            description: Joi.string(),
            money_in: Joi.number(),
            balance: Joi.number(),
            created_at: Joi.date()
        })
    )
});
