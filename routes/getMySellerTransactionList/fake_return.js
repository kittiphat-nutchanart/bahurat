// Load and instantiate Chance
const chance = require('chance').Chance();
const R = require('ramda');

module.exports = () => {
    const makeTransaction = () => {
        return {
            id: chance.integer(),
            description: chance.sentence({words: 3}),
            money_in: chance.integer({min: -1000000, max: 1000000}),
            balance: chance.integer({min: 0, max: 1000000}),
            created_at: chance.date(),
        };
    };
    const seller_transactions = chance.integer({min: 1, max: 5}) > 4 ?
          [] :
          R.map(makeTransaction, R.range(1, 20));

    return {
        seller_transactions
    };
};
