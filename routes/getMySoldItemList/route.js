const Joi = require('@hapi/joi');

const { OrderLine } = require('../../models');
const schema = require('./schema.js');

module.exports = {
    method: 'GET',
    path: '/api/me/sold_items',
    options: {
        auth: 'session'
    },
    handler: async (request, h) => {

        const sold_items = await OrderLine.query()
              .joinRelated('order')
              .where({
                  author_id: request.auth.credentials.user.id,
              })
              .whereNotNull('order.paid_at')
              .orderBy('id', 'desc')
              .limit(100)
              .withGraphFetched(`[
                  base_product,
                  artwork
              ]`);

        const res = { sold_items };
        return Joi.attempt(res, schema, {stripUnknown: true, presence: 'required'});
    }
};
