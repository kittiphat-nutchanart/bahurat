const Joi = require('@hapi/joi');

module.exports = Joi.object({
    sold_items: Joi.array().items(
        Joi.object({
            id: Joi.number(),
            artwork: Joi.object({
                title: Joi.string(),
            }),
            base_product: Joi.object({
                name: Joi.string()
            }),
            author_unit_profit: Joi.number(),
            author_unit_fee: Joi.number(),
            author_unit_tax: Joi.number(),
            quantity: Joi.number(),
            created_at: Joi.date()
        })
    )
});
