const tap = require('tap');
const objection = require('objection');
const Joi = require('@hapi/joi');
const Knex = require('knex');

const Auth = require('../../../authentication');
const serverPromise = require('../../../server');
const schema = require("./../schema.js");
const knexConfigs = require('../../../knexfile.js');
const SampleRecords = require('../../../models/sample.js');
const {
    BaseProduct,
    Artwork,
    Order,
    OrderLine,
} = require('../../../models');
const TestHelper = require('../../../services/testHelper');

async function main() {
    const server = await serverPromise;

    tap.test('can get sold items', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });

        await BaseProduct.query().insert({
            ...SampleRecords.BaseProduct,
            id: 1,
            name: 'product_1',
            can_choose_color: true,
            has_second_option: true,
            bg_px_side_length: 0,
            frame_percent_left: 0,
            frame_percent_top: 0,
            frame_percent_width: 0,
            frame_mm_width: 0,
            frame_mm_height: 0,
        });

        await Artwork.query().insert({
            ...SampleRecords.Artwork,
            id: 1,
            author_id: 1,
            title: 'artwork_title',
            image_id: 1
        });

        await Order.query().insert({
            ...SampleRecords.Order,
            id: 100,
            payment_method: 'credit_card',
            paid_at: new Date().toISOString(),
            session_id: '16fd2706-8baf-433b-82eb-8c7fada847da',
            email: 'hello@mail.com',
            line_total: 100,
            shipping_cost: 0,
            total: 100
        });

        await OrderLine.query().insert({
            ...SampleRecords.OrderLine,
            author_id: 1,
            author_unit_profit: 100,
            author_unit_tax: 100,
            author_total_profit: 100,
            author_total_tax: 100,
            author_total_earnings: 100,
            unit_price: 100,
            total_price: 100,
            base_product_id: 1,
            base_product_color_id: 1,
            base_product_second_option_id: 1,
            artwork_id: 1,
            artwork_mm_width: 100,
            artwork_mm_top: 100,
            artwork_mm_left: 100,
            quantity: 1,
            order_id: 100
        });

        // when
        const res = await server.inject({
            method: 'GET',
            url: '/api/me/sold_items',
            auth: {
                strategy: 'session',
                credentials: Auth.mockCredentials({ id: 1})
            }
        });

        //then
        t.equal(res.statusCode, 200);
        t.equal(res.result.sold_items.length, 1);
        Joi.assert(
            res.result,
            schema,
            'the res obeys the schema',
            { presence: 'required' }
        );
    });

}


main();
