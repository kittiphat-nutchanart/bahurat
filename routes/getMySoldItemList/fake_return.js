// Load and instantiate Chance
const chance = require('chance').Chance();
const R = require('ramda');


module.exports = () => {
    const makeSoldItem = () => ({
        id: chance.integer(),
        artwork: {
            title: chance.sentence({words: 3}),
        },
        base_product: {
            name: chance.sentence({words: 3})
        },
        author_unit_profit: chance.integer({ min: 0, max: 100000 }),
        author_unit_fee: chance.integer({ min: 0, max: 100000 }),
        author_unit_tax: chance.integer({ min: 0, max: 100000 }),
        quantity: chance.integer({ min: 0, max: 10 }),
        created_at: chance.date()
    });

    const sold_items = chance.bool({ likelihood: 90 }) ?
          R.map(makeSoldItem, R.range(1, chance.integer({min: 2, max: 20}))) :
          [];

    return {sold_items};
};
