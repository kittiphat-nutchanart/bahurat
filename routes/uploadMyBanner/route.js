const Joi = require('@hapi/joi');

const { User } = require('../../models');
const Storage = require('../../storage');

module.exports = {
    method: 'POST',
    path: '/api/me/account/banner',
    options: {
        auth: 'session',
        payload: {
            parse: true,
            output: 'file',
            multipart: true,
            // first part is filesize limit the second part is for headers etc.
            maxBytes: (1 * 1024 * 1024) + (10 * 1024),
        },
        validate: {
            payload: Joi.object({
                file: Joi.object({
                    filename: Joi.string(),
                    path: Joi.string()
                }).prefs({ allowUnknown: true })
            }).prefs({ presence: 'required' })
        }
    },
    handler: async (request, h) => {
        const user = await User.query()
              .findById(request.auth.credentials.user.id)
              .withGraphFetched('banner');

        if (user.banner) {
            await User.transaction(async (trx) => {
                await user.$query(trx).patch({ banner_id: null });
                await user.banner.$query(trx).patch({ deleted: true });
            });
        }

        const uploadImg = await Storage.uploadImageFromLocal(
            request.payload.file.path,
            request.payload.file.filename,
            request.auth.credentials.user.id,
            'banners'
        );

        await user.$query().patch({ banner_id: uploadImg.id });

        return h.continue;
    }
};
