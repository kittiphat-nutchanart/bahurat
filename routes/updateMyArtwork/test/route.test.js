const tap = require('tap');
const Knex = require('knex');
const objection = require('objection');

const serverPromise = require('../../../server');
const knexConfigs = require('../../../knexfile.js');
const SampleRecords = require('../../../models/sample.js');
const { Artwork, Tag, ArtworkTag } = require('../../../models');
const Auth = require('../../../authentication');
const TestHelper = require('../../../services/testHelper');

async function main() {
    const server = await serverPromise;

    tap.test('ok 200', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });

        await Artwork.query().insert({
            ...SampleRecords.Artwork,
            author_id: 1,
            title: 'artwork_title',
            image_id: 1
        });

        // when
        const res = await server.inject({
            method: 'PATCH',
            url: '/api/me/artwork/1',
            auth: {
                strategy: 'session',
                credentials: Auth.mockCredentials({ id: 1})
            },
            payload: {
                title: 'yass',
                tags: ['awesome']
            }
        });

        const artwork = await Artwork.query().findById(1);
        const tag = await Tag.query().first();
        const artworkTag = await ArtworkTag.query().first();

        //then
        t.equal(res.statusCode, 204, 'returns 200 ok');
        t.equal(artwork.title, 'yass', 'title gets updated');
        t.equal(tag.name, 'awesome');
        t.equal(artworkTag.artwork_id, artwork.id);
        t.equal(artworkTag.tag_id, tag.id);
    });

    tap.test('400 badRequest when update property that is not allowed', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });

        // when
        const res = await server.inject({
            method: 'PATCH',
            url: '/api/me/artwork/1',
            auth: {
                strategy: 'session',
                credentials: Auth.mockCredentials({ id: 1})
            },
            payload: {
                i_am: 'yass'
            }
        });


        //then
        t.equal(res.statusCode, 400, 'returns 400 ok');
    });
}


main();
