const Joi = require('@hapi/joi');
const Boom = require('@hapi/boom');

const {
    Artwork
} = require('../../models');
const {
    TAG_MAX_LENGTH,
    TAG_REGEX,
    MAX_TAGS_COUNT
} = require('../../models/constraints');
const ArtworkService = require('../../services/artwork');

module.exports = {
    method: 'PATCH',
    path: '/api/me/artwork/{id}',
    options: {
        auth: 'session',
        validate: {
            params: Joi.object({
                id: Joi.number().integer().min(1)
            }),
            payload: Joi.object({
                title: Joi.string(),
                description: Joi.string(),
                tags: Joi.array().items(
                    Joi.string().max(TAG_MAX_LENGTH).pattern(TAG_REGEX).trim()
                ).max(MAX_TAGS_COUNT),
                visible: Joi.boolean()
            }).min(1)
        }
    },
    handler: async (request, h) => {
        const artwork = await Artwork.query()
              .findById(request.params.id)
              .throwIfNotFound();

        if (artwork.author_id !== request.auth.credentials.user.id) {
            throw Boom.forbidden();
        }

        await Artwork.transaction(async (trx) => {
            const update = { ...request.payload };

            if (update.tags) {
                await ArtworkService.updateTags(request.params.id, update.tags, trx);

                // delete update.tags
                // so code below don't have to deal with it
                delete update.tags;
            }

            // update the remaining properties in update
            if (Object.keys(update).length) {
                await Artwork.query(trx)
                    .findById(request.params.id)
                    .patch(update);
            }
        });

        return h.continue;
    }
};
