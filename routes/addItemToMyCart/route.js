const Joi = require('@hapi/joi');
const Boom = require('@hapi/boom');
const assert = require('assert').strict;

const {
    CartLine,
    OverlaidProduct,
    BaseProductColor,
    BaseProductSecondOption
} = require('../../models');

module.exports = {
    method: 'POST',
    path: '/api/me/cart/line',
    options: {
        validate: {
            payload: Joi.object({
                overlaid_product_id: Joi.number().integer().min(1),
                base_product_color_id: Joi.number().integer().min(1),
                base_product_second_option_id: Joi.number().integer().min(1)
            }).prefs({ presence: 'required' })
        }
    },
    handler: async (request, h) => {
        const {
            overlaid_product_id,
            base_product_color_id,
            base_product_second_option_id
        } = request.payload;

        const overlaidProductPromise = OverlaidProduct.query()
              .findById(overlaid_product_id)
              .throwIfNotFound();

        const baseProductColorPromise = BaseProductColor.query()
              .findById(base_product_color_id)
              .throwIfNotFound();

        const BaseProductSecondOptionPromise = BaseProductSecondOption.query()
              .findById(base_product_second_option_id)
              .throwIfNotFound();

        const [
            overlaidProduct,
            baseProductColor,
            baseProductSecondOption,
        ] = await Promise.all([
            overlaidProductPromise,
            baseProductColorPromise,
            BaseProductSecondOptionPromise
        ]);

        const { base_product_id } = overlaidProduct;

        const sameBaseProduct =
              (base_product_id === baseProductColor.base_product_id)
              && (base_product_id === baseProductSecondOption.base_product_id);
        assert(sameBaseProduct);

        const cartLines = await CartLine.query()
              .where({
                  session_id: request.yar.id
              });

        const matches = (target) =>
              (target.overlaid_product_id === overlaid_product_id)
              && (target.base_product_color_id === base_product_color_id)
              && (target.base_product_second_option_id === base_product_second_option_id);
        if (cartLines.some(matches)) return h.continue;

        const MAX_CART_LINES = 20;
        if (cartLines.length >= MAX_CART_LINES) {
            throw Boom.badRequest('too many items in the cart');
        }

        const cartLine = {
            session_id: request.yar.id,
            ...request.payload,
            base_product_id,
            quantity: 1
        };
        await CartLine.query().insert(cartLine);

        return h.continue;
    }
};
