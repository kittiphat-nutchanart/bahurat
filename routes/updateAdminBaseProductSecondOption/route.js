const Joi = require('@hapi/joi');
const { BaseProductSecondOption } = require('../../models');

const ProductService = require('../../services/product');

module.exports = {
    method: 'PATCH',
    path: '/api/admin/base_product_second_option/{id}',
    options: {
        auth: {
            strategy: 'session',
            access: {
                scope: 'admin'
            }
        },
        validate: {
            params: Joi.object({
                id: Joi.number().integer().min(1)
            }),
            payload: Joi.object({
                enabled: Joi.boolean()
            }).min(1)
        }
    },
    handler: async (request, h) => {
        await BaseProductSecondOption.transaction(async trx => {
            const secondOpt = await BaseProductSecondOption.query(trx)
                  .patchAndFetchById(request.params.id, request.payload)
                  .throwIfNotFound();

            await ProductService.assertBaseProductHasValidState(
                secondOpt.base_product_id,
                trx
            );
        });

        return h.continue;
    }
};
