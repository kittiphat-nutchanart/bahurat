const Joi = require('@hapi/joi');

module.exports = Joi.object({
    comments: Joi.array().items(
        Joi.object({
            id: Joi.number(),
            username: Joi.string(),
            avatar_url: Joi.string(),
            body: Joi.string(),
            created_at: Joi.date()
        })
    )
});
