const Joi = require('@hapi/joi');

const { Comment, Artwork } = require('../../models');
const { project } = require('../../models/projection.js');
const Storage = require('../../storage');

module.exports = {
    method: 'GET',
    path: '/api/artwork/{id}/comments',
    options: {
        validate: {
            query: Joi.object({
                size: Joi.number().integer().min(1).max(100).default(20),
                after: Joi.number().integer().min(1)
            })
        }
    },
    handler: async (request, h) => {
        // fetching latest comments
        let comments = await Artwork.relatedQuery('comments')
            .for(request.params.id)
            .where((builder) => {
                if (request.query.after) {
                    builder.where('id', '<', request.query.after);
                }
            })
            .orderBy('id', 'desc')
            .limit(request.query.size)
            .withGraphFetched('user.avatar');


        const schema = {
            comments: {
                id: 1,
                body: 1,
                username(input) {
                    return input.user.username;
                },
                avatar_url(input) {
                    const avatar_size = 50;
                    const avatar_url = input.user.avatar ?
                        Storage.getImgURL(input.user.avatar.path, {
                            maxWidth: avatar_size,
                            maxHeight: avatar_size
                        })
                        : Storage.getLetterAvatarURL(input.user.username, avatar_size);
                    return avatar_url;
                },
                created_at: 1
            }
        };

        return project({ comments }, schema);
    }
};
