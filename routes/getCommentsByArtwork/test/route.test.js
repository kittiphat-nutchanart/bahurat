const tap = require('tap');
const Joi = require('@hapi/joi');
const Knex = require('knex');
const objection = require('objection');

const serverPromise = require('../../../server');
const schema = require("./../schema.js");
const knexConfigs = require('../../../knexfile.js');
const { Artwork, Comment, User } = require('../../../models');
const SampleRecords = require('../../../models/sample.js');
const TestHelper = require('../../../services/testHelper');


async function main() {
    const server = await serverPromise;

    tap.test('returns comments', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });

        await User.query().insert({
            ...SampleRecords.User,
            id: 1,
            username: 'kangbomber',
            email: 'hello@mail.com',
            bank_account: '',
            about: '',
            i_am: ''
        });

        await Artwork.query().insert({
            ...SampleRecords.Artwork,
            id: 1
        });

        await Comment.query().insert({
            ...SampleRecords.Comment,
            artwork_id: 1,
            user_id: 1,
            body: 'comment 1'
        });

        await Comment.query().insert({
            ...SampleRecords.Comment,
            artwork_id: 1,
            user_id: 1,
            body: 'comment 2'
        });

        // when
        const res = await server.inject({
            method: 'GET',
            url: '/api/artwork/1/comments'
        });

        //then
        t.equal(res.statusCode, 200);
        Joi.assert(
            res.result,
            schema,
            { presence: 'required' }
        );
    });
}


main();
