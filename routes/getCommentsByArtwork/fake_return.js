// Load and instantiate Chance
const chance = require('chance').Chance();
const R = require('ramda');

module.exports = () => {
    const makeComment = () => {
        return {
            id: chance.integer(),
            username: chance.word(),
            avatar_url: 'https://picsum.photos/50?random=' + chance.integer(),
            body: chance.paragraph({ sentences: 1 }),
            created_at: chance.date()
        };
    };
    const comments = chance.bool({ likelihood: 10 }) ?
          [] :
          R.map(makeComment, R.range(1, chance.integer({ min: 2, max: 20 })));

    return { comments };
};
