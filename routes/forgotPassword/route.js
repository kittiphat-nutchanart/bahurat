const Boom = require('@hapi/boom');
const Joi = require('@hapi/joi');
const uuidv4 = require('uuid/v4');

const UserService = require('../../services/user');
const { User } = require('../../models');

module.exports = {
    method: 'POST',
    path: '/api/user/forgot_password',
    options: {
        validate: {
            payload: Joi.object({
                email: Joi.string()
            }).prefs({ presence: 'required' })
        }
    },
    handler: async (request, h) => {
        const { email } = request.payload;

        const user = await User.query().findOne({ email });

        if (!user) {
            throw Boom.badRequest('EMAIL_DO_NOT_EXIST');
        }

        const password_reset_token = uuidv4();
        await user.$query()
            .patch({
                password_reset_token,
                password_reset_requested_at: new Date().toISOString()
            });

        const inAppResetPasswordUrl =
              process.env.ROOT_URL + '/reset-password' +
              '?token=' + password_reset_token;

        await UserService.sendPasswordResetEmail(inAppResetPasswordUrl, email);

        return h.continue;
    }
};
