const tap = require('tap');
const Knex = require('knex');
const sinon = require('sinon');
const objection = require('objection');

const serverPromise = require('../../../server');
const knexConfigs = require('../../../knexfile.js');
const UserService = require('../../../services/user');
const SampleRecords = require('../../../models/sample.js');
const {
    User
} = require('../../../models');
const TestHelper = require('../../../services/testHelper');

async function main() {
    const server = await serverPromise;

    tap.test('forgotPasword will send you a reset-password email', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });

        const sinonSandbox = sinon.createSandbox();
        t.teardown(async () => {
            sinonSandbox.restore();
        });

        const sendMailStub = sinonSandbox.stub(UserService, 'sendPasswordResetEmail');

        sinonSandbox.stub(process, 'env').value({
            ROOT_URL: 'http://myapp.com'
        });

        const user = await User.query().insert({
            ...SampleRecords.User,
            username: 'hello',
            email: 'mail@email.com'
            //password_reset_token: null,
            //password_reset_requested_at: null
        });

        // when
        const res = await server.inject({
            method: 'POST',
            url: '/api/user/forgot_password',
            payload: {
                email: 'mail@email.com'
            }
        });

        const userAfterReset = await user.$query();
        const inAppResetUrl =
              process.env.ROOT_URL + '/reset-password?token=' +
              userAfterReset.password_reset_token;

        //then
        t.equal(res.statusCode, 204);
        t.notEqual(
            user.password_reset_token,
            userAfterReset.password_reset_token
        );
        t.notEqual(
            user.password_reset_requested_at,
            userAfterReset.password_reset_requested_at
        );
        sinon.assert.calledWith(sendMailStub, inAppResetUrl, user.email);
    });

}


main();
