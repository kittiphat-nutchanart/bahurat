const tap = require('tap');
const Knex = require('knex');
const objection = require('objection');
const Iron = require('@hapi/iron');
const request = require('supertest');
const path = require('path');
const sinon = require('sinon');

const Auth = require('../../../authentication');
const Storage = require('../../../storage');
const serverPromise = require('../../../server');
const knexConfigs = require('../../../knexfile.js');
const SampleRecords = require('../../../models/sample.js');
const {
    User,
    UploadImg
} = require('../../../models');
const TestHelper = require('../../../services/testHelper');

async function main() {
    const server = await serverPromise;

    tap.test('can upload avatar', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });

        const sinonSandbox = sinon.createSandbox();
        t.teardown(() => {
            sinonSandbox.restore();
        });

        const user = await User.query().insert({
            ...SampleRecords.User,
            id: 1,
            username: 'kangbomber',
            email: 'hello@mail.com',
            bank_account: '',
            about: '',
            i_am: '',
            avatar_id: 1
        });

        const oldAvatar = await UploadImg.query().insert({
            ...SampleRecords.UploadImg,
            id: 1,
            original_filename: 'hello.jpg',
            filesize: 999999,
            path: 'path/to/img',
            width: 1000,
            height: 1000,
            type: 'jpg',
            mime: 'image/jpeg',
            owner_id: 1,
            deleted: false
        });

        const newAvatar = await UploadImg.query().insert({
            ...SampleRecords.UploadImg,
            id: 2,
            original_filename: 'hello.jpg',
            filesize: 999999,
            path: 'path/to/img',
            width: 1000,
            height: 1000,
            type: 'jpg',
            mime: 'image/jpeg',
            owner_id: 1
        });

        const uploadStub = sinonSandbox.stub(Storage, 'uploadImageFromLocal')
            .returns(Promise.resolve(newAvatar));

        const session = await Iron.seal(
            Auth.mockCredentials({ id: 1 }),
            '11111111111111111111111111111111',
            Iron.defaults
        );

        const filepath = path.join(__dirname, 'fixtures/150.png');

        // when
        const res = await new Promise((rslv, rej) => {
            request(server.listener)
                .post('/api/me/account/avatar')
                .attach('file', filepath)
                .set('Cookie', [`sid=${session}`])
                .end((err, res) => {
                    if (err) {
                        rej(err);
                        return;
                    }
                    rslv(res);
                });
        });

        const userAfterUpload = await user.$query();
        const oldAvatarAfterUpload = await oldAvatar.$query();

        // then
        t.equal(res.statusCode, 204);
        t.ok(oldAvatarAfterUpload.deleted);
        t.equal(userAfterUpload.avatar_id, newAvatar.id);

        t.ok(uploadStub.firstCall);
        t.equal(typeof uploadStub.firstCall.args[0], 'string'); // localpath i.e. /tmp/...
        t.equal(uploadStub.firstCall.args[1], '150.png'); // originalFilename
        t.equal(uploadStub.firstCall.args[2], 1); // id of user who uploads
        t.equal(uploadStub.firstCall.args[3], 'avatars'); // assetType
    });

}


main();
