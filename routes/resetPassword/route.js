const Joi = require('@hapi/joi');
const Boom = require('@hapi/boom');
const dayjs = require('dayjs');

const UserService = require('../../services/user');
const { User } = require('../../models');

module.exports = {
    method: 'POST',
    path: '/api/user/reset_password',
    options: {
        validate: {
            payload: Joi.object({
                password_reset_token: Joi.string(),
                password: Joi.string()
            }).prefs({ presence: 'required' })
        }
    },
    handler: async (request, h) => {
        const user = await User.query()
              .findOne({ password_reset_token: request.payload.password_reset_token })
              .throwIfNotFound();

        // in days
        const tokenAge = dayjs().diff(user.password_reset_requested_at, 'day');
        const ageLimit = 7;
        if (tokenAge > ageLimit) {
            throw Boom.badRequest('PASSWORD_RESET_TOKEN_HAS_EXPIRED');
        }

        const bcrypt_hash = await UserService.encryptPassword(request.payload.password);

        await user.$query()
            .patch({
                bcrypt_hash,
                logout_all_at: new Date().toISOString(),
                password_reset_token: null, // expire the token
                password_reset_requested_at: null
            });

        return h.continue;
    }
};
