const tap = require('tap');
const Knex = require('knex');
const sinon = require('sinon');
const objection = require('objection');

const serverPromise = require('../../../server');
const knexConfigs = require('../../../knexfile.js');
const SampleRecords = require('../../../models/sample.js');
const {
    User
} = require('../../../models');
const TestHelper = require('../../../services/testHelper');

async function main() {
    const server = await serverPromise;

    tap.test('password can be reset', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });

        const sinonSandbox = sinon.createSandbox();
        t.teardown(async () => {
            sinonSandbox.restore();
        });

        //const sendMailStub = sinonSandbox.stub(UserService, 'sendPasswordResetEmail');
        const password_reset_token = '16fd2706-8baf-433b-82eb-8c7fada847da';

        const user = await User.query().insert({
            ...SampleRecords.User,
            username: 'hello',
            email: 'mail@email.com',
            password_reset_token,
            password_reset_requested_at: new Date().toISOString()
        });

        const password = 'newPassword';

        // when
        const res = await server.inject({
            method: 'POST',
            url: '/api/user/reset_password',
            payload: {
                password_reset_token,
                password
            }
        });

        const userAfterReset = await user.$query();

        //then
        t.equal(res.statusCode, 204);
        t.notEqual(
            user.bcrypt_hash,
            userAfterReset.bcrypt_hash
        );
        t.notEqual(
            user.logout_all_at,
            userAfterReset.logout_all_at
        );
        t.equal(
            userAfterReset.password_reset_token,
            null
        );
        t.equal(
            userAfterReset.password_reset_requested_at,
            null
        );
    });

}


main();
