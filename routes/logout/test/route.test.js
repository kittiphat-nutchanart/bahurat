const tap = require('tap');
const setCookie = require('set-cookie-parser');

const serverPromise = require('../../../server');


async function main() {
    const server = await serverPromise;

    tap.test('user can logout', async (t) => {
        // given
        const fakeSessionData = 'aabbccdd';

        // when
        const res = await server.inject({
            method: 'POST',
            url: '/api/logout',
            headers: {
                Cookie: `sid=${fakeSessionData}`
            }
        });

        const cookies = setCookie.parse(
            res.headers['set-cookie'],
            { map: true }
        );

        // then
        t.equal(res.statusCode, 204);
        t.equal(cookies.sid.value, '', '"sid" or cookie-key is removed');
    });
}


main();
