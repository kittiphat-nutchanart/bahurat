module.exports = {
    method: 'POST',
    path: '/api/logout',
    handler: async (request, h) => {
        request.cookieAuth.clear();

        return h.continue;
    }
};
