const CartService = require('../../services/cart');

module.exports = {
    method: 'GET',
    path: '/api/me/cart',
    handler: async (request, h) => {

        const cart = await CartService.getCart(request.yar.id);

        return cart;
    }
};
