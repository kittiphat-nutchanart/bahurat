const tap = require('tap');
const Knex = require('knex');
const objection = require('objection');
const Iron = require('@hapi/iron');

const serverPromise = require('../../../server');
const knexConfigs = require('../../../knexfile.js');
const TestHelper = require('../../../services/testHelper');

async function main() {
    const server = await serverPromise;

    tap.test('200 ok', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });

        const session = await Iron.seal(
            { id: 'abcdef', _store: {} },
            '11111111111111111111111111111111',
            Iron.defaults
        );

        // when
        const res = await server.inject({
            method: 'GET',
            url: '/api/me/cart',
            headers: {
                Cookie: `session=${session}`
            }
        });

        // then
        t.equal(res.statusCode, 200, 'gives 200 ok');
        t.equal(res.result.cart.lines.length, 0, 'cart contains 0 item');
    });

}


main();
