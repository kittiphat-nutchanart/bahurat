const Joi = require('@hapi/joi');

const { ArtworkLike, Artwork } = require('../../models');

module.exports = {
    method: 'POST',
    path: '/api/artwork/{id}/like',
    options: {
        auth: 'session',
        validate: {
            params: Joi.object({
                id: Joi.number().integer().required()
            })
        }
    },
    handler: async (request, h) => {
        await ArtworkLike.transaction(async (trx) => {
            await ArtworkLike.query(trx).insert({
                artwork_id: request.params.id,
                user_id: request.auth.credentials.user.id
            });

            await Artwork.query(trx).findById(request.params.id)
                .increment('likes_count', 1);
        });

        return h.continue;
    }
};
