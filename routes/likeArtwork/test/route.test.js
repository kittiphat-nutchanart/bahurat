const tap = require('tap');
const Joi = require('@hapi/joi');
const Knex = require('knex');
const objection = require('objection');

const serverPromise = require('../../../server');
const knexConfigs = require('../../../knexfile.js');
const { ArtworkLike, Artwork } = require('../../../models');
const SampleRecords = require('../../../models/sample');
const Auth = require('../../../authentication');
const TestHelper = require('../../../services/testHelper');

async function main() {
    const server = await serverPromise;

    tap.test('can like artworks', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });

        const artwork = await Artwork.query().insertAndFetch({
            ...SampleRecords.Artwork
        });

        // when
        const res = await server.inject({
            method: 'POST',
            url: '/api/artwork/1/like',
            auth: {
                strategy: 'session',
                credentials: Auth.mockCredentials({ id: 1})
            }
        });

        const like = await ArtworkLike.query().first();
        const updatedArtwork = await artwork.$query();

        //then
        t.equal(res.statusCode, 204);
        t.ok(like);
        t.equal(updatedArtwork.likes_count, artwork.likes_count + 1);
    });
}


main();
