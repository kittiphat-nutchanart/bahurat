const tap = require('tap');
const Knex = require('knex');
const objection = require('objection');

const serverPromise = require('../../../server');
const knexConfigs = require('../../../knexfile.js');
const SampleRecords = require('../../../models/sample.js');
const { User } = require('../../../models');
const Auth = require('../../../authentication');
const TestHelper = require('../../../services/testHelper');

async function main() {
    const server = await serverPromise;

    tap.test('ok 200', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });

        await User.query().insert({
            ...SampleRecords.User,
            id: 1,
            username: 'kangbomber',
            email: 'hello@mail.com',
            bank_account: '',
            about: '',
            i_am: ''
        });

        // when
        const res = await server.inject({
            method: 'PATCH',
            url: '/api/me/account',
            auth: {
                strategy: 'session',
                credentials: Auth.mockCredentials({ id: 1})
            },
            payload: {
                about: 'yass'
            }
        });

        const user = await User.query().findById(1);

        //then
        t.equal(res.statusCode, 204);
        t.equal(user.about, 'yass');
    });

    tap.test('400 badRequest when update property that is not allowed', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });

        await User.query().insert({
            ...SampleRecords.User,
            id: 1,
            username: 'kangbomber',
            email: 'hello@mail.com',
            bank_account: '',
            about: '',
            i_am: ''
        });

        // when
        const res = await server.inject({
            method: 'PATCH',
            url: '/api/me/account',
            auth: {
                strategy: 'session',
                credentials: Auth.mockCredentials({ id: 1})
            },
            payload: {
                i_am: 'yass'
            }
        });

        await User.query().findById(1);

        //then
        t.equal(res.statusCode, 400, 'returns 400 ok');
    });
}


main();
