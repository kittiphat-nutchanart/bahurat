const Joi = require('@hapi/joi');

const { User } = require('../../models');

module.exports = {
    method: 'PATCH',
    path: '/api/me/account',
    options: {
        auth: 'session',
        validate: {
            payload: Joi.object({
                bank_account: Joi.string(),
                about: Joi.string().max(1000, 'utf8').allow(''),
            }).min(1)
        }
    },
    handler: async (request, h) => {

        await User.query()
              .findById(request.auth.credentials.user.id)
              .patch(request.payload);

        return h.continue;
    }
};
