const tap = require('tap');
const Knex = require('knex');
const objection = require('objection');

const serverPromise = require('../../../server');
const knexConfigs = require('../../../knexfile.js');
const SampleRecords = require('../../../models/sample.js');
const {
    BaseProduct,
    BaseProductColor,
    BaseProductSecondOption,
    UploadImg
} = require('../../../models');
const Auth = require('../../../authentication');
const TestHelper = require('../../../services/testHelper');

async function main() {
    const server = await serverPromise;

    tap.test('ok 200', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });

        await BaseProduct.query().insert({
            ...SampleRecords.BaseProduct,
            id: 1,
            name: 'pink T-shirt',
            can_choose_color: true,
            has_second_option: true,
            bg_px_side_length: 1000,
            frame_percent_left: .324,
            frame_percent_top: .192,
            frame_percent_width: .352,
            frame_mm_width: 250,
            frame_mm_height: 300,
        });

        await UploadImg.query().insert({
            ...SampleRecords.UploadImg,
            id: 1,
            original_filename: 'hello.jpg',
            filesize: 999999,
            path: 'path/to/img',
            width: 1000,
            height: 1000,
            type: 'jpg',
            mime: 'image/jpeg',
            owner_id: 1
        });
        await BaseProductColor.query().insert({
            ...SampleRecords.BaseProductColor,
            base_product_id: 1,
            name: 'blue',
            hex: '#0000ff',
            background_id: 1,
            enabled: true,
            is_default: true
        });

        await BaseProductSecondOption.query().insert({
            ...SampleRecords.BaseProductSecondOption,
            base_product_id: 1,
            value: 'second_option',
            enabled: true
        });

        // when
        const res = await server.inject({
            method: 'PATCH',
            url: '/api/admin/base_product/1',
            auth: {
                strategy: 'session',
                credentials: Auth.mockCredentials({ id: 1}, ['admin'])
            },
            payload: {
                enabled: true
            }
        });

        const base_product = await BaseProduct.query().findById(1);

        //then
        t.equal(res.statusCode, 204, 'returns 204 ok');
        t.ok(base_product.enabled, 'enabled is set to true');
    });

    tap.test('invalid state occurs. base_product is not ready', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });

        // missing colors and second_opts so it's not ready
        await BaseProduct.query().insert({
            ...SampleRecords.BaseProduct,
            id: 1,
            name: 'pink T-shirt',
            can_choose_color: true,
            has_second_option: true,
            bg_px_side_length: 1000,
            frame_percent_left: .324,
            frame_percent_top: .192,
            frame_percent_width: .352,
            frame_mm_width: 250,
            frame_mm_height: 300,
        });


        // when
        const res = await server.inject({
            method: 'PATCH',
            url: '/api/admin/base_product/1',
            auth: {
                strategy: 'session',
                credentials: Auth.mockCredentials({ id: 1}, ['admin'])
            },
            payload: {
                enabled: true
            }
        });


        //then
        t.ok(res.statusCode >= 400);
    });
}


main();
