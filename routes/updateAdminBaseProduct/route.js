const Joi = require('@hapi/joi');
const { BaseProduct } = require('../../models');

const ProductService = require('../../services/product');

module.exports = {
    method: 'PATCH',
    path: '/api/admin/base_product/{id}',
    options: {
        auth: {
            strategy: 'session',
            access: {
                scope: 'admin'
            }
        },
        validate: {
            params: Joi.object({
                id: Joi.number().integer().min(1)
            }),
            payload: Joi.object({
                enabled: Joi.boolean()
            }).min(1)
        }
    },
    handler: async (request, h) => {
        await BaseProduct.transaction(async trx => {
            await BaseProduct.query(trx)
                .findById(request.params.id)
                .patch(request.payload);

            await ProductService.assertBaseProductHasValidState(
                request.params.id,
                trx
            );
        });

        return h.continue;
    }
};
