const Joi = require('@hapi/joi');

module.exports = Joi.object({
    artwork: Joi.object({
        id: Joi.number(),
    })
});
