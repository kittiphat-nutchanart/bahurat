const tap = require('tap');
const Joi = require('@hapi/joi');
const Knex = require('knex');
const objection = require('objection');
const Iron = require('@hapi/iron');
const request = require('supertest');
const path = require('path');
const sinon = require('sinon');

const Auth = require('../../../authentication');
const Storage = require('../../../storage');
const serverPromise = require('../../../server');
const schema = require("../schema.js");
const knexConfigs = require('../../../knexfile.js');
const SampleRecords = require('../../../models/sample.js');
const {
    User,
    BaseProduct,
    BaseProductColor,
    UploadImg
} = require('../../../models');
const TestHelper = require('../../../services/testHelper');

async function main() {
    const server = await serverPromise;

    tap.test('200 ok', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });

        const sinonSandbox = sinon.createSandbox();
        t.teardown(async () => {
            sinonSandbox.restore();
        });

        const newArtworkImg = await UploadImg.query().insert({
            ...SampleRecords.UploadImg,
        });
        const uploadStub = sinonSandbox.stub(Storage, 'uploadImageFromLocal')
              .returns(Promise.resolve(newArtworkImg));

        await User.query().insert({
            ...SampleRecords.User,
            id: 1,
            username: 'kangbomber',
            email: 'hello@mail.com',
            bank_account: '',
            about: '',
            i_am: ''
        });

        await BaseProduct.query().insert({
            ...SampleRecords.BaseProduct,
            id: 1,
            name: 'product_1',
            can_choose_color: true,
            has_second_option: true,
            bg_px_side_length: 1000,
            frame_percent_left: .347,
            frame_percent_top: .211,
            frame_percent_width: .306,
            frame_mm_width: 210,
            frame_mm_height: 297,
            enabled: true
        });

        await BaseProductColor.query().insert({
            ...SampleRecords.BaseProductColor,
            base_product_id: 1,
            name: 'blue',
            hex: '#0000ff',
            background_id: (
                await UploadImg.query().insert({
                    ...SampleRecords.UploadImg,
                    original_filename: 'hello.jpg',
                    filesize: 999999,
                    path: 'path/to/img',
                    width: 1000,
                    height: 1000,
                    type: 'jpg',
                    mime: 'image/jpeg',
                    owner_id: 1
                }).then(i => i.id)
            ),
            is_default: true
        });

        const session = await Iron.seal(
            Auth.mockCredentials({ id: 1 }),
            '11111111111111111111111111111111',
            Iron.defaults
        );

        // when
        const res = await new Promise((rslv, rej) => {
            request(server.listener)
                .post('/api/me/artwork')
                .attach('file', path.join(__dirname, 'fixtures/300.jpg'))
                .field('title', 'bahurat\'s sample artwork')
                .set('Cookie', [`sid=${session}`])
                .end((err, res) => {
                    if (err) {
                        rej(err);
                        return;
                    }
                    rslv(res);
                });
        });


        // then
        t.equal(res.statusCode, 200);
        Joi.assert(
            res.body,
            schema,
            'res obeys schema',
            { presence: 'required' }
        );
    });

}


main();
