const Joi = require('@hapi/joi');

const { Artwork } = require('../../models');
const {
    TAG_MAX_LENGTH,
    TAG_REGEX,
    MAX_TAGS_COUNT
} = require('../../models/constraints');
const Storage = require('../../storage');
const ProductService = require('../../services/product');
const ArtworkService = require('../../services/artwork');
const logger = require('../../logger');

module.exports = {
    method: 'POST',
    path: '/api/me/artwork',
    options: {
        auth: 'session',
        payload: {
            parse: true,
            output: 'file',
            multipart: true,
            // first part is filesize limit the second part is for headers, title, description and etc.
            maxBytes: (10 * 1024 * 1024) + (100 * 1024),
        },
        validate: {
            payload: Joi.object({
                file: Joi.object({
                    filename: Joi.string(),
                    path: Joi.string()
                }).prefs({ allowUnknown: true }),
                title: Joi.string(),
                description: Joi.string().optional().allow('').default(''),
                tags: Joi.string().custom((value, helper) => {
                    const tags_schema = Joi.array().items(
                        Joi.string().max(TAG_MAX_LENGTH).pattern(TAG_REGEX).trim()
                    ).max(MAX_TAGS_COUNT);

                    const tags = value.split(',');

                    Joi.assert(tags, tags_schema);

                    return tags;
                }).optional(),
                visible: Joi.boolean().optional().default(true),
            }).prefs({ presence: 'required' })
        }
    },
    handler: async (request, h) => {
        const uploadImg = await Storage.uploadImageFromLocal(
            request.payload.file.path,
            request.payload.file.filename,
            request.auth.credentials.user.id,
            'artworks'
        );

        const artwork = await Artwork.transaction(async (trx) => {
            const _artwork = await Artwork.query(trx).insert({
                author_id: request.auth.credentials.user.id,
                image_id: uploadImg.id,
                title: request.payload.title,
                description: request.payload.description,
                visible: request.payload.visible,
            });

            if (request.payload.tags) {
                await ArtworkService.updateTags(_artwork.id, request.payload.tags, trx);
            }

            return _artwork;
        });

        try {
            await ProductService.addOverlaidProduct(artwork.id);
        } catch(err) {
            // this may happen b/c there's no products in system
            logger.warn(err, 'failed to add products after upload');
        }

        return {
            artwork: {
                id: artwork.id
            }
        };
    }
};
