const tap = require('tap');
const Knex = require('knex');
const sinon = require('sinon');
const objection = require('objection');

const serverPromise = require('../../../server');
const knexConfigs = require('../../../knexfile.js');
const SampleRecords = require('../../../models/sample.js');
const {
    User
} = require('../../../models');
const TestHelper = require('../../../services/testHelper');


async function main() {
    const server = await serverPromise;

    tap.test('user can activate their account', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });

        const sinonSandbox = sinon.createSandbox();
        t.teardown(async () => {
            sinonSandbox.restore();
        });

        sinonSandbox.stub(process, 'env').value({
            ROOT_URL: 'http://myapp.com'
        });

        const confirmation_token = '16fd2706-8baf-433b-82eb-8c7fada847da';
        const user = await User.query().insert({
            ...SampleRecords.User,
            username: 'hello',
            email: 'mail@email.com',
            confirmed_at: null,
            confirmation_token
        });

        // when
        const res = await server.inject({
            method: 'POST',
            url: '/api/user/activate/' + confirmation_token,
        });

        const userAfterReset = await user.$query();

        //then
        t.equal(res.statusCode, 204); // redirect
        t.ok(userAfterReset.confirmed_at);
    });

}


main();
