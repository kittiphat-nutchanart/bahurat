const Joi = require('@hapi/joi');

const { User } = require('../../models');

module.exports = {
    method: 'POST',
    path: '/api/user/activate/{token}',
    options: {
        validate: {
            params: Joi.object({
                token: Joi.string()
            }).prefs({ presence: 'required' })
        }
    },
    handler: async (request, h) => {
        const { token } = request.params;

        const user = await User.query()
              .findOne({ confirmation_token: token  })
              .throwIfNotFound();

        if (!user.confirmed_at) {
            await user.$query()
                .patch({ confirmed_at: new Date().toISOString() });
        }

        // fix do toast notification in frontend
        return h.continue;
    }
};
