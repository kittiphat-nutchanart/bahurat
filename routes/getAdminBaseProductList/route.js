const { BaseProduct } = require('../../models');

module.exports = {
    method: 'GET',
    path: '/api/admin/base_products',
    options: {
        auth: {
            strategy: 'session',
            access: {
                scope: 'admin'
            }
        },
    },
    handler: async (request, h) => {
        const base_products = await BaseProduct.query();

        // admin can see everything
        return { base_products };
    }
};
