const tap = require('tap');
const Joi = require('@hapi/joi');
const Knex = require('knex');
const objection = require('objection');

const Auth = require('../../../authentication');
const serverPromise = require('../../../server');
const schema = require("./../schema.js");
const knexConfigs = require('../../../knexfile.js');
const SampleRecords = require('../../../models/sample.js');
const {
    BaseProduct
} = require('../../../models');
const TestHelper = require('../../../services/testHelper');

async function main() {
    const server = await serverPromise;

    tap.test('200 ok', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });


        await BaseProduct.query().insert({
            ...SampleRecords.BaseProduct,
            id: 1,
            name: 'product_1',
            can_choose_color: true,
            has_second_option: true,
            bg_px_side_length: 0,
            frame_percent_left: 0,
            frame_percent_top: 0,
            frame_percent_width: 0,
            frame_mm_width: 0,
            frame_mm_height: 0,
        });


        // when
        const res = await server.inject({
            method: 'GET',
            url: '/api/admin/base_products',
            auth: {
                strategy: 'session',
                credentials: Auth.mockCredentials({ id: 1 }, ['admin'])
            }
        });

        // then
        t.equal(res.statusCode, 200, 'returns 200 ok');
        t.equal(res.result.base_products.length, 1, 'has 1 base_product');
        Joi.assert(
            res.result,
            schema,
            'the response obeys the schema',
            { presence: 'required', allowUnknown: true }
        );
    });
}


main();
