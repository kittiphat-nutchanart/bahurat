// Load and instantiate Chance
const chance = require('chance').Chance();
const R = require('ramda');

module.exports = () => {
    const makeBaseProduct = () => {
        return {
            id: chance.integer(),
            name: chance.sentence({ words: 3 }),
            enabled: chance.bool({ likelihood: 50 })
        };
    };

    const randomRange = R.range(
        1,
        chance.integer({ min: 2, max: 6 })
    );
    const base_products = chance.bool({ likelihood: 20 }) ?
          [] :
          R.map(makeBaseProduct, randomRange);

    return { base_products };
};
