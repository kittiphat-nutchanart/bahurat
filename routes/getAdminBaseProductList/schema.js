const Joi = require('@hapi/joi');

module.exports = Joi.object({
    base_products: Joi.array().items(
        Joi.object({
            id: Joi.number(),
            name: Joi.string(),
            enabled: Joi.boolean().allow(0, 1)
        })
    )
});
