const Joi = require('@hapi/joi');
const R = require('ramda');

const { Order } = require('../../models');

module.exports = {
    method: 'PATCH',
    path: '/api/admin/order/{id}',
    options: {
        auth: {
            strategy: 'session',
            access: {
                scope: 'admin'
            }
        },
        validate: {
            params: Joi.object({
                id: Joi.number().integer().min(1)
            }),
            payload: Joi.object({
                order_status: Joi.string(),
                payment_status: Joi.string(),
                fulfillment_status: Joi.string()
            }).min(1)
        }
    },
    handler: async (request, h) => {
        const patchObj = R.pick(
            ['order_status', 'fulfillment_status'],
            request.payload
        );

        if (request.payload.payment_status === 'paid') {
            patchObj.paid_at = new Date().toISOString();
        } else if (request.payload.payment_status === 'unpaid') {
            patchObj.paid_at = null;
        }


        await Order.query()
            .findById(request.params.id)
            .patch(patchObj);

        return h.continue;
    }
};
