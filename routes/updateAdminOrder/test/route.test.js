const tap = require('tap');
const Knex = require('knex');
const objection = require('objection');

const serverPromise = require('../../../server');
const knexConfigs = require('../../../knexfile.js');
const Auth = require('../../../authentication');
const SampleRecords = require('../../../models/sample.js');
const {
    Order
} = require('../../../models');
const TestHelper = require('../../../services/testHelper');

async function main() {
    const server = await serverPromise;

    tap.test('admin can update order', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });

        const order = await Order.query().insert({
            ...SampleRecords.Order,
            payment_method: 'credit_card',
            session_id: '16fd2706-8baf-433b-82eb-8c7fada847da',
            email: 'hello@mail.com',
            line_total: 100,
            shipping_cost: 0,
            total: 100
        });

        // when
        const res = await server.inject({
            method: 'PATCH',
            url: '/api/admin/order/1',
            auth: {
                strategy: 'session',
                credentials: Auth.mockCredentials({ id: 1}, ['admin'])
            },
            payload: {
                payment_status: 'paid'
            }
        });

        const updatedOrder = await order.$query();

        //then
        t.equal(res.statusCode, 204);
        t.equal(updatedOrder.payment_status, 'paid');
    });
}


main();
