const tap = require('tap');
const Joi = require('@hapi/joi');
const Knex = require('knex');
const objection = require('objection');

const serverPromise = require('../../../server');
const schema = require("./../schema.js");
const knexConfigs = require('../../../knexfile.js');
const { Artwork, Comment, User } = require('../../../models');
const SampleRecords = require('../../../models/sample.js');
const Auth = require('../../../authentication');
const TestHelper = require('../../../services/testHelper');

async function main() {
    const server = await serverPromise;

    tap.test('can add a comment', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });

        await User.query().insert({
            ...SampleRecords.User,
            id: 1,
            username: 'kangbomber',
            email: 'hello@mail.com',
            bank_account: '',
            about: '',
            i_am: ''
        });

        await Artwork.query().insert({
            ...SampleRecords.Artwork,
            id: 1
        });

        // when
        const res = await server.inject({
            method: 'POST',
            url: '/api/artwork/1/comment',
            auth: {
                strategy: 'session',
                credentials: Auth.mockCredentials({ id: 1})
            },
            payload: {
                body: 'this is a comment.'
            }
        });


        //then
        t.equal(res.statusCode, 200);
        Joi.assert(
            res.result,
            schema,
            { presence: 'required' }
        );
    });
}


main();
