const Joi = require('@hapi/joi');

module.exports = Joi.object({
    comment: Joi.object({
        id: Joi.number(),
        username: Joi.string(),
        avatar_url: Joi.string(),
        body: Joi.string(),
        created_at: Joi.date()
    })
});
