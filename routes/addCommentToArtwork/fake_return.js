// Load and instantiate Chance
const chance = require('chance').Chance();

module.exports = () => {
    const comment = {
        id: chance.integer(),
        username: chance.word(),
        avatar_url: 'https://picsum.photos/50?random=' + chance.integer(),
        body: chance.paragraph({ sentences: 1 }),
        created_at: chance.date()
    };

    return { comment };
};
