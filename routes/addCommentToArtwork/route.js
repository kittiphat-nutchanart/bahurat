const Joi = require('@hapi/joi');

const { Comment } = require('../../models');
const { project } = require('../../models/projection.js');
const Storage = require('../../storage');

module.exports = {
    method: 'POST',
    path: '/api/artwork/{id}/comment',
    options: {
        auth: 'session',
        validate: {
            payload: Joi.object({
                body: Joi.string().required()
            })
        }
    },
    handler: async (request, h) => {
        const id = await Comment.query().insert({
            artwork_id: request.params.id,
            user_id: request.auth.credentials.user.id,
            body: request.payload.body
        }).then(i => i.id);

        const comment = await Comment.query()
            .findById(id)
            .withGraphFetched('user.avatar');

        const schema = {
            comment: {
                id: 1,
                body: 1,
                username(input) {
                    return input.user.username;
                },
                avatar_url(input) {
                    const avatar_size = 50;
                    const avatar_url = input.user.avatar ?
                          Storage.getImgURL(input.user.avatar.path, {
                              maxWidth: avatar_size,
                              maxHeight: avatar_size
                          })
                          : Storage.getLetterAvatarURL(input.user.username, avatar_size);
                    return avatar_url;
                },
                created_at: 1
            }
        };

        return project({ comment }, schema);
    }
};
