const R = require('ramda');
// Load and instantiate Chance
const chance = require('chance').Chance();

module.exports = () => {
    const artwork_px_width = chance.integer({min:200,max:800});
    const artwork_px_height = chance.integer({min:200,max:800});

    const tags = chance.bool() ?
          R.range(1, chance.integer({ min: 2, max: 6 }))
          .map(() => chance.word())
          : [];

    const artwork = {
        id: chance.integer(),
        title: chance.sentence({words: 3}),
        description: chance.bool() ? chance.paragraph() : '',
        tags,
        created_at: chance.date(),
        _url: `https://picsum.photos/${artwork_px_width}/${artwork_px_height}?random=${chance.integer()}`,
        original_img_url: `https://picsum.photos/${artwork_px_width}/${artwork_px_height}?random=${chance.integer()}`,
        px_width: artwork_px_width,
        px_height: artwork_px_height,
        author: {
            username: chance.word(),
            avatar_url: 'https://picsum.photos/50/70'
        },
        i_liked: chance.bool()
    };

    return {
        artwork
    };
};
