const Joi = require('@hapi/joi');

module.exports = Joi.object({
    artwork: Joi.object({
        id: Joi.number(),
        title: Joi.string(),
        description: Joi.string().allow(''),
        tags: Joi.array().items(
            Joi.string()
        ),
        created_at: Joi.date(),
        _url: Joi.string(),
        original_img_url: Joi.string(),
        px_width: Joi.number(),
        px_height: Joi.number(),
        author: Joi.object({
            username: Joi.string(),
            avatar_url: Joi.string()
        }),
        i_liked: Joi.boolean()
    })
});
