const tap = require('tap');
const objection = require('objection');
const Joi = require('@hapi/joi');
const Knex = require('knex');
const sinon = require('sinon');

const serverPromise = require('../../../server');
const schema = require("./../schema.js");
const knexConfigs = require('../../../knexfile.js');
const SampleRecords = require('../../../models/sample.js');
const {
    User,
    Artwork,
    UploadImg,
    Tag,
    ArtworkTag
} = require('../../../models');
const Storage = require('../../../storage');
const TestHelper = require('../../../services/testHelper');

async function main() {
    const server = await serverPromise;

    tap.test('get item ok 200', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });

        const sinonSandbox = sinon.createSandbox();
        t.teardown(() => {
            sinonSandbox.restore();
        });
        sinonSandbox.stub(Storage, 'getOriginalImgURL')
            .returns('http://fakeurl.to/img');

        // populate the db
        const artwork = await Artwork.query().insert({
            ...SampleRecords.Artwork,
            id: 100,
            author_id: 1,
            title: 'artwork_title',
            visible: true,
            image_id: (await UploadImg.query().insert({
                ...SampleRecords.UploadImg
            }).then(i => i.id))
        });

        const tag = await Tag.query().insert({
            ...SampleRecords.Tag
        });

        await ArtworkTag.query().insert({
            ...SampleRecords.ArtworkTag,
            artwork_id: artwork.id,
            tag_id: tag.id
        });

        await User.query().insert({
            ...SampleRecords.User,
            id: 1,
            username: 'hello',
            email: 'mail@email.com'
        });


        // when
        const res = await server.inject({
            method: 'GET',
            url: `/api/artwork/100`,
        });

        // then
        t.equal(res.statusCode, 200, 'returns 200 equal');
        Joi.assert(res.result, schema, { presence: "required" });
        t.equal(res.result.artwork.id, 100, 'id matches');
    });

}


main();
