const Joi = require('@hapi/joi');

const { Artwork, ArtworkLike } = require('../../models');
const schema = require('./schema.js');
const Storage = require('../../storage');
const { project } = require('../../models/projection.js');

module.exports = {
    method: 'GET',
    path: '/api/artwork/{id}',
    options: {
        auth: {
            strategy: 'session',
            mode: 'optional'
        },
        validate: {
            params: Joi.object({
                id: Joi.number().integer().min(1)
            })
        }
    },
    handler: async (request, h) => {
        const artwork = await Artwork.query()
              .findById(request.params.id)
              .publicVisible()
              .withGraphFetched('[author.avatar, image, tags]')
              .throwIfNotFound();

        let i_liked = false;
        if (request.auth.isAuthenticated) {
            const like = await ArtworkLike.query()
                  .findOne({
                      user_id: request.auth.credentials.user.id,
                      artwork_id: artwork.id
                  });

            if (like) i_liked = true;
        }
        artwork.i_liked = i_liked;

        const artwork_size = 800;
        artwork._url = Storage.getImgURL(
            artwork.image.path,
            { maxWidth: artwork_size, maxHeight: artwork_size }
        );

        artwork.original_img_url = Storage.getOriginalImgURL(artwork.image.path);

        const avatar_size = 50;
        artwork.author.avatar_url = artwork.author.avatar ?
            Storage.getImgURL(artwork.author.avatar.path, {
                maxWidth: avatar_size,
                maxHeight: avatar_size
            })
            : Storage.getLetterAvatarURL(artwork.author.username, avatar_size);

        const schema = {
            artwork: {
                id: 1,
                title: 1,
                description: 1,
                tags: (artwork) => artwork.tags.map(tag => tag.name),
                created_at: 1,
                _url: 1,
                original_img_url: 1,
                px_width: 1,
                px_height: 1,
                author: {
                    username: 1,
                    avatar_url: 1
                },
                i_liked: 1
            }
        };

        return project({ artwork }, schema);
    }
};
