const Boom = require('@hapi/boom');
const Joi = require('@hapi/joi');

const { OverlaidProduct } = require('../../models');
const Storage = require('../../storage');
const schema = require('./schema.js');

module.exports = {
    method: 'GET',
    path: '/api/me/artwork/{id}/overlaid_products',
    options: {
        auth: 'session',
        validate: {
            params: Joi.object({
                id: Joi.number().integer().min(1)
            })
        }
    },
    handler: async (request, h) => {
        const overlaid_products = await OverlaidProduct.query()
              .withGraphFetched('[artwork.image, default_base_product_color, base_product]')
              .where('artwork_id', request.params.id);

        // make sure that only owner/author has the right to access
        if (
            (overlaid_products.length > 0)
            && (overlaid_products[0].author_id !== request.auth.credentials.user.id)
        ) {
            // fix here
            // use the correct repesponse no-access right
            throw Boom.badImplementation();
        }

        overlaid_products.forEach((overlaidProduct) => {
            const opts = { size: Storage.DimenS };
            overlaidProduct._img_url = Storage.getOverlaidProductImgURL(
                overlaidProduct.imgParams,
                opts
            );
        });

        return Joi.attempt(
            { overlaid_products },
            schema,
            { stripUnknown: true, presence: 'required' }
        );
    }
};
