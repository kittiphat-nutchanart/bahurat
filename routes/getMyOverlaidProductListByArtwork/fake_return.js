// Load and instantiate Chance
const chance = require('chance').Chance();
const R = require('ramda');

module.exports = () => {
    const makeOverlaidProduct = () => {

        return {
            id: chance.integer(),
            base_product: {
                name: chance.sentence({words: 3})
            },
            _img_url: `https://picsum.photos/200?random=${chance.integer()}`,
            visible: chance.bool(),
            profit: chance.integer({min: 0, max: 100000})
        };
    };

    const overlaid_products = R.map(
        makeOverlaidProduct,
        R.range(1, chance.integer({min: 2, max: 7}))
    );
    return { overlaid_products };
};
