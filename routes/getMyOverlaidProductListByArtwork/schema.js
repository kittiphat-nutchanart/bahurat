const Joi = require('@hapi/joi');

module.exports = Joi.object({
    overlaid_products: Joi.array().items(
        Joi.object({
            id: Joi.number(),
            _img_url: Joi.string(),
            visible: Joi.alternatives().try(Joi.boolean(), Joi.number()),
            profit: Joi.number(),
            base_product: Joi.object({
                name: Joi.string()
            })
        })
    )
});
