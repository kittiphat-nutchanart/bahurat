const { OverlaidProduct } = require('../../models');
const Boom = require('@hapi/boom');
const Joi = require('@hapi/joi');

const { project } = require('../../models/projection.js');
const Storage = require('../../storage');

module.exports = {
    method: 'GET',
    path: '/api/me/overlaid_product/{id}',
    options: {
        auth: 'session',
        validate: {
            params: Joi.object({
                id: Joi.number().integer().min(1)
            })
        }
    },
    handler: async (request, h) => {
        const overlaid_product = await OverlaidProduct.query()
              .withGraphFetched('[artwork.image, base_product.[base_product_colors]]')
              .findById(request.params.id);

        if (!overlaid_product) {
            throw Boom.notFound();
        }

        // make sure that only owner/author has the right to access
        if (
            (overlaid_product.author_id !== request.auth.credentials.user.id)
        ) {
            throw Boom.unauthorized();
        }

        const artworkOpts = {
            maxWidth: Storage.DimenS,
            maxHeight: Storage.DimenS
        };
        const artwork_url = Storage.getImgURL(overlaid_product.artwork.path, artworkOpts);
        overlaid_product.artwork._url = artwork_url;

        overlaid_product.base_product
            .base_product_colors.forEach((color) => {
                const bgOpts = {
                    maxWidth: Storage.DimenM,
                    maxHeight: Storage.DimenM
                };

                color._bg_url = Storage.getImgURL(color.bg_path, bgOpts);
                color._is_default =
                    (overlaid_product.default_base_product_color_id === color.id);

            });

        const schema = {
            id: 1,
            artwork_mm_width: 1,
            artwork_mm_top: 1,
            artwork_mm_left: 1,
            default_base_product_color_id: 1,
            artwork: {
                id: 1,
                _url: 1,
                px_height: 1,
                px_width: 1,
            },
            base_product: {
                id: 1,
                name: 1,
                bg_px_side_length: 1,
                frame_percent_left: 1,
                frame_percent_top: 1,
                frame_percent_width: 1,
                frame_mm_width: 1,
                frame_mm_height: 1,
                base_product_colors: {
                    id: 1,
                    name: 1,
                    hex: 1,
                    _bg_url: 1,
                    _is_default: 1,
                }
            }
        };
        const res = {
            overlaid_product: project(overlaid_product, schema),
        };

        return res;
    }
};
