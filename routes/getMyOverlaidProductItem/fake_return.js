const chance = require('chance').Chance();

module.exports = () => {
    const overlaid_product = {
        overlaid_product: {
            id: 1,
            artwork_mm_width: 210,
            artwork_mm_top: 0,
            artwork_mm_left: 0,
            default_base_product_color_id: 1234,
            artwork: {
                id: 12,
                _url: '/static/artwork.png',
                px_width: 1989,
                px_height: 2459
            },
            base_product: {
                id: 123,
                name: 'just a T-shirt',
                bg_px_side_length: 1000,
                frame_percent_left: .347, // .01 = 1% , 1 = 100%
                frame_percent_top: .211,
                frame_percent_width: .306,
                frame_mm_width: 210,
                frame_mm_height: 297,
                base_product_colors: [
                    {
                        id: 1234,
                        name: 'pink',
                        hex: '#FFC0CB',
                        _bg_url: '/static/just-T-shirt-pink.jpg',
                        _is_default: true
                    },
                    {
                        id: 12345,
                        name: 'blue',
                        hex: '#0000ff',
                        _bg_url: '/static/just-T-shirt-blue.jpg',
                        _is_default: false
                    }
                ]
            }
        }
    };

    if (chance.bool()) {
        overlaid_product.overlaid_product.base_product.base_product_colors
            .forEach(i => i.name = '')
    }

    return overlaid_product;
};
