const Joi = require('@hapi/joi');

module.exports = Joi.object({
    overlaid_product: Joi.object({
        id: Joi.number(),
        artwork_mm_width: Joi.number(),
        artwork_mm_top: Joi.number(),
        artwork_mm_left: Joi.number(),
        default_base_product_color_id: Joi.number(),
        artwork: Joi.object({
            id: Joi.number(),
            _url: Joi.string(),
            px_height: Joi.number(),
            px_width: Joi.number()
        }),
        base_product: Joi.object({
            id: Joi.number(),
            name: Joi.string(),
            bg_px_side_length: Joi.number(),
            frame_percent_left: Joi.number(),
            frame_percent_top: Joi.number(),
            frame_percent_width: Joi.number(),
            frame_mm_width: Joi.number(),
            frame_mm_height: Joi.number(),
            base_product_colors: Joi.array().items(
                Joi.object({
                    id: Joi.number(),
                    name: Joi.string().allow(''),
                    hex: Joi.string(),
                    _bg_url: Joi.string(),
                    _is_default: Joi.boolean()
                })
            )
        })
    })
});
