const Joi = require('@hapi/joi');
const tap = require('tap');

const fakeReturn = require("./../fake_return.js");
const schema = require("./../schema.js");


function main() {
    tap.test("get_my_overlaid_product_item.fake_return obeys the schema", async t => {
        Joi.assert(fakeReturn(), schema, "fake_return obeys the schema", {presence: "required"});
    });
}

main();
