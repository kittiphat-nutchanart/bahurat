// Load and instantiate Chance
const chance = require('chance').Chance();
const R = require('ramda');

module.exports = () => {
    const makeArtwork = () => {
        return {
            id: chance.integer(),
            title: chance.sentence({words: 3}),
            _url: "https://picsum.photos/200?random=" + chance.integer()
        };
    };
    const artworks = chance.bool({likelihood: 10}) ?
          [] :
          R.map(makeArtwork, R.range(1,chance.integer({min: 2, max: 25})));

    return {
        artworks,
        page_count: R.isEmpty(artworks) ? 0 : chance.integer({min: 1, max: 10})
    };
};
