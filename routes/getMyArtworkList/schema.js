const Joi = require('@hapi/joi');

module.exports = Joi.object({
    artworks: Joi.array().items(
        Joi.object({
            id: Joi.number(),
            title: Joi.string(),
            _url: Joi.string()
        })
    ),
    page_count: Joi.number()
});
