const Joi = require('@hapi/joi');

const schema = require('./schema.js');
const { Artwork } = require('../../models');
const Storage = require('../../storage');

module.exports = {
    method: 'GET',
    path: '/api/me/artworks',
    options: {
        auth: 'session',
        validate: {
            query: Joi.object({
                page: Joi.number().required(),
                sort: Joi.string().allow('new', 'old').required(),
                size: Joi.number().integer().min(1).max(100).default(20)
            })
        }
    },
    handler: async (request, h) => {
        const artworksQuery = Artwork.query()
              .where('author_id', request.auth.credentials.user.id)
              .authorVisible()
              .withGraphFetched('image');

        const SORT_TO_ORDER_BY = {
            new: ['id', 'desc'],
            old: ['id', 'asc']
        };

        const artworksPromise = artworksQuery
              .orderBy(...SORT_TO_ORDER_BY[request.query.sort])
              .offset(request.query.size * (request.query.page - 1))
              .limit(request.query.size);

        const rowCountPromise = artworksQuery.resultSize();

        const [artworks, rowCount] = await Promise.all([
            artworksPromise,
            rowCountPromise
        ]);

        const page_count = Math.ceil(rowCount / request.query.size);

        artworks.forEach(artwork => {
            artwork._url = Storage.getImgURL(artwork.path, {
                maxWidth: Storage.DimenS,
                maxHeight: Storage.DimenS
            });
        });

        return Joi.attempt(
            { artworks, page_count },
            schema,
            { stripUnknown: true, presence: 'required' }
        );
    }
};
