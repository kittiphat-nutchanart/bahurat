const tap = require('tap');
const Joi = require('@hapi/joi');
const Knex = require('knex');
const objection = require('objection');
const sinon = require('sinon');
const R = require('ramda');

const serverPromise = require('../../../server');
const schema = require("./../schema.js");
const knexConfigs = require('../../../knexfile.js');
const SampleRecords = require('../../../models/sample.js');
const { User, Artwork, UploadImg } = require('../../../models');
const Auth = require('../../../authentication');
const TestHelper = require('../../../services/testHelper');

async function main() {
    const server = await serverPromise;

    tap.test('200 ok', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });

        await User.query().insert({
            ...SampleRecords.User,
            id: 1,
            username: 'kangbomber',
            email: 'hello@mail.com',
            bank_account: '1234',
            about: '',
            i_am: ''
        });

        await Artwork.query().insert({
            ...SampleRecords.Artwork,
            id: 1,
            author_id: 1,
            title: 'artwork_title',
            image_id: (await UploadImg.query().insert({
                ...SampleRecords.UploadImg
            }).then(i => i.id))
        });

        await Artwork.query().insert({
            ...SampleRecords.Artwork,
            id: 2,
            author_id: 1,
            title: 'artwork_title',
            image_id: (await UploadImg.query().insert({
                ...SampleRecords.UploadImg
            }).then(i => i.id))
        });

        await Artwork.query().insert({
            ...SampleRecords.Artwork,
            id: 3,
            author_id: 2,
            title: 'artwork_title',
            image_id: (await UploadImg.query().insert({
                ...SampleRecords.UploadImg
            }).then(i => i.id))
        });

        // when
        const res = await server.inject({
            method: 'GET',
            url: `/api/me/artworks?page=1&sort=new`, // sort=[new|old]
            auth: {
                strategy: 'session',
                credentials: Auth.mockCredentials({ id: 1})
            }
        });

        //then
        t.equal(res.statusCode, 200, 'returns 200 equal');
        Joi.assert(
            res.result,
            schema,
            'it obeys the schema',
            { presence: 'required' }
        );
        t.ok(
            R.allPass([
                R.contains(1),
                R.contains(2),
                R.complement(R.contains(3))
            ])(R.map(R.prop('id'), res.result.artworks)),
            'it retunrs only your artworks, no other\'s'
        );
    });

    // fix
    // come up with a more practical test
    //tap.test('page_count is computed correctly', async (t) => {
    //    // given
    //    // knex setup
    //    await knex.initialize();
    //    await knex.migrate.latest();
    //    t.teardown(async () => {
    //        await knex.destroy();
    //    });

    //    const recordCount = 99;
    //    await Artwork.knexQuery().insert(R.map(() => ({
    //        ...SampleRecords.Artwork,
    //        author_id: 1,
    //        title: 'artwork_1',
    //        image_id: 1
    //    }), R.range(1, R.inc(recordCount))));

    //    // when
    //    const res = await server.inject({
    //        method: 'GET',
    //        url: `/api/me/artworks?page=1&sort=new`, // sort=[new|old]
    //        auth: {
    //            strategy: 'session',
    //            credentials: Auth.mockCredentials({ id: 1})
    //        }
    //    });


    //    // then
    //    t.equal(
    //        res.result.page_count,
    //        Math.ceil(recordCount / res.request.query.size),
    //        'page_count is computed correctly'
    //    );
    //});

    tap.test('query-param "sort" works as expected', async (t) => {

        t.beforeEach(async (done, t) => {
            // spy, stub set-up
            const sinonSandbox = sinon.createSandbox();
            const artworkQueryBuilder = Artwork.query();
            sinonSandbox.stub(Artwork, 'query').returns(artworkQueryBuilder);

            t.context.spy = sinonSandbox.spy(artworkQueryBuilder, 'orderBy');
            t.context.sinonSandbox = sinonSandbox;
        });

        t.afterEach(async (done, t) => {
            // spy, stub restore
            t.context.sinonSandbox.restore();
        });


        t.test('sort=new', async (t) => {
            //given
            // nothing

            // when
            await server.inject({
                method: 'GET',
                url: `/api/me/artworks?page=1&sort=new`, // sort=[new|old]
                auth: {
                    strategy: 'session',
                    credentials: Auth.mockCredentials({ id: 1})
                }
            });

            //then
            t.equal(t.context.spy.callCount, 1, 'orderBy is called once');
            t.ok(
                t.context.spy.lastCall.calledWithExactly('id', 'desc'),
                'orderBy is called with correct args'
            );
        });

        t.test('sort=old', async (t) => {
            //given
            // nothing

            // when
            await server.inject({
                method: 'GET',
                url: `/api/me/artworks?page=1&sort=old`, // sort=[new|old]
                auth: {
                    strategy: 'session',
                    credentials: Auth.mockCredentials({ id: 1})
                }
            });

            //then
            t.equal(t.context.spy.callCount, 1, 'orderBy is called once');
            t.ok(
                t.context.spy.lastCall.calledWithExactly('id', 'asc'),
                'orderBy is called with correct args'
            );
        });
    });

    tap.test('401 when not logged-in', async (t) => {
        // given
        // nothing

        // when
        const res = await server.inject({
            method: 'GET',
            url: '/api/me/artworks?page=1&sort=new'
        });

        //then
        t.equal(res.statusCode, 401, 'returns 401 status');
    });

}


main();
