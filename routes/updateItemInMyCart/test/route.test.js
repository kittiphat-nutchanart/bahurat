const tap = require('tap');
const Knex = require('knex');
const objection = require('objection');
const Iron = require('@hapi/iron');
const sinon = require('sinon');

const serverPromise = require('../../../server');
const knexConfigs = require('../../../knexfile.js');
const CartService = require('../../../services/cart');
const SampleRecords = require('../../../models/sample.js');
const {
    CartLine,
} = require('../../../models');
const TestHelper = require('../../../services/testHelper');


async function main() {
    const server = await serverPromise;

    tap.test('can update line item', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });

        const sinonSandbox = sinon.createSandbox();
        t.teardown(async () => {
            sinonSandbox.restore();
        });

        const session = await Iron.seal(
            { id: 'abcdefg', _store: {} },
            '11111111111111111111111111111111',
            Iron.defaults
        );

        const cartLine = await CartLine.query().insert({
            ...SampleRecords.CartLine,
            id: 10,
            session_id: 'abcdefg',
            overlaid_product_id: 1,
            base_product_id: 1,
            base_product_color_id: 1,
            base_product_second_option_id: 1,
            quantity: 1
        });

        const fakeCart = {};
        sinonSandbox.stub(CartService, 'getCart')
            .returns(Promise.resolve(fakeCart));

        // when
        const res = await server.inject({
            method: 'PATCH',
            url: '/api/me/cart/line/10',
            headers: {
                Cookie: `session=${session}`
            },
            payload: {
                quantity: 5
            }
        });

        const updatedCartLine = await cartLine.$query();

        // then
        t.equal(res.statusCode, 200);
        t.equal(updatedCartLine.quantity, 5);
        t.equal(res.result, fakeCart);
    });

    // fixhere
    // test_401_not_authorized when delete other's item not yours
}


main();
