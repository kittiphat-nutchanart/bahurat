const Joi = require('@hapi/joi');
const Boom = require('@hapi/boom');

const { CartLine } = require('../../models');
const CartService = require('../../services/cart');

module.exports = {
    method: 'PATCH',
    path: '/api/me/cart/line/{id}',
    options: {
        validate: {
            params: Joi.object({
                id: Joi.number().integer().min(1)
            }),
            payload: Joi.object({
                quantity: Joi.number().integer().min(1),
            }).min(1)
        }
    },
    handler: async (request, h) => {

        const cartLine = await CartLine.query()
              .findById(request.params.id)
              .throwIfNotFound();

        if (cartLine.session_id !== request.yar.id) {
            throw Boom.unauthorized();
        }

        await cartLine.$query().patch(request.payload);

        const cart = await CartService.getCart(request.yar.id);

        return cart;
    }
};
