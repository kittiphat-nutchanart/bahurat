const tap = require('tap');
const Knex = require('knex');
const sinon = require('sinon');
const objection = require('objection');

const serverPromise = require('../../../server');
const knexConfigs = require('../../../knexfile.js');
const UserService = require('../../../services/user');
const {
    User
} = require('../../../models');
const TestHelper = require('../../../services/testHelper');


async function main() {
    const server = await serverPromise;

    tap.test('people can sign-up a new account', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });

        const sinonSandbox = sinon.createSandbox();
        t.teardown(async () => {
            sinonSandbox.restore();
        });

        const encryptPasswordSpy = sinonSandbox.spy(UserService, 'encryptPassword');
        const sendMailStub = sinonSandbox.stub(UserService, 'sendAccountConfirmationEmail');

        sinonSandbox.stub(process, 'env').value({
            ROOT_URL: 'http://myapp.com'
        });

        // when
        const res = await server.inject({
            method: 'POST',
            url: '/api/user/signup',
            payload: {
                username: 'timmy',
                email: 'timmy@mail.com',
                password: '**********',
            }
        });

        const user = await User.query().first();


        //then
        t.equal(res.statusCode, 204);
        t.equal(user.username, 'timmy');
        t.equal(user.email, 'timmy@mail.com');
        sinon.assert.calledWith(encryptPasswordSpy, '**********');
        sinon.assert.calledWith(sendMailStub, user.confirmation_token, user.email);
    });

    // fix
    // test username has been used error

    // fix
    // test email has been used error
}


main();
