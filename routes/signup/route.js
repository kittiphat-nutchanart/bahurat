const Joi = require('@hapi/joi');
const Boom = require('@hapi/boom');
const uuidv4 = require('uuid/v4');

const { User } = require('../../models');
const UserService = require('../../services/user');

module.exports = {
    method: 'POST',
    path: '/api/user/signup',
    options: {
        validate: {
            payload: Joi.object({
                username: Joi.string().pattern(/^[a-zA-Z0-9_]+$/),
                email: Joi.string().email({ tlds: false }),
                password: Joi.string(),
            }).prefs({ presence: 'required' })
        }
    },
    handler: async (request, h) => {
        const {
            email,
            username,
            password
        } = request.payload;

        const userWithSameUsername = await User.query()
              .findOne({ username });
        if (userWithSameUsername) {
            throw Boom.badRequest('USERNAME_HAS_BEEN_TAKEN');
        }

        const userWithSameEmail = await User.query()
            .findOne({ email });
        if (userWithSameEmail) {
            throw Boom.badRequest('EMAIL_HAS_BEEN_USED');
        }

        const bcrypt_hash = await UserService.encryptPassword(password);
        const confirmation_token = uuidv4();
        const newUser = {
            email,
            username,
            bcrypt_hash,
            confirmation_token
        };
        await User.query()
            .insert(newUser);

        await UserService.sendAccountConfirmationEmail(confirmation_token, email);

        return h.continue;
    }
};
