const tap = require('tap');
const Knex = require('knex');
const objection = require('objection');
const Iron = require('@hapi/iron');
const sinon = require('sinon');

const CartService = require('../../../services/cart');
const serverPromise = require('../../../server');
const knexConfigs = require('../../../knexfile.js');
const SampleRecords = require('../../../models/sample.js');
const {
    OrderLine,
    Order
} = require('../../../models');
const TestHelper = require('../../../services/testHelper');

async function main() {
    const server = await serverPromise;

    tap.test('can update checkout order', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });

        const sinonSandbox = sinon.createSandbox();
        t.teardown(async () => {
            sinonSandbox.restore();
        });

        const fakeCheckout = {};
        sinonSandbox.stub(CartService, 'getCheckout').returns(fakeCheckout);

        const order = await Order.query().insert({
            ...SampleRecords.Order,
            id: 1,
            session_id: '16fd2706-8baf-433b-82eb-8c7fada847da',
            line_total: 100,
            shipping_cost: 0,
            total: 100
        });

        await OrderLine.query().insert({
            ...SampleRecords.OrderLine,
            author_id: 1,
            author_unit_profit: 100,
            author_unit_tax: 100,
            author_total_profit: 100,
            author_total_tax: 100,
            author_total_earnings: 100,
            unit_price: 100,
            total_price: 100,
            base_product_id: 1,
            base_product_color_id: 1,
            base_product_second_option_id: 1,
            artwork_id: 100,
            artwork_mm_width: 100,
            artwork_mm_top: 100,
            artwork_mm_left: 100,
            quantity: 1,
            order_id: 1
        });

        const session = await Iron.seal(
            { id: '16fd2706-8baf-433b-82eb-8c7fada847da', _store: {} },
            '11111111111111111111111111111111',
            Iron.defaults
        );

        // when
        const res = await server.inject({
            method: 'PATCH',
            url: '/api/checkout/1',
            headers: {
                Cookie: `session=${session}`
            },
            payload: {
                shipping_full_name: 'somsak',
                shipping_address: '53 m8',
                shipping_district: 'DoembangNangbuat',
                shipping_province: 'Suphanburi',
                shipping_country_code: 'TH',
                shipping_postal_code: '72120',
                phone: '-',
                email: 'kang@mail.com',

                payment_method: 'credit_card',
                payment_gateway: 'fake_gateway',
                payment_card_id: 'fake_card_id',
                payment_return_uri: 'http://www.website.com'
            }
        });

        const updatedOrder = await order.$query();

        // then
        t.equal(res.statusCode, 200);
        t.equal(res.result, fakeCheckout);
        t.equal(updatedOrder.shipping_address, '53 m8');
        t.ok(updatedOrder.can_complete_checkout);
    });

}


main();
