const Joi = require('@hapi/joi');
const Boom = require('@hapi/boom');

const { Order } = require('../../models');
const CartService = require('../../services/cart');
const Payment = require('../../payment');

module.exports = {
    method: 'PATCH',
    path: '/api/checkout/{id}',
    options: {
        validate: {
            params: Joi.object({
                id: Joi.number().integer().min(1)
            }),
            payload: Joi.object({
                shipping_full_name: Joi.string(),
                shipping_address: Joi.string(),
                shipping_district: Joi.string(),
                shipping_province: Joi.string(),
                shipping_postal_code: Joi.string(),
                shipping_country_code: Joi.string().valid('TH'),

                phone: Joi.string(),
                email: Joi.string().email(),

                payment_method: Joi.string().valid('credit_card'),
                payment_gateway: Joi.string(),
                payment_card_id: Joi.string(),
                payment_return_uri: Joi.string()
            })
                .and(
                    // all these need to be present tegether
                    // so that shipping cost can be determined
                    'shipping_full_name',
                    'shipping_address',
                    'shipping_district',
                    'shipping_province',
                    'shipping_postal_code',
                    'shipping_country_code',
                    'phone',
                    'email'
                )
                .and(
                    'payment_method',
                    'payment_gateway',
                    'payment_card_id',
                    'payment_return_uri'
                )
                .min(1)
        }
    },
    handler: async (request, h) => {
        const orderId = request.params.id;

        const order = await Order.query()
              .findById(orderId)
              .throwIfNotFound();

        if (request.yar.id !== order.session_id) {
            throw Boom.unauthorized();
        }

        if (order.checkout_complete) {
            // cannot update complete checkout
            throw Boom.badRequest();
        }

        Object.assign(order, request.payload);

        if (request.payload.payment_method === 'credit_card') {
            order.payment_card_data = await Payment.validateCardId(
                request.payload.payment_gateway,
                request.payload.payment_card_id
            );
        }

        // put shipping calculation code here if needed

        // check if buyer fills in shipping and payment info
        if (order.shipping_full_name && order.payment_method) {
            order.can_complete_checkout = true;
        }

        await Order.query()
            .update(order)
            .where({
                id: orderId,
                checkout_complete: false // prevent updating complete checkout
            });

        const checkout = await CartService.getCheckout(orderId);
        return checkout;
    }
};
