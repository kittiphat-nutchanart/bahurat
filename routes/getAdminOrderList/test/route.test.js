const tap = require('tap');
const Joi = require('@hapi/joi');
const Knex = require('knex');
const objection = require('objection');

const Auth = require('../../../authentication');
const serverPromise = require('../../../server');
const schema = require("./../schema.js");
const knexConfigs = require('../../../knexfile.js');
const SampleRecords = require('../../../models/sample.js');
const {
    Order
} = require('../../../models');
const TestHelper = require('../../../services/testHelper');

async function main() {
    const server = await serverPromise;

    tap.test('can get a list of orders for admin', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });

        await Order.query().insert({
            ...SampleRecords.Order,
            checkout_complete: true,
            payment_method: 'credit_card',
            paid_at: new Date().toISOString(),
            session_id: '16fd2706-8baf-433b-82eb-8c7fada847da',
            email: 'hello@mail.com',
            line_total: 100,
            shipping_cost: 0,
            total: 100
        });

        await Order.query().insert({
            ...SampleRecords.Order,
            checkout_complete: true,
            order_status: 'archived',
            payment_method: 'bank_transfer',
            paid_at: new Date().toISOString(),
            session_id: '16fd2706-8baf-433b-82eb-8c7fada847da',
            email: 'hello@mail.com',
            line_total: 100,
            shipping_cost: 0,
            total: 100
        });

        // when
        const res = await server.inject({
            method: 'GET',
            url: '/api/admin/orders?page=1&size=20&payment_status=paid',
            auth: {
                strategy: 'session',
                credentials: Auth.mockCredentials({ id: 1}, ['admin'])
            }
        });

        // then
        t.equal(res.statusCode, 200);
        t.equal(res.result.orders.length, 2);
        Joi.assert(res.result, schema, 'the res obeys the schema', { presence: "required" });
    });

}

main();
