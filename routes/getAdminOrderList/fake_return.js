const chance = require('chance').Chance();
const R = require('ramda');

module.exports = () => {
    const makeOrder = () => {
        return {
            id: chance.integer(),
            order_status: chance.pickone(['open', 'archived']),
            payment_status: chance.pickone(['paid', 'unpaid']),
            fulfillment_status: chance.pickone(['fulfilled', 'unfulfilled']),
            created_at: chance.date()
        };
    };

    const orders = chance.bool({ likelihood: 20 }) ?
          [] :
          R.map(makeOrder, R.range(1, chance.integer({ min: 2, max: 21 })));

    return {
        orders,
        page_count: chance.integer({min: 1, max: 100})
    };
};
