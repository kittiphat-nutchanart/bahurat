const Joi = require('@hapi/joi');

module.exports = Joi.object({
    orders: Joi.array().items(
        Joi.object({
            id: Joi.number(),
            order_status: Joi.string(),
            payment_status: Joi.string(),
            fulfillment_status: Joi.string(),
            created_at: Joi.date()
        })
    ),
    page_count: Joi.number()
});
