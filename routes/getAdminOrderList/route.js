const Joi = require('@hapi/joi');
const R = require('ramda');

const { Order } = require('../../models');
const { project } = require('../../models/projection.js');

module.exports = {
    method: 'GET',
    path: '/api/admin/orders',
    options: {
        auth: {
            strategy: 'session',
            access: {
                scope: 'admin'
            }
        },
        validate: {
            query: Joi.object({
                page: Joi.number().required(),
                size: Joi.number().integer().min(1).max(100).default(25),

                order_status: Joi.string().valid('open', 'archived'),
                payment_status: Joi.string().valid('unpaid', 'paid'),
                fulfillment_status: Joi.string().valid('unfulfilled', 'fulfilled')
            })
        }
    },
    handler: async (request, h) => {

        const whereArgs = R.pick(
            ['order_status', 'fulfillment_status'],
            request.query
        );

        const ordersQuery = Order.query()
              .where(whereArgs)
              .where((builder => {
                  if (request.query.payment_status === 'paid') {
                      builder.whereNotNull('paid_at')
                  } else if (request.query.payment_status === 'unpaid') {
                      builder.whereNull('paid_at')
                  }
              }))
              .andWhere({ checkout_complete: true });

        const orders = await ordersQuery
              .offset((request.query.page - 1) * request.query.size)
              .limit(request.query.size)
              .orderBy('id', 'desc');

        const orderCount = await ordersQuery.resultSize();
        const page_count = Math.ceil(orderCount / request.query.size);

        const schema = {
            orders: {
                id: 1,
                order_status: 1,
                payment_status: 1,
                fulfillment_status: 1,
                created_at: 1
            },
            page_count: 1
        };

        return project({ orders, page_count }, schema);
    }
};
