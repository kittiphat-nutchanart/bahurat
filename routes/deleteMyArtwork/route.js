const Joi = require('@hapi/joi');
const { Artwork } = require('../../models');
const Boom = require('@hapi/boom');

module.exports = {
    method: 'DELETE',
    path: '/api/me/artwork/{id}',
    options: {
        auth: 'session',
        validate: {
            params: Joi.object({
                id: Joi.number().integer().min(1)
            })
        }
    },
    handler: async (request, h) => {
        const artwork = await Artwork.query()
              .findById(request.params.id);

        if (!artwork) {
            throw Boom.notFound();
        }

        if (artwork.author_id !== request.auth.credentials.user.id) {
            throw Boom.forbidden();
        }

        await artwork.$query().update({ deleted: true });

        return h.continue;
    }
};
