const tap = require('tap');
const Knex = require('knex');
const objection = require('objection');

const serverPromise = require('../../../server');
const knexConfigs = require('../../../knexfile.js');
const SampleRecords = require('../../../models/sample.js');
const { Artwork } = require('../../../models');
const Auth = require('../../../authentication');
const TestHelper = require('../../../services/testHelper');

async function main() {
    const server = await serverPromise;

    tap.test('author can delete their artwork', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });

        await Artwork.query().insert({
            ...SampleRecords.Artwork,
            author_id: 1
        });

        // when
        const res = await server.inject({
            method: 'DELETE',
            url: '/api/me/artwork/1',
            auth: {
                strategy: 'session',
                credentials: Auth.mockCredentials({ id: 1})
            },
            payload: {
                title: 'yass'
            }
        });

        const artwork = await Artwork.query().findById(1);

        //then
        t.equal(res.statusCode, 204);
        t.ok(artwork.deleted);
    });
}


main();
