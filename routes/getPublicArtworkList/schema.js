const Joi = require('@hapi/joi');

module.exports = Joi.object({
    artworks: Joi.array().items(
        Joi.object({
            id: Joi.number(),
            title: Joi.string(),
            _url: Joi.string(),
            px_width: Joi.number(),
            px_height: Joi.number(),
            author: Joi.object({
                username: Joi.string()
            })
        })
    ),
    page_count: Joi.number()
});
