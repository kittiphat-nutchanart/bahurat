const tap = require('tap');
const Joi = require('@hapi/joi');
const Knex = require('knex');
const objection = require('objection');

const serverPromise = require('../../../server');
const schema = require("./../schema.js");
const knexConfigs = require('../../../knexfile.js');
const SampleRecords = require('../../../models/sample.js');
const {
    User,
    Artwork,
    UploadImg
} = require('../../../models');
const TestHelper = require('../../../services/testHelper');


async function main() {
    const server = await serverPromise;

    tap.test('route responds correctly 200', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });

        await User.query().insert({
            ...SampleRecords.User,
            id: 288,
            username: 'yoyo',
            email: 'hello@mail.com',
        });

        await Artwork.query().insert({
            ...SampleRecords.Artwork,
            id: 487,
            author_id: 288,
            title: 'artwork_1',
            image_id: (await UploadImg.query().insert({
                ...SampleRecords.UploadImg
            }).then(i => i.id)),
            visible: true
        });

        // when
        const res = await server.inject({
            method: 'GET',
            url: '/api/artworks?page=1&sort=new',
        });

        // then
        t.equal(res.statusCode, 200, 'returns 200 ok');
        Joi.assert(res.result, schema, 'the res obeys the schema', {presence: "required"});
        t.equal(res.result.artworks[0].author.username, 'yoyo', 'username matched');
        //t.ok(false, 'includes given/inserted artworks');
        //t.ok(false, 'no blocked artworks');
        //t.ok(false, 'no in-visible artworks');
    });

    tap.test('author specified', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });

        await User.query().insert({
            ...SampleRecords.User,
            id: 288,
            username: 'yoyo',
            email: 'hello@mail.com',
        });

        await Artwork.query().insert({
            ...SampleRecords.Artwork,
            id: 487,
            author_id: 288,
            title: 'artwork_1',
            image_id: (await UploadImg.query().insert({
                ...SampleRecords.UploadImg
            }).then(i => i.id)),
            visible: true
        });

        // when
        const res = await server.inject({
            method: 'GET',
            url: '/api/artworks?page=1&sort=new&author=not_yoyo',
        });

        // then
        t.equal(res.result.artworks.length, 0, 'username matched');
    });
}


main();
