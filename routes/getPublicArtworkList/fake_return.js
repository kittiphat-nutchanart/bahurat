// Load and instantiate Chance
const chance = require('chance').Chance();
const R = require('ramda');

module.exports = () => {
    const makeArtwork = () => {
        const px_width = chance.integer({min:200,max:800});
        const px_height = chance.integer({min:200,max:800});
        return {
            id: chance.integer(),
            title: chance.sentence({words: 3}),
            _url: `https://picsum.photos/${px_width}/${px_height}?random=${chance.integer()}`,
            px_width,
            px_height,
            author: {
                username: chance.word()
            }
        };
    };
    const artworks = chance.bool({likelihood: 80}) ?
          R.map(makeArtwork, R.range(1, 20)) :
          [];

    return {
        artworks,
        page_count: chance.integer({min: 1, max: 100})
    };
};
