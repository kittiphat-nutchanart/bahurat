const Joi = require('@hapi/joi');
const R = require('ramda');

const { Artwork, User, Tag } = require('../../models');
const Storage = require('../../storage');
const { project } = require('../../models/projection.js');

module.exports = {
    method: 'GET',
    path: '/api/artworks',
    options: {
        validate: {
            query: Joi.object({
                author: Joi.string().optional(),
                tag: Joi.string().optional(),
                page: Joi.number().required(),
                sort: Joi.string().valid('new', 'popular').required(),
                size: Joi.number().integer().min(1).max(100).default(25)
            }).oxor('tag', 'author')
        }
    },
    handler: async (request, h) => {
        let artworksQuery = Artwork.query().publicVisible();

        if (request.query.author) {
            artworksQuery = artworksQuery.andWhere(
                'author_id',
                User.query().where('username', request.query.author).select('id')
            );
        } else if (request.query.tag) {
            artworksQuery = artworksQuery.select('artworks.*')
                .innerJoin('artworks_to_tags', 'artworks_to_tags.artwork_id', 'artworks.id')
                .andWhere(
                    'artworks_to_tags.tag_id',
                    Tag.query().select('id').where('name', request.query.tag)
                );
        }

        let orderByArgs;
        if (request.query.tag) {
            // ignore request.query.sort
            // use "new" sort in every case
            orderByArgs = ['artworks_to_tags.artwork_id', 'desc'];
        } else {
            if (request.query.sort === 'new') {
                orderByArgs = ['id', 'desc'];
            } else if (request.query.sort === 'popular') {
                orderByArgs = ['score', 'desc'];
            }
        }

        const artworksPromise = artworksQuery
              .offset(R.dec(request.query.page) * request.query.size)
              .limit(request.query.size)
              .orderBy(...orderByArgs)
              .withGraphFetched('[author, image]');

        const rowCountPromise = artworksQuery.resultSize();

        const [artworks, rowCount] = await Promise.all([
            artworksPromise,
            rowCountPromise
        ]);

        const page_count = Math.ceil(rowCount / request.query.size);

        artworks.forEach((artwork) => {
            artwork._url = Storage.getImgURL(artwork.image.path, {
                maxWidth: Storage.DimenM,
                maxHeight: Storage.DimenS
            });
        });

        const schema = {
            id: 1,
            title: 1,
            _url: 1,
            px_width: 1,
            px_height: 1,
            author: {
                username: 1
            }
        };
        const res = {
            artworks: project(artworks, schema),
            page_count
        };

        return res;
    }
};
