const Joi = require('@hapi/joi');
const { OverlaidProduct } = require('../../models');
const Boom = require('@hapi/boom');

const Storage = require('../../storage');

module.exports = {
    method: 'PATCH',
    path: '/api/me/overlaid_product/{id}',
    options: {
        auth: 'session',
        validate: {
            params: Joi.object({
                id: Joi.number().integer().min(1)
            }),
            payload: Joi.object({
                artwork_mm_width: Joi.number().min(1).integer(),
                artwork_mm_top: Joi.number().min(0).integer(),
                artwork_mm_left: Joi.number().min(0).integer(),
                default_base_product_color_id: Joi.number().integer(),
                visible: Joi.boolean(),
                profit: Joi.number().min(0).integer()
            }).and('artwork_mm_width', 'artwork_mm_top', 'artwork_mm_left').min(1)
        }
    },
    handler: async (request, h) => {
        const overlaid_product = await OverlaidProduct.query()
              .findById(request.params.id)
              .withGraphFetched('[artwork, base_product]');

        if (!overlaid_product) {
            throw Boom.notFound();
        }

        if (overlaid_product.author_id !== request.auth.credentials.user.id) {
            throw Boom.forbidden();
        }

        if (request.payload.artwork_mm_width) {
            const {
                artwork_mm_width,
                artwork_mm_top,
                artwork_mm_left
            } = request.payload;

            const {
                artwork_px_width,
                artwork_px_height
            } = overlaid_product.artwork;

            const {
                frame_mm_width,
                frame_mm_height
            } = overlaid_product.base_product;

            const MM_IN_AN_INCH = 25.4;
            const dpi = (artwork_px_width / artwork_mm_width) * MM_IN_AN_INCH;
            if (dpi < Storage.MIN_DPI) {
                throw Boom.badRequest('dpi is too low');
            }

            const artwork_mm_height =
                  artwork_mm_width * (artwork_px_height / artwork_px_width);
            const params = {
                artwork_mm_width,
                artwork_mm_height,
                artwork_mm_left,
                artwork_mm_top,
                frame_mm_width,
                frame_mm_height
            };
            const isInside = Storage.artworkIsInside(params);
            if (!isInside) {
                throw Boom.badRequest('artwork is outside printing area');
            }
        }

        await OverlaidProduct.query()
            .findById(request.params.id)
            .patch(request.payload);

        return h.continue;
    }
};
