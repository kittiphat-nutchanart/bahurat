const tap = require('tap');
const Knex = require('knex');
const objection = require('objection');

const serverPromise = require('../../../server');
const knexConfigs = require('../../../knexfile.js');
const {
    Artwork,
    BaseProduct,
    OverlaidProduct
} = require('../../../models');
const SampleRecords = require('../../../models/sample.js');
const Auth = require('../../../authentication');
const TestHelper = require('../../../services/testHelper');

async function main() {

    // init knex and tell objection.js to use it
    const knex = Knex(knexConfigs.testing);
    objection.Model.knex(knex);
    const server = await serverPromise;


    tap.test('ok 200', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });

        await BaseProduct.query().insert({
            ...SampleRecords.BaseProduct,
            //id: 1,
            name: 'pink T-shirt',
            can_choose_color: true,
            has_second_option: true,
            bg_px_side_length: 1000,
            frame_percent_left: .324,
            frame_percent_top: .192,
            frame_percent_width: .352,
            frame_mm_width: 250,
            frame_mm_height: 300,
        });

        await Artwork.query().insert({
            ...SampleRecords.Artwork,
            author_id: 1,
            title: 'artwork_title',
            image_id: 1
        });

        await OverlaidProduct.query().insert({
            ...SampleRecords.OverlaidProduct,
            //id: 1,
            base_product_id: 1,
            artwork_id: 1,
            author_id: 1,
            artwork_mm_width: 200,
            artwork_mm_top: 0,
            artwork_mm_left: 0,
            default_base_product_color_id: 1,
            profit: 0,
            visible: true
        });

        // when
        const res = await server.inject({
            method: 'PATCH',
            url: '/api/me/overlaid_product/1',
            auth: {
                strategy: 'session',
                credentials: Auth.mockCredentials({ id: 1})
            },
            payload: {
                artwork_mm_width: 200,
                artwork_mm_top: 0,
                artwork_mm_left: 0
            }
        });

        //then
        t.equal(res.statusCode, 204, 'returns 200 ok');
    });

    tap.test('400 fails to set enabled = true when no enabled colors', async (t) => {
    });

    tap.test('400 badRequest when update property that is not allowed', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });

        // when
        const res = await server.inject({
            method: 'PATCH',
            url: '/api/me/overlaid_product/1',
            auth: {
                strategy: 'session',
                credentials: Auth.mockCredentials({ id: 1})
            },
            payload: {
                i_am: 'yass'
            }
        });


        //then
        t.equal(res.statusCode, 400, 'returns 400 ok');
    });
}


main();
