const Joi = require('@hapi/joi');

module.exports = Joi.object({
    overlaid_products: Joi.array().items(
        Joi.object({
            id: Joi.number(),
            _img_url: Joi.string(),
            author: Joi.object({
                username: Joi.string(),
            }),
            base_product: Joi.object({
                name: Joi.string(),
            }),
            artwork: Joi.object({
                title: Joi.string(),
            })
        })
    ),
    page_count: Joi.number()
});
