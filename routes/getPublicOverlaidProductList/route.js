const schema = require('./schema.js');
const Joi = require('@hapi/joi');
const R = require('ramda');

const { User, OverlaidProduct } = require('../../models');
const Storage = require('../../storage');

module.exports = {
    method: 'GET',
    path: '/api/overlaid_products',
    options: {
        validate: {
            query: Joi.object({
                author: Joi.string().optional(),
                artwork: Joi.number().optional(),
                page: Joi.number().required(),
                sort: Joi.string().valid('new', 'popular').required(),
                size: Joi.number().integer().min(1).max(100).default(25)
            })
        }
    },
    handler: async (request, h) => {
        let overlaidProductsQuery = OverlaidProduct.query().where(OverlaidProduct.visibleToPublic);

        if (request.query.author) {
            overlaidProductsQuery = overlaidProductsQuery.andWhere(
                'author_id',
                User.query().where('username', request.query.author).select('id')
            );
        } else if (request.query.artwork) {
            // fix
            // need test
            overlaidProductsQuery = overlaidProductsQuery.andWhere(
                'artwork_id',
                request.query.artwork
            );
        }

        const orderByArgs = ({
            new: ['id', 'desc'],
            popular: ['score', 'desc']
        })[request.query.sort];

        const overlaidProductsPromise = overlaidProductsQuery
              .offset(R.dec(request.query.page) * request.query.size)
              .limit(request.query.size)
              .orderBy(...orderByArgs)
              .withGraphFetched('[author, base_product, artwork.image, default_base_product_color]');

        const rowCountPromise = overlaidProductsPromise.resultSize();

        const [overlaid_products, rowCount] = await Promise.all([
            overlaidProductsPromise,
            rowCountPromise
        ]);

        const page_count = Math.ceil(rowCount/request.query.size);

        overlaid_products.forEach((overlaidProduct) => {
            const opts = { size: Storage.DimenS };
            overlaidProduct._img_url = Storage.getOverlaidProductImgURL(
                overlaidProduct.imgParams,
                opts
            );
        });

        return Joi.attempt(
            { overlaid_products, page_count },
            schema,
            {stripUnknown: true, presence: 'required'}
        );

    }
};
