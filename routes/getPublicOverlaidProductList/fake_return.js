// Load and instantiate Chance
const chance = require('chance').Chance();
const R = require('ramda');

module.exports = () => {
    const makeOverlaidProduct = () => ({
        id: chance.integer(),
        _img_url: `https://picsum.photos/200?random=${chance.integer()}`,
        author: {
            username: chance.word()
        },
        base_product: {
            name: chance.sentence({word: 4})
        },
        artwork: {
            title: chance.sentence({word: 4})
        }
    });

    const overlaid_products = chance.bool({likelihood: 20}) ?
          [] :
          R.map(makeOverlaidProduct, R.range(1, 21));
    return {
        overlaid_products,
        page_count: chance.integer({min: 1, max: 100})
    };
};
