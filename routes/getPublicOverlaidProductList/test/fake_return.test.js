const Joi = require('@hapi/joi');

const fakeReturn = require("./../fake_return.js");
const schema = require("./../schema.js");

const tap = require('tap');


function main() {
    tap.test("get_overlaid_product_list.fake_return obeys the schema", async t => {
        Joi.assert(fakeReturn(), schema, "fake_return obeys the schema", {presence: "required"});

    });
}

main();
