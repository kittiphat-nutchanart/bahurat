const tap = require('tap');
const Joi = require('@hapi/joi');
const Knex = require('knex');
const objection = require('objection');

const serverPromise = require('../../../server');
const schema = require("./../schema.js");
const knexConfigs = require('../../../knexfile.js');
const SampleRecords = require('../../../models/sample.js');
const {
    User,
    BaseProduct,
    BaseProductColor,
    Artwork,
    OverlaidProduct,
    UploadImg
} = require('../../../models');
const TestHelper = require('../../../services/testHelper');


async function main() {
    const server = await serverPromise;

    tap.test('route responds correctly 200', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });

        await User.query().insert({
            ...SampleRecords.User,
            id: 1,
            username: 'yoyo',
            email: 'hello@mail.com',
        });

        await Artwork.query().insert({
            ...SampleRecords.Artwork,
            id: 1,
            author_id: 288,
            title: 'artwork_1',
            image_id: 1,
            visible: true
        });

        await BaseProduct.query().insert({
            ...SampleRecords.BaseProduct,
            id: 1,
            name: 'product_1',
            can_choose_color: true,
            has_second_option: true,
            bg_px_side_length: 0,
            frame_percent_left: 0,
            frame_percent_top: 0,
            frame_percent_width: 0,
            frame_mm_width: 0,
            frame_mm_height: 0,
        });

        await UploadImg.query().insert({
            ...SampleRecords.UploadImg,
            id: 1,
            original_filename: 'hello.jpg',
            filesize: 999999,
            path: 'path/to/img',
            width: 1000,
            height: 1000,
            type: 'jpg',
            mime: 'image/jpeg',
            owner_id: 1
        });
        await BaseProductColor.query().insert({
            ...SampleRecords.BaseProductColor,
            id: 1,
            base_product_id: 1,
            name: 'blue',
            hex: '#0000ff',
            background_id: 1
        });

        await OverlaidProduct.query().insert({
            ...SampleRecords.OverlaidProduct,
            id: 1,
            base_product_id: 1,
            artwork_id: 1,
            author_id: 1,
            artwork_mm_width: 0,
            artwork_mm_top: 0,
            artwork_mm_left: 0,
            default_base_product_color_id: 1,
            profit: 4000,
            visible: true
        });


        // when
        const res = await server.inject({
            method: 'GET',
            url: '/api/overlaid_products?page=1&sort=new',
        });

        // then
        t.equal(res.statusCode, 200, 'returns 200 ok');
        Joi.assert(
            res.result,
            schema,
            'the res obeys the schema',
            { presence: "required" }
        );
    });

}


main();
