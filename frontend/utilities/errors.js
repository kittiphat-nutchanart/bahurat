import errors_th from '~/../errors/translations.th.json';

const errors = {
   locale: 'th',


    /**
     * @param {*} err - error thrown by axios which error's body should be in "hapi Boom" format
     */
    translateError(err) {
        const message = err?.response?.data?.message;

        if (!message) {
            return err;
        }

        let translations;
        if (this.locale === 'th') {
            translations = errors_th;
        }

        const translation = translations[message];

        if (!translation) {
            return err;
        }

        return translation;
    }
}

export default errors;