const axios_progress = {
    makeHandler(callback) {
      let progress = 0; // 0 - 1
      let fakeProgress = 0; // 0 - 1
      const progressSnapshots = [];
      let snapshotIdx = 0;

      const intervalId = setInterval(() => {
        if (progress === 1) {
            if (snapshotIdx >= progressSnapshots.length) {
                clearInterval(intervalId);
                return;
            }

            fakeProgress = progressSnapshots[snapshotIdx];
            snapshotIdx++;
        } else {
            progressSnapshots.push(progress);
        }

        const curProgress = (progress + fakeProgress) * 100 / 2;
        const niceProgress = curProgress > 99 ? 99 : Math.round(curProgress); // integer 0 - 99
        callback(niceProgress);
      }, 1000);

      return (event) => {
        progress = event.loaded / event.total; // 0 - 1
        console.log('progress event: ', progress);
      }
    }
}

export default axios_progress;