import svgToMiniDataURI from 'mini-svg-data-uri';
export default {
  methods: {
    blank_rectangle_svg_uri({width, height, color}) {
      return svgToMiniDataURI(`
        <svg xmlns="http://www.w3.org/2000/svg" width="${width}" height="${height}">
          <rect width="${width}" height="${height}" fill="${color}" />
        </svg>
      `);
    }
  }
}