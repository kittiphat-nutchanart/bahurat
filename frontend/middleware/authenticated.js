export default function ({ store, redirect }) {
  if (!store.state.loggedInUser) {
    return redirect('/login');
  }
};