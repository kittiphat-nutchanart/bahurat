import axios from 'axios';

export const state = () => ({
  isInited: false,
  loggedInUser: null,
  cartBtnVisible: false,
  developMode: false,
});

export const mutations = {
  set_loggedInUser(state, user) {
    state.loggedInUser = user;
  },
  set_cartBtnVisible(state, visible) {
    state.cartBtnVisible = visible;
  },
  set_developMode(state, active) {
    state.developMode = active;
  },
  set_isInited(state, isInited) {
    state.isInited = isInited;
  }
};

export const actions = {
  async initState({ commit, state }) {
    if (state.isInited) return;
    commit('set_isInited', true);

    try {
      const data = await axios.get('/api/me/account').then(res => res.data);

      if (data.logged_in) {
        commit('set_loggedInUser', data.account);
      }
      commit('set_cartBtnVisible', data.has_cart);
    } catch(err) {
      // use try/catch in case something went wrong with hapi session header
      // e.g. header gone missing, session.id missing etc.
      console.error(err);
    }
  }
};