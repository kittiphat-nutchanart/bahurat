const Boom = require('@hapi/boom');

class EmailTaken extends Boom {
    constructor() {
        const error = 'EMAIL_TAKEN';
        const message = error;
        super(message, { statusCode: 400 });
        this.output.payload.error = error;
    }
}

class UsernameTaken extends Boom {
    constructor() {
        const error = 'USERNAME_TAKEN';
        const message = error;
        super(message, { statusCode: 400 });
        this.output.payload.error = error;
    }
}

class NotFound extends Boom {
    constructor() {
        const error = 'NOT_FOUND';
        const message = error;
        super(message, { statusCode: 400 });
        this.output.payload.error = error;
    }
}
