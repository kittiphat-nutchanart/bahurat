const path = require('path');
const fs = require('fs');

const tap = require('tap');
const sinon = require('sinon');
const Joi = require('@hapi/joi');
const Knex = require('knex');
const objection = require('objection');
const AWS = require('aws-sdk');

const knexConfigs = require('../../knexfile.js');
const Storage = require('../index.js');
const { UploadImg } = require('../../models');
const TestHelper = require('../../services/testHelper');

async function main() {
    tap.test('getImgURL() works as expected', async (t) => {
        // given
        const objKey = 'artwork.png';
        const maxWidth = 300;
        const maxHeight = 200;
        const ThumborSecurityKey = 'MY_SECURE_KEY';
        const ThumborEndpoint = 'http://localhost:8888';

        const sinonSandbox = sinon.createSandbox();
        sinonSandbox.stub(Storage, 'ThumborSecurityKey').value(ThumborSecurityKey);
        sinonSandbox.stub(Storage, 'ThumborEndpoint').value(ThumborEndpoint);
        t.teardown(() => {
            sinonSandbox.restore();
        });

        const expectedURL = 'http://localhost:8888/Zl8tL2KGS-ddUk6j-xqU-3LgKUE=/opts:thumbnail(300,200)/artwork.png';

        // when
        const imgUrl = Storage.getImgURL(objKey, { maxWidth, maxHeight });

        // then
        t.equal(imgUrl, expectedURL, 'imgURL is computed correctly');
    });

    tap.test('getOriginalImgURL() works as expected', async (t) => {
        // given
        const objKey = 'rootdir/artwork.png';

        const sinonSandbox = sinon.createSandbox();
        t.teardown(() => {
            sinonSandbox.restore();
        });
        sinonSandbox.stub(process, 'env').value({
            ...process.env,
            IMG_ENDPOINT: 'http://imgendpoint.fake'
        });

        const expectedURL = 'http://imgendpoint.fake/rootdir/artwork.png';

        // when
        const imgUrl = Storage.getOriginalImgURL(objKey);

        // then
        t.equal(imgUrl, expectedURL);
    });

    tap.test('getOverlaidProductImgURL() works as expected', async (t) => {
        // given
        const imgParams = {
            artwrkPath: 'artwork.png',
            artwrkMilWidth: 200,
            artwrkMilTop: 0,
            artwrkMilLeft: 25,
            bgPath: 'just-T-shirt-pink.jpg',
            frmPrcntLeft: .324,
            frmPrcntTop: .192,
            frmPrcntWidth: .352,
            frmMilWidth: 250
        };
        const opts = { size: 1400 };

        const ThumborSecurityKey = 'MY_SECURE_KEY';
        const ThumborEndpoint = 'http://localhost:8888';

        const sinonSandbox = sinon.createSandbox();
        sinonSandbox.stub(Storage, 'ThumborSecurityKey').value(ThumborSecurityKey);
        sinonSandbox.stub(Storage, 'ThumborEndpoint').value(ThumborEndpoint);
        t.teardown(() => {
            sinonSandbox.restore();
        });

        const expectedURL = 'http://localhost:8888/9e5Py64cXVdCqtcvkhIiymN80Ls=/opts:watermark(artwork.png,0.35920,0.19200,0.28160):thumbnail(1400,1400)/just-T-shirt-pink.jpg';

        // when
        const imgUrl = Storage.getOverlaidProductImgURL(imgParams, opts);

        // then
        t.equal(imgUrl, expectedURL, 'imgURL is computed correctly');


    });

    tap.test('constants a set of static properties', async (t) => {
        t.ok(Storage.hasOwnProperty('ThumborEndpoint'), 'string', 'has ThumborEndpoint');
        t.ok(Storage.hasOwnProperty('ThumborSecurityKey'), 'string', 'has ThumborSecurityKey');

        t.equal(Storage.DimenS, 400, 'DimenS is 400');
        t.equal(Storage.DimenM, 800, 'DimenS is 800');
        t.equal(Storage.DimenL, 1200, 'DimenS is 1200');

        t.ok(Storage.s3 instanceof AWS.S3, 'has s3 instance');
        t.ok(Storage.hasOwnProperty('S3_BUCKET'), 'has S3_BUCKET');

        t.equal(Storage.MIN_DPI, 300, 'has S3_BUCKET');
    });

    tap.test('getDefaultPrintingParams() works as expected', async (t) => {
        const printingParamsSchema = Joi.object({
            artwork_mm_width: Joi.number(),
            artwork_mm_left: Joi.number(),
            artwork_mm_top: Joi.number()
        });

        const paramsList = [
            {
                frame_mm_width: 5 * 25.4,
                frame_mm_height: 8 * 25.4,
                artwork_px_width: 300,
                artwork_px_height: 700
            },
            {
                frame_mm_width: 8 * 25.4,
                frame_mm_height: 5 * 25.4,
                artwork_px_width: 300,
                artwork_px_height: 700
            },
            {
                frame_mm_width: 3 * 25.4,
                frame_mm_height: 8 * 25.4,
                artwork_px_width: 300,
                artwork_px_height: 700
            },
            {
                frame_mm_width: 8 * 25.4,
                frame_mm_height: 3 * 25.4,
                artwork_px_width: 300,
                artwork_px_height: 700
            },
            {
                frame_mm_width: 5 * 25.4,
                frame_mm_height: 8 * 25.4,
                artwork_px_width: 30000,
                artwork_px_height: 70000
            },
            {
                frame_mm_width: 8 * 25.4,
                frame_mm_height: 5 * 25.4,
                artwork_px_width: 30000,
                artwork_px_height: 70000
            },
            {
                frame_mm_width: 3 * 25.4,
                frame_mm_height: 8 * 25.4,
                artwork_px_width: 30000,
                artwork_px_height: 70000
            },
            {
                frame_mm_width: 8 * 25.4,
                frame_mm_height: 3 * 25.4,
                artwork_px_width: 30000,
                artwork_px_height: 70000
            }
        ];


        for (const params of paramsList) {
            const printingParams = Storage.getDefaultPrintingParams(params);

            const dpi = (params.artwork_px_width / printingParams.artwork_mm_width) * 25.4;
            const artwork_mm_height = printingParams.artwork_mm_width * (params.artwork_px_height / params.artwork_px_width);

            Joi.assert(
                printingParams,
                printingParamsSchema,
                'printingParams obeys schema',
                { presence: 'required' }
            );
            t.ok(dpi >= Storage.MIN_DPI, 'dpi meets the minimum');
            t.ok(
                printingParams.artwork_mm_width <= params.frame_mm_width,
                'artwork smaller than the frame width'
            );
            t.ok(
                artwork_mm_height <= params.frame_mm_height,
                'artwork smaller than the frame height'
            );
        }
    });

    tap.test('artworkIsInside() works as expected', async (t) => {
        t.test('artwork stays inside', async (t) => {
            // given
            const params = {
                artwork_mm_width: 100,
                artwork_mm_height: 200,
                artwork_mm_left: 0,
                artwork_mm_top: 0,
                frame_mm_width: 300,
                frame_mm_height: 250
            };

            // when
            const isInside = Storage.artworkIsInside(params);


            //then
            t.ok(isInside);
        });

        t.test('artwork goes beyond top border', async (t) => {
            // given
            const params = {
                artwork_mm_width: 100,
                artwork_mm_height: 200,
                artwork_mm_left: 0,
                artwork_mm_top: -10,
                frame_mm_width: 300,
                frame_mm_height: 250
            };

            // when
            const isInside = Storage.artworkIsInside(params);


            //then
            t.notOk(isInside);
        });

        t.test('artwork goes beyond left border', async (t) => {
            // given
            const params = {
                artwork_mm_width: 100,
                artwork_mm_height: 200,
                artwork_mm_left: -100,
                artwork_mm_top: 0,
                frame_mm_width: 300,
                frame_mm_height: 250
            };

            // when
            const isInside = Storage.artworkIsInside(params);


            //then
            t.notOk(isInside);
        });

        t.test('artwork goes beyond bottom border', async (t) => {
            // given
            const params = {
                artwork_mm_width: 100,
                artwork_mm_height: 241,
                artwork_mm_left: 0,
                artwork_mm_top: 10,
                frame_mm_width: 300,
                frame_mm_height: 250
            };

            // when
            const isInside = Storage.artworkIsInside(params);


            //then
            t.notOk(isInside);
        });

        t.test('artwork goes beyond right border', async (t) => {
            // given
            const params = {
                artwork_mm_width: 301,
                artwork_mm_height: 200,
                artwork_mm_left: 0,
                artwork_mm_top: 0,
                frame_mm_width: 300,
                frame_mm_height: 250
            };

            // when
            const isInside = Storage.artworkIsInside(params);


            //then
            t.notOk(isInside);
        });
    });

    tap.test('uploadImageFromLocal()', async (t) => {
        t.test('success and UploadImg is inserted', async (t) => {
            // given
            const teardown = await TestHelper.setupDB();
            t.teardown(async () => {
                await teardown();
            });

            const sinonSandbox = sinon.createSandbox();
            t.teardown(() => {
                sinonSandbox.restore();
            });

            const s3_bucket = 'my_bucket';

            sinonSandbox.stub(process, 'env').value({
                S3_BUCKET: s3_bucket
            });

            const createStreamSpy = sinonSandbox.spy(fs, 'createReadStream');

            const stubPutObject = sinonSandbox.stub(Storage.s3, 'putObject')
                .returns({
                    promise() {
                        return Promise.resolve(null);
                    }
                });

            const filenameWithoutExt = '150';
            const filename = filenameWithoutExt + '.png';
            const originalFilename = 'originalname.png';
            const localpath = path.join(__dirname, 'fixtures', filename);
            const ownerId = 1;
            const assetType = 'artworks';

            // when
            const img = await Storage.uploadImageFromLocal(
                localpath,
                originalFilename,
                ownerId,
                assetType
            );


            // then
            t.equal(img.original_filename, originalFilename);
            t.equal(img.filesize, 373);
            t.match(img.path, new RegExp(assetType + '\/.+\.png'));
            t.equal(img.width, 150);
            t.equal(img.height, 150);
            t.equal(img.type, 'png');
            t.equal(img.mime, 'image/png');
            t.equal(img.owner_id, ownerId);

            const putOpts = stubPutObject.firstCall.args[0];
            t.equal(putOpts.Key, img.path);
            t.equal(putOpts.Bucket, s3_bucket);
            t.equal(putOpts.ContentType, img.mime);
            t.ok(putOpts.Body instanceof fs.ReadStream);

            // for 1.) probeImageSize and 2.) upload to s3
            sinon.assert.calledTwice(createStreamSpy);

            sinon.assert.alwaysCalledWith(createStreamSpy, localpath);
        });

        t.test('deletes object when fails to insert', async (t) =>{
            // given
            const teardown = await TestHelper.setupDB();
            t.teardown(async () => {
                await teardown();
            });

            const sinonSandbox = sinon.createSandbox();
            t.teardown(() => {
                sinonSandbox.restore();
            });

            const s3_bucket = 'my_bucket';

            sinonSandbox.stub(process, 'env').value({
                S3_BUCKET: s3_bucket
            });
            sinonSandbox.stub(Storage.s3, 'putObject')
                .returns({
                    promise() {
                        return Promise.resolve(null);
                    }
                });

            const stubDeleteObject = sinonSandbox.stub(Storage.s3, 'deleteObject')
                  .returns({
                      promise() {
                          return Promise.resolve(null);
                      }
                  });

            sinonSandbox.stub(UploadImg, 'query')
                .returns({
                    insert() {
                        return Promise.reject(new Error());
                    }
                });

            const filenameWithoutExt = '150';
            const filename = filenameWithoutExt + '.png';
            const originalFilename = 'originalname.png';
            const localpath = path.join(__dirname, 'fixtures', filename);
            const ownerId = 1;
            const assetType = 'artworks';

            // when
            const error = await Storage.uploadImageFromLocal(
                localpath,
                originalFilename,
                ownerId,
                assetType
            ).catch(e => e);

            const deleteOpts = stubDeleteObject.firstCall.args[0];

            // then
            t.ok(error instanceof Error);
            t.match(deleteOpts.Key, new RegExp(assetType + '\/.+\.png'));
            t.equal(deleteOpts.Bucket, s3_bucket);
        });
    });

    tap.test('getLetterAvatarURL()', async (t) => {
        // given


        // when
        const url = await Storage.getLetterAvatarURL(
            'kangbomber',
            150
        );


        // then
        t.equal(url, 'https://ui-avatars.com/api/?uppercase=true&name=k&background=4dabf7&color=ffffff&size=150');
    });
}


main();
