const fs = require('fs');
const crypto = require('crypto');

const urljoin = require('url-join');
const AWS = require('aws-sdk');
const { nanoid } = require('nanoid');
const probeImageSize = require('probe-image-size');

const { UploadImg } = require('../models');

const s3_config = {};
if (process.env.S3_MINIO_ENDPOINT) {
    s3_config.endpoint = process.env.S3_MINIO_ENDPOINT;
    s3_config.s3ForcePathStyle = true;
}

module.exports = class Storage {
    static ThumborEndpoint = process.env.THUMBOR_ENDPOINT || 'localhost:8888';
    static ThumborSecurityKey = process.env.THUMBOR_SECURITY_KEY || 'SECURITY_KEY';

    // predefined fit-in dimensions for consistent sizing
    static DimenS = 400;
    static DimenM = 800;
    static DimenL = 1200;

    static s3 = new AWS.S3(s3_config);
    static S3_BUCKET = process.env.S3_BUCKET;

    static MIN_DPI = 300;

    /**
     * compute thumbor URL of an image
     * @param {string} key - s3 object key
     * @param {Object} opts - options for manipulating the image
     * @param {number} opts.maxWidth - the image's maxWidth in px leave undefined if no limit
     * @param {number} opts.maxHeight - the image's maxHeight in px leave undefined if no limit
     */
    static getImgURL(key, opts = {}) {
        const thumbnailOpt = (opts.maxWidth && opts.maxHeight) ?
              `:thumbnail(${opts.maxWidth},${opts.maxHeight})`
              : '' ;
        const unsignedPath = `opts${thumbnailOpt}/${key}`;
        const signedPath = this._thumborSign(unsignedPath);

        return urljoin(this.ThumborEndpoint, signedPath);
    }

    static getOriginalImgURL(key) {
        return urljoin(process.env.IMG_ENDPOINT, key);
    }

    /**
     * sign operations part of thumbor_URL
     * @param {string} unsignedPath - e.g. 'fit-in/100x100/path/to/img.jpg' note that it MUST NOT start with "/" i.e. use "fit-in/..." instead of "/fit-in/..."
     * @return {string} the unsignedPath prepended with its hash e.g. "some-hash/unsigned/path"
     */
    static _thumborSign(unsignedPath) {
        const hash = crypto.createHmac('sha1', this.ThumborSecurityKey)
              .update(unsignedPath)
              .digest('base64');


        /**
         * According to thumbor doc:
         *
         * base64.urlsafe_b64encode(s)
         * Encode string s using a URL-safe alphabet, which substitutes
         * - instead of + and _ instead of / in the standard Base64 alphabet.
         * The result can still contain =.
         */
        const urlsafeHash = hash.replace(/\+/g, '-').replace(/\//g, '_');

        return urlsafeHash + '/' + unsignedPath;
    }

    /**
     * @param {Object} imgParams
     * @param {string} imgParams.artwrkPath - s3 key of artwork
     * @param {number} imgParams.artwrkMilWidth - width of artwork in mm
     * @param {number} imgParams.artwrkMilTop - Y-axis distance of artwork's top-left relative to frame's top-left in mm
     * @param {number} imgParams.artwrkMilLeft - X-axis distance of artwork's top-left relative to frame's top-left in mm
     * @param {string} imgParams.bgPath - s3 key of background
     * @param {number} imgParams.frmPrcntLeft - Y-axis distance as percentage, b/w 0 and 1, relative to size
     * @param {number} imgParams.frmPrcntTop - X-axis distance as percentage, b/w 0 and 1, relative to size
     * @param {number} imgParams.frmPrcntWidth - frame's width as percentage, b/w 0 and 1, relative to size
     * @param {number} imgParams.frmMilWidth - frame's width in mm
     * @param {Object} opts - options
     * @param {number} opts.size - size in pixels indicating the dimensions of the square output image e.g. size = 100 -> will result in a 100x100 image
     */
    static getOverlaidProductImgURL(imgParams, opts) {

        const {
            artwrkPath,
            artwrkMilWidth,
            artwrkMilTop,
            artwrkMilLeft,
            bgPath,
            frmPrcntLeft,
            frmPrcntTop,
            frmPrcntWidth,
            frmMilWidth
        } = imgParams;

        const { size } = opts;

        const artworkRelativeX =
              frmPrcntLeft + ((artwrkMilLeft * frmPrcntWidth) / frmMilWidth);
        const artworkRelativeY =
              frmPrcntTop + ((artwrkMilTop * frmPrcntWidth) / frmMilWidth);
        const artworkRelativeWidth =
              (artwrkMilWidth * frmPrcntWidth) / frmMilWidth;

        const thumbnailOpt = 'thumbnail(' + size + ',' + size + ')';
        const watermarkOpt =
              'watermark(' +
              artwrkPath + ',' +
              artworkRelativeX.toFixed(5) + ',' +
              artworkRelativeY.toFixed(5) + ',' +
              artworkRelativeWidth.toFixed(5) + ')';
        const unsignedPath =
              'opts:' + watermarkOpt + ':' + thumbnailOpt + '/' + bgPath;
        const signedPath = this._thumborSign(unsignedPath);

        return urljoin(this.ThumborEndpoint, signedPath);
    }

    // fix
    // need to implement
    static getOPImgURLFromOP(overlaidProduct, opts) {
    }

    // fix
    // need test
    static getOPImgURLFromCartLine(cartLine, opts) {
        const imgParams = {};
        imgParams.artwrkPath = cartLine.overlaid_product.artwork.image.path;
        imgParams.artwrkMilWidth = cartLine.overlaid_product.artwork_mm_width;
        imgParams.artwrkMilTop = cartLine.overlaid_product.artwork_mm_top;
        imgParams.artwrkMilLeft = cartLine.overlaid_product.artwork_mm_left;
        imgParams.bgPath = cartLine.base_product_color.background.path;
        imgParams.frmPrcntLeft = cartLine.overlaid_product
            .base_product.frame_percent_left;
        imgParams.frmPrcntTop = cartLine.overlaid_product
            .base_product.frame_percent_top;
        imgParams.frmPrcntWidth = cartLine.overlaid_product
            .base_product.frame_percent_width;
        imgParams.frmMilWidth = cartLine.overlaid_product
            .base_product.frame_mm_width;

        return this.getOverlaidProductImgURL(imgParams, opts);
    }

    // fix
    // need test
    static getOPImgURLFromOrderLine(orderLine, opts) {
        const imgParams = {};
        imgParams.artwrkPath = orderLine.artwork.image.path;
        imgParams.artwrkMilWidth = orderLine.artwork_mm_width;
        imgParams.artwrkMilTop = orderLine.artwork_mm_top;
        imgParams.artwrkMilLeft = orderLine.artwork_mm_left;
        imgParams.bgPath = orderLine.base_product_color.background.path;
        imgParams.frmPrcntLeft = orderLine.base_product.frame_percent_left;
        imgParams.frmPrcntTop = orderLine.base_product.frame_percent_top;
        imgParams.frmPrcntWidth = orderLine.base_product.frame_percent_width;
        imgParams.frmMilWidth = orderLine.base_product.frame_mm_width;

        return this.getOverlaidProductImgURL(imgParams, opts);
    }

    /**
     * @param {Object} params
     * @param {number} params.frame_mm_width
     * @param {number} params.frame_mm_height
     * @param {number} params.artwork_px_width
     * @param {number} params.artwork_px_height
     * @returns {Object} printing params
     */
    static getDefaultPrintingParams(params) {
        const {
            frame_mm_width,
            frame_mm_height,
            artwork_px_width,
            artwork_px_height
        } = params;

        let artwork_mm_width = frame_mm_width;

        // check if the artwork overflows the frame
        const artwork_mm_height = artwork_mm_width * (artwork_px_height / artwork_px_width);
        if (artwork_mm_height > frame_mm_height) {
            const artwork_mm_height = frame_mm_height;
            artwork_mm_width = artwork_mm_height * (artwork_px_width / artwork_px_height);
        }

        // make sure that dpi meets the minimum
        const MM_IN_AN_INCH = 25.4;
        const dpi = (artwork_px_width / artwork_mm_width) * MM_IN_AN_INCH;
        if (dpi < this.MIN_DPI) {
            let factor = 2;
            while ((factor * dpi) < this.MIN_DPI) {
                factor *=  2;
            }
            artwork_mm_width /= factor;
        }

        const artwork_mm_top = 0;
        const artwork_mm_left = (frame_mm_width - artwork_mm_width) / 2;

        return {
            artwork_mm_width,
            artwork_mm_left,
            artwork_mm_top
        };
    }

    /**
     * @param {Object} params
     * @param {number} params.artwork_mm_width
     * @param {number} params.artwork_mm_height
     * @param {number} params.artwork_mm_left
     * @param {number} params.artwork_mm_top
     * @param {number} params.frame_mm_width
     * @param {number} params.frame_mm_height
     * @returns {boolean} returns true if artwork's inside, false otherwise
     */
    static artworkIsInside(params) {
        const {
            artwork_mm_width,
            artwork_mm_height,
            artwork_mm_left,
            artwork_mm_top,
            frame_mm_width,
            frame_mm_height
        } = params;

        if (artwork_mm_top < 0) {
            return false;
        }

        if (artwork_mm_left < 0) {
            return false;
        }

        if ((artwork_mm_left + artwork_mm_width) > frame_mm_width) {
            return false;
        }

        if ((artwork_mm_top + artwork_mm_height) > frame_mm_height) {
            return false;
        }

        return true;
    }

    /**
     * @param {string} localpath - path in local filesystem
     * @param {string} originalFilename
     * @param {number} ownerId - id of user who uploads
     * @param {('artworks'|'banners'|'avatars'|'products')} assetType
     */
    static async uploadImageFromLocal(localpath, originalFilename, ownerId, assetType) {
        const fileToProbe = fs.createReadStream(localpath);
        const imageMeta = await probeImageSize(fileToProbe);

        const allowedImageTypes = ['jpg', 'png'];
        if (!allowedImageTypes.includes(imageMeta.type)) {
            throw Error('image type not allowed');
        }


        const fileToUpload = fs.createReadStream(localpath);
        const s3_key = assetType + '/' + nanoid() + '.' + imageMeta.type;
        const putOpts = {
            Bucket: process.env.S3_BUCKET,
            Key: s3_key,
            Body: fileToUpload,
            ContentType: imageMeta.mime
        };
        await this.s3.putObject(putOpts).promise();

        const filesize = await fs.promises.stat(localpath).then(stats => stats.size);
        const uploadImg = {
            original_filename: originalFilename,
            filesize,
            path: s3_key,
            width: imageMeta.width,
            height: imageMeta.height,
            type: imageMeta.type,
            mime: imageMeta.mime,
            owner_id: ownerId
        };

        const newUploadImg = await UploadImg.query()
            .insert(uploadImg)
            .catch(async (e) => {
                // when putObject succeeds but fails to insert to database
                // it's very likely that deleteObject will also succeed
                // if errs, save to logfile dont let it reject
                const deleteOpts = {
                    Bucket: process.env.S3_BUCKET,
                    Key: s3_key,
                };
                await this.s3.deleteObject(deleteOpts)
                    .promise()
                    .catch(console.error);

                throw e;
            });

        return newUploadImg;
    }

    static getLetterAvatarURL(username, size) {
        const palette = [
            '#ff6b6b', // $oc-red-5
            '#f06595', // $oc-pink-5
            '#cc5de8', // $oc-grape-5
            '#9775fa', // $oc-violet-4
            '#748ffc', // $oc-indigo-4
            '#4dabf7', // $oc-blue-4
            '#22b8cf', // $oc-cyan-5
            '#0ca678', // $oc-teal-7
            '#40c057', // $oc-green-6
            '#94d82d', // $oc-lime-5
            '#ffd43b', // $oc-yellow-4
            '#fd7e14' // $oc-orange-6
        ];

        const letter = username[0]; // white letter

        const hexStr = crypto.createHash('md5')
            .update(username)
            .digest('hex')
            .substring(0, 8); // make sure that the number is smaller than max safe integer
        const randomInt = parseInt(hexStr, 16);
        const colorIndex = randomInt % palette.length;
        const bgColor = palette[colorIndex];

        const avatarUrl = `https://ui-avatars.com/api/?uppercase=true&name=${letter}&background=${bgColor.substring(1, 7)}&color=ffffff&size=${size}`;

        return avatarUrl;
    }
};
