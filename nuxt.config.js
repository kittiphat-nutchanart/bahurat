import pkg from "./package";

export default {
  ssr: true,
  server: {
    port: 3001,
    host: "0.0.0.0"
  },
  srcDir: "frontend/",
  ignore: process.env.NODE_ENV === 'production' ? 
    [
      "pages/debug/**/*.vue",
      "pages/admin/**/*.vue",
      "pages/cart.vue",
      "pages/checkout.vue",
      "pages/track-order.vue"
    ] : [],
  // privateRuntimeConfig only avaiable on serverside/ssr and is not exposed on frontend
  privateRuntimeConfig: {
    baseURL: process.env.ROOT_URL
  },
  /*
   ** Headers of the page
   */
  head: {
    titleTemplate: (title) => {
      return title ? `${title} - wadlen` 
        : (process.client && document.title) ? document.title
        : 'wadlen';
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: pkg.description }
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
      {
        rel: "stylesheet",
        href: "https://fonts.googleapis.com/css2?family=Sarabun:wght@300;700&display=swap"
      },
      //{
      //  rel: "stylesheet",
      //  href: "https://fonts.googleapis.com/icon?family=Material+Icons"
      //}
    ]
  },

  /*
   ** Customize the progress-bar color
   */
  loading: { color: "#fff" },

  /*
   ** Global CSS
   */
  css: [
    "@/styles/normalize.css",
    "@/styles/style.scss"
  ],

  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    { src: "~/plugins/vue-lazyload.js", mode: "client" },
    { src: "~/plugins/client-init-hook.js", mode: "client" },
    { src: "~/plugins/ssr-config.js", mode: "server" }
  ],

  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    //"@nuxtjs/axios"
    ['nuxt-i18n', { 
      strategy: 'prefix_except_default', 
      vueI18nLoader: true ,
      detectBrowserLanguage: false
    }],
    'portal-vue/nuxt',
    '@nuxtjs/dayjs'
  ],
  buildModules: ['@nuxtjs/svg'],
  /*
   ** Axios module configuration
   */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },

  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {}
  },
  serverMiddleware: [
    // enable ssr on specific pages
    function (req, res, next) {
      const crawlerAgent = /^facebook/
      const ssrPaths = [
        /^\/artwork\/.+/
      ];

      res.spa = true;
      if (req.headers['user-agent'] 
        && req.headers['user-agent'].match(crawlerAgent)
        && ssrPaths.some(reg => req.originalUrl.match(reg))
      ) {
        res.spa = false;
      }
      
      next();
    }
  ],
  router: {
    extendRoutes(routes, resolve) {
      routes.push({
        name: 'shop',
        path: '/shop/:username',
        component: resolve(__dirname, 'frontend/pages/user/_username.vue')
      });
    }
  },
  i18n: {
    locales: ['th', 'en'],
    defaultLocale: 'th'
  },
  dayjs: {
    locales: ['th', 'en'],
    defaultLocale: 'th',
    plugins: [
      'relativeTime'
    ]
  }
};
