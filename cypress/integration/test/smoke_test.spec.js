/// <reference types="cypress" />

context('smoke test', () => {
  context('no login', () => {
    it('visits homepage', () => {
      cy.visit('localhost');
    });

    it('navigates to individual artworks', () => {
      cy.get('[data-cy=artwork-link]').first().click();
      cy.location('pathname').should('eq', '/artwork/1');
    });

    it('navigates to author\'s page ', () => {
      cy.get('.cy-author-link').first().click();
      cy.location('pathname').should('eq', '/user/kangbomber');
    });

    it('navigate author\'s artwork', () => {
      cy.get('[data-cy=artwork-link]').first().click();
      cy.location('pathname').should('eq', '/artwork/1');
    });

    it('go back and navigates to author\'s shop', () => {
      cy.go('back');
      cy.get('.cy-shop-link').first().click({ force: true });
      cy.location('pathname').should('eq', '/shop/kangbomber');
    });

    it('navigate author\'s product', () => {
      cy.get('.cy-product-img').first().click();
      cy.location('pathname', { timeout: 10000 }).should('eq', '/product/1');
    });

    it('can signup', () => {
      cy.get('[data-cy=login-link]').click();
      cy.get('[data-cy=signup-link]').click();
      cy.get('[data-cy=username-input]', { timeout: 10000 }).type('naughty666');
      cy.get('[data-cy=email-input]').type('awesome_person@gmail.com');
      cy.get('[data-cy=password-input]').type('fktekfkjeoi38ku');
      cy.get('[data-cy=signup-confirm-btn]').click();
      cy.get('[data-cy=please-confirm-email]').should('be.visible');
    });

    it('can reset password when forget', () => {
      cy.visit('localhost');
      cy.get('[data-cy=login-link]').click();
      cy.get('[data-cy=forgot-password-link]').click();
      cy.get('[data-cy=email-input]').type('mail@email.com');
      cy.get('[data-cy=send-email-btn]').click();
      cy.get('[data-cy=please-check-email-msg]').click();
    });

    it('can activate new account', () => {
      cy.visit('http://localhost/activate-account?token=6f501e2e-3323-4134-8f97-f87a0daadca0');
      cy.get('[data-cy=account-activated-msg]', { timeout: 10000 }).should('be.visible');
    });

    context('shopping', () => {
      beforeEach(() => {
        // without this the cookies would be reset
        // at the beginning of every it('...')
        // and we wouldn't statyed logged in
        // and thus couldn't do things like, for example,
        // change our settings which requires authentication
        Cypress.Cookies.preserveOnce('sid', 'session');
      });

      beforeEach(() => {
        cy.visit('localhost/product/1');
        cy.get('[data-cy=add-to-cart-btn]').click();
        cy.get('[data-cy=item-added-msg]')
          .find('[data-cy=cart-link]').click();
        cy.location('pathname', { timeout: 20000 }).should('eq', '/cart');
      });

      it('can update/delete cart-item', () => {
        // update quantity
        cy.get('[data-cy=cart-item]').first().as('cartItem')
          .find('[data-cy=qty-select]').as('qty')
          .should('have.value', '1')
          .select('2');
        cy.wait(100); // waiting the overlay loading screen to kick in
        cy.get('@cartItem').should('be.visible');
        cy.reload();
        cy.get('@qty').should('have.value', '2');

        // delete item
        cy.get('@cartItem')
          .find('[data-cy=delete-btn]')
          .click();
        cy.get('[data-cy=cart-empty-msg]').should('be.visible');
        cy.reload();
        cy.get('[data-cy=cart-empty-msg]').should('be.visible');
      });

      it('can checkout', () => {
        // code here
      });
    });

    it('buyer can see their order\'s status', () => {
      cy.visit('localhost/track-order?id=1&token=hello');
      cy.get('.track-order-root').should('be.visible');
    });
  });

  context('logged in', () => {
    before(() => {
      cy.login();
    });

    beforeEach(() => {
      // without this the cookies would be reset
      // at the beginning of every it('...')
      // and we wouldn't statyed logged in
      // and thus couldn't do things like, for example,
      // change our settings which requires authentication
      Cypress.Cookies.preserveOnce('sid', 'session');
    });

    context('settings', () => {
      it('the settings page show up', () => {
        cy.visit('localhost/me/account');
        cy.get('.account-root').should('be.visible');
      });

      it('can edit about', () => {
        const aboutText = 'this is about me';
        cy.get('[data-cy=about-edit-btn]').click();
        cy.get('.slideout').should('exist');
        cy.get('[data-cy=about-input]').clear().type(aboutText);
        cy.get('[data-cy=slideout-btn]').click();
        cy.get('.slideout').should('not.exist');
        cy.reload();
        cy.get('[data-cy=about]').should('contain', aboutText);
      });

      it('can upload avatar', () => {
        cy.get('[data-cy=avatar]').then(avatar => {
          const avatarURL = avatar.prop('src');

          cy.get('[data-cy=avatar-edit-btn]').click();
          cy.get('.slideout').should('exist');
          cy.get('[data-cy=avatar-input]').attachFile('avatar.png');
          cy.get('[data-cy=avatar-img]', { timeout: 10000 }).should('be.visible');
          cy.get('[data-cy=slideout-btn]').click();
          cy.get('.slideout').should('not.exist');
          cy.reload();
          cy.get('[data-cy=avatar]').should('not.have.attr', 'src', avatarURL);
        });
      });

      it('can upload banner', () => {
        cy.get('[data-cy=banner]').then(banner => {
          const bannerURL = banner.prop('src');

          cy.get('[data-cy=banner-edit-btn]').click();
          cy.get('.slideout').should('exist');
          cy.get('[data-cy=banner-input]').attachFile('banner.jpg');
          cy.get('[data-cy=slideout-btn]').click();
          cy.get('.slideout').should('not.exist');
          cy.reload();
          cy.get('[data-cy=banner]').should('not.have.attr', 'src', bannerURL);
        });
      });
    });

    context('artworks', () => {
      it('can navigate to my artworks and see artwork\'s details', () => {
        cy.get('[data-cy=my-artworks-tab]').click();
        cy.get('[data-cy=artwork-item]').click();
        cy.location('pathname').should('eq', '/me/artwork/1');
      });

      context('individual artwork', () => {
        it('can edit artwork\'s title', () => {
          cy.get('[data-cy=edit-title-btn]').click();
          cy.get('.slideout').should('exist');
          cy.get('[data-cy=title-input]').clear().type('battlefield earth');
          cy.get('[data-cy=slideout-btn]').click();
          cy.get('.slideout').should('not.exist');
          cy.reload();
          cy.get('[data-cy=artwork-title]').should('contain', 'battlefield earth');
        });

        it('can toggle artwork\'s visibility', () => {
          cy.get('[data-cy=visible-switch]').should('have.class', 'toggled');
          cy.get('[data-cy=visible-switch]').click();
          cy.get('[data-cy=visible-switch]').should('not.have.class', 'toggled');
          cy.reload();
          cy.get('[data-cy=visible-switch]').should('not.have.class', 'toggled');
        });

        // develop feature
        // use click({ force: true }) on hidden element
        context('artwork\'s prints', () => {
          beforeEach(() => {
            cy.get('[data-cy=prints-tab]').as('printsTab').click({ force: true });
            cy.get('[data-cy=print-item]').first().as('firstPrint');
          });

          it('can toggle print\'s visibility', () => {
            cy.get('@firstPrint')
              .find('[data-cy=print-visible-switch]')
              .as('printSwitch')
              .should('have.class', 'toggled')
              .click()
              .should('not.have.class', 'toggled');
            cy.reload();
            cy.get('@printsTab').click({ force: true });
            cy.get('@printSwitch')
              .should('not.have.class', 'toggled');
          });

          it('can edit print\'s earnings', () => {
            cy.get('@firstPrint')
              .find('[data-cy=earnings-edit-btn]')
              .click();
            cy.get('[data-cy=earnings-input]').clear().type('100');
            cy.get('[data-cy=slideout-btn]').click();
            cy.get('.slideout').should('not.exist');
            cy.reload();
            cy.get('@printsTab').click({ force: true });
            cy.get('@firstPrint')
              .find('[data-cy=earnings]')
              .should('contain', '100');
          });

          it('can adjust artwork on print', () => {
            cy.get('@firstPrint')
              .find('[data-cy=print-edit-btn]')
              .click();
            cy.get('canvas.upper-canvas').should('exist');
            cy.get('[data-cy=size-slider]', { timeout: 10000 })
              .should('be.visible')
              .invoke('val', 20)
              .trigger('change');
            cy.get('[data-cy=slideout-btn]').click();
            cy.get('.slideout').should('not.exist');
          });
        });
      });
    });

    context('earnings', () => {
    // can see balance

    // can see earnings transactions

    // can see recently sold items
    });

    context('upload', () => {
      it('can upload new artwork', () => {
        cy.get('[data-cy=artwork-upload-link]').click();
        cy.get('[data-cy=artwork-input]').attachFile('artwork.jpg');
        cy.get('[data-cy=title-input]').clear().type('I love Pakdong');
        cy.get('[data-cy=slideout-btn]').click();
        cy.get('.slideout').should('not.exist');
        cy.location('pathname').should('eq', '/artwork/2');
      });
    });
  });

  // admin

  // can see all orders and filter status

  // can see individual order and "printer-view order"

  // can update status of an order be it archive-status, payment-status, fullfillment-status

  // can see list of base-products

  // can update an individual base-product e.g. visibility, availability, quantity, set default
  context('admin', () => {
    before(() => {
      cy.login();
    });

    beforeEach(() => {
      // without this the cookies would be reset
      // at the beginning of every it('...')
      // and we wouldn't statyed logged in
      // and thus couldn't do things like, for example,
      // change our settings which requires authentication
      Cypress.Cookies.preserveOnce('sid', 'session');
    });

    it('visit orders page', () => {
      cy.visit('localhost/admin/orders');
    });

    it('can see orders and use filters', () => {
      cy.get('[data-cy=order-item]').as('orderItem').should('be.visible');

      cy.get('[data-cy=order-status-filter]').select('open');
      cy.wait(500);
      cy.get('@orderItem').should('be.visible');

      cy.get('[data-cy=payment-status-filter]').select('paid');
      cy.wait(500);
      cy.get('@orderItem').should('be.visible');

      cy.get('[data-cy=fulfillment-status-filter]').select('unfulfilled');
      cy.wait(500);
      cy.get('@orderItem').should('be.visible');
    });

    it('can see order\'s details', () => {
      cy.get('[data-cy=order-details-link]').click();
      cy.location('pathname').should('eq', '/admin/order/1');
    });

    it('update archive status', () => {
      cy.get('[data-cy=order-status]').as('orderStatus').should('contain', 'open').click();
      cy.get('.slideout').should('be.visible').find('select').select('archived');
      cy.get('[data-cy=slideout-btn]').click();
      cy.get('.slideout').should('not.be.visible');
      cy.get('@orderStatus').should('contain', 'archived');
      cy.reload();
      cy.get('@orderStatus').should('contain', 'archived');
    });
      
    it('update payment status', () => {
      cy.get('[data-cy=payment-status]').as('paymentStatus').should('contain', 'paid').click();
      cy.get('.slideout')
        .should('be.visible')
        .find('select')
        .select('unpaid');
      cy.get('[data-cy=slideout-btn]').click();
      cy.get('.slideout').should('not.be.visible');
      cy.get('@paymentStatus').should('contain', 'unpaid');
      cy.reload();
      cy.get('@paymentStatus').should('contain', 'unpaid');
    });

    it('update fulfillment status', () => {
      cy.get('[data-cy=fulfillment-status]')
        .as('fulfillmentStatus')
        .should('contain', 'unfulfilled')
        .click();
      cy.get('.slideout')
        .should('be.visible')
        .find('select')
        .select('fulfilled');
      cy.get('[data-cy=slideout-btn]').click();
      cy.get('.slideout').should('not.be.visible');
      cy.get('@fulfillmentStatus').should('contain', 'fulfilled');
      cy.reload();
      cy.get('@fulfillmentStatus').should('contain', 'fulfilled');
    });

    it('visit admin/printer/order/:id', () => {
      cy.get('[data-cy=printer-link]').click();
      cy.location('pathname').should('eq', '/admin/printer/order/1');
      cy.get('[data-cy=printer-order-item]').first().should('be.visible');
      cy.go('back');
    });
    
    it('navigate to base-products', () => {
      cy.get('[data-cy=products-link]').click();
      cy.location('pathname').should('eq', '/admin/base-products');
    });

    it ('navigate to one of base-products\' details page', () => {
      cy.get('[data-cy=details-link]').click();
      cy.location('pathname').should('eq', '/admin/base-product/1');
    })

    context('play with base-product\'s settings', () => {
      it('toggle product\'s enable status', () => {
        cy.get('[data-cy=product-enable-switch]')
          .as('productSwitch')
          .should('have.class', 'toggled')
          .click()
          .should('not.have.class', 'toggled');
        cy.reload();
        cy.get('@productSwitch').should('not.have.class', 'toggled');
      });

      it('toggle color\'s enable status', () => {
        cy.get('[data-cy=color-enable-switch]')
          .first()
          .as('colorSwitch')
          .should('have.class', 'toggled')
          .click()
          .should('not.have.class', 'toggled');
        cy.reload();
        cy.get('@colorSwitch').should('not.have.class', 'toggled');
      });

      it('toggle second opt\'s enable status', () => {
        cy.get('[data-cy=second-opt-enable-switch]')
          .first()
          .as('secondOptSwitch')
          .should('have.class', 'toggled')
          .click()
          .should('not.have.class', 'toggled');
        cy.reload();
        cy.get('@secondOptSwitch').should('not.have.class', 'toggled');
      });
      
      context('product variant', () => {
        beforeEach(() => {
          cy.get('[data-cy=product-variant]').first().as('productVariant');
        });

        it('can edit base-cost', () => {
          cy.get('@productVariant').find('[data-cy=base-cost-edit-btn]').click();
          cy.get('.slideout').should('be.visible');
          cy.get('[data-cy=base-cost-input]').clear().type('350');
          cy.get('[data-cy=slideout-btn]').click();
          cy.get('.slideout').should('not.be.visible');
          cy.get('@productVariant').find('[data-cy=base-cost]').should('contain', '350');
          cy.reload();
          cy.get('@productVariant').find('[data-cy=base-cost]').should('contain', '350');
        });

        it('can edit quantity', () => {
          cy.get('@productVariant').find('[data-cy=qty-edit-btn]').click();
          cy.get('[data-cy=qty-input]').clear().type('8');
          cy.get('.slideout').should('be.visible');
          cy.get('[data-cy=slideout-btn]').click();
          cy.get('.slideout').should('not.be.visible');
          cy.get('@productVariant').find('[data-cy=qty]').should('contain', '8');
          cy.reload();
          cy.get('@productVariant').find('[data-cy=qty]').should('contain', '8');
        });
      });
    });
  });
});
