# bahurat
an artworks sharing platform

# start frontend
        $ cd bahurt
        $ npx nuxt

# start mock-up server
        $ cd bahurat
        $ node mock_server

# start real server
        $ cd bahurat
        $ node index.js

# start nginx
        $ cd bahurat/proxy
        $ nginx -c nginx_dev.conf

# use fixtures
        $ cd bahurat/fixtures
        
insert sample data

        $ sudo -u postgres dropdb fake_db
        $ sudo -u postgres createdb fake_db
        $ cd db_scripts
        $ node insertSampleData.js

fixture images
        
        # copy all the files over to some empty folder outside the project
        # so removing/creating files during development doesn't interfere with git
        $ cp s3_bucket/* bucket_container/fake_bucket
        
    

# use thumbor
during the development you use thumbor with images in bahurat/fixtures/s3_bucket

config file:

        # thumbor.conf

        # the following config is necessary to make thumbor work with the fixture iamges
        LOADER = 'thumbor.loaders.file_loader'
        FILE_LOADER_ROOT_PATH = '/home/kangbomber/bucket_container/fake_bucket'
        
start thumbor

        # start thumbor with the config above
        $ thumbor -c thumbor.conf
        
now you can use docker
        
        # copy .env file from 'bahurat/thumbor_docker/.env.template'
        # replace <env_path> with actual path to .env file 
        $ docker run -p 8888:8888 --env-file <env_path> --network k_network kangbomber/k-thumbor thumbor -l debug --use-environment true -d


# use minio
minio is used instead of s3 during the development b/c minio is s3-compatible

        # tell minio to start the server with bucket_container as it saving/serving location
        # see above to learn more about bucket_container
        $ minio server bucket_container

run in docker
        
        # first copy copy bahurat/fixtures/s3_bucket to outside the project e.g. to ~/minio_data
        # replace <data_location> below with the new location above e.g. $PWD/minio_data:/data
        $ docker run -p 9000:9000 -v <data_location>:/data --name k.minio --network k_network minio/minio server /data

# .env file
make sure you edit bahurat/.env file accordingly
e.g. setting database username/password, setting thumbor endpoint, setting minio/s3 endpoint

# pm2

        $ pm2 [start|restart|stop|delete] ecosystem.config.js
        
        # run command manually
        $ pm2 start "npm run start:api"

# set up database for testing

        $ sudo -u postgres psql -c "create user bahurat_test with password 'password' superuser;"
        $ sudo -u postgres psql -c 'create database bahurat_test with owner bahurat_test;'

# during development
Insert sample data and start server

        # This wipes out everything inserted previously
        $ node ./fixtures/db_scirpt/insertSampleData.js && node index.js

# apply latest migrations

        $ node ./migrate_latest.js