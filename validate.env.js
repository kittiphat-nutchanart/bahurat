require('dotenv').config({ path: require('path').join(__dirname, './.env') });

const Joi = require('@hapi/joi');
const { Model } = require('objection');
const Knex = require('knex');
const axios = require('axios');
const assert = require('assert').strict;

const knexConfigs = require('./knexfile.js');
const Mailer = require('./mailer');
const Storage = require('./storage');

async function main() {
    const schema = Joi.object({
        NODE_ENV: Joi.string().valid('production'),

        ROOT_URL: Joi.string(),

        PGUSER: Joi.string(),
        PGPASSWORD: Joi.string(),
        PGHOST: Joi.string(),
        PGDATABASE: Joi.string(),
        PGPORT: Joi.number(),

        COOKIE_PASSWORD: Joi.string().min(32),

        THUMBOR_ENDPOINT: Joi.string(),
        THUMBOR_SECURITY_KEY: Joi.string(),

        IMG_ENDPOINT: Joi.string(),

        AWS_ACCESS_KEY_ID: Joi.string(),
        AWS_SECRET_ACCESS_KEY: Joi.string(),
        AWS_REGION: Joi.string(),
        S3_BUCKET: Joi.string(),

        EMAIL_FROM_ADDRESS: Joi.string(),
        EMAIL_SERVER: Joi.string(),
        EMAIL_PORT: Joi.number(),
        EMAIL_USER: Joi.string(),
        EMAIL_PASS: Joi.string()
    }).prefs({ presence: 'required', allowUnknown: true });

    // validating environment variables
    Joi.assert(process.env, schema);
    assert.equal(process.env.S3_MINIO_ENDPOINT, undefined, 'process.env.S3_MINIO_ENDPOINT should be undefined');

    try{
        console.log('testing database connection');
        // give the knex instance to objection
        const knex = Knex(knexConfigs.default);
        await knex.raw('SELECT 1');
        const [, newMigrations] = await knex.migrate.list();
        await knex.destroy();
        if (newMigrations.length) {
            throw new Error(`unapplied migrations detected:\n${JSON.stringify(newMigrations, null, 4)}`);
        }
    } catch(err) {
        console.error('database connection failed');
        throw err;
    }

    try{
        console.log('testing email smtp connection');
        const mailOptions = {
            to: process.env.EMAIL_FROM_ADDRESS,
            from: process.env.EMAIL_FROM_ADDRESS,
            subject: 'just testing smtp connection',
            html: '',
            text: ''
        };
        await Mailer.transporter.sendMail(mailOptions);
    } catch(err) {
        console.error('email smtp connection failed');
        throw err;
    }

    try{
        console.log('testing s3 conneciton');
        const objParams = {
            Bucket: Storage.S3_BUCKET,
            Key: 'testing-img.jpeg'
        };
        // should not throw if exists
        await Storage.s3.headObject(objParams).promise();
    } catch(err) {
        console.error('s3 connection failed. make sure you copy fixtures/testing-img.jpeg to your bucket.');
        throw err;
    }


    try{
        console.log('testing thumbor');
        const imgOpts = {
            maxWidth: 100,
            maxHeight: 100
        };
        const imgURL = Storage.getImgURL('testing-img.jpeg', imgOpts);
        await axios.get(imgURL);
    } catch(err) {
        console.error('thumbor connection failed');
        throw err;
    }

    try{
        console.log('testing IMG_ENDPOINT');
        const imgURL = Storage.getOriginalImgURL('testing-img.jpeg');
        await axios.get(imgURL);
    } catch(err) {
        console.error('IMG_ENDPOINT connection failed');
        throw err;
    }

    console.log('validating .env success');
}

main().catch((err) => console.error(err) || process.exit(1));
