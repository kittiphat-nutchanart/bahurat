const { Model, QueryBuilder } = require('objection');

// fix
// cron set confirmation_token field when confirmed_at field pass 7 days
//
// fix
// cron delete unconfirmaed users after 7 days
class User extends Model {
    static get tableName() {
        return 'users';
    }

    static get relationMappings() {
        return {
            banner: {
                relation: Model.BelongsToOneRelation,
                modelClass: UploadImg,
                join: {
                    from: 'users.banner_id',
                    to: 'upload_imgs.id'
                }
            },
            avatar: {
                relation: Model.BelongsToOneRelation,
                modelClass: UploadImg,
                join: {
                    from: 'users.avatar_id',
                    to: 'upload_imgs.id'
                }
            },
            money_transactions: {
                relation: Model.HasManyRelation,
                modelClass: MoneyTransaction,
                join: {
                    from: 'users.id',
                    to: 'money_transactions.user_id'
                }
            }
        };
    }
}

class UploadImg extends Model {
    // Model property
    static tableName = 'upload_imgs';

}

class BaseProductColor extends Model {
    static get tableName() {
        return 'base_product_colors';
    }

    static get relationMappings() {
        return {
            background: {
                relation: Model.BelongsToOneRelation,
                modelClass: UploadImg,
                join: {
                    from: 'base_product_colors.background_id',
                    to: 'upload_imgs.id'
                }
            },
            base_product: {
                relation: Model.BelongsToOneRelation,
                modelClass: BaseProduct,
                join: {
                    from: 'base_product_colors.base_product_id',
                    to: 'base_products.id'
                }
            }
        };
    }

    static query(...args) {
        return super.query(...args).withGraphFetched('background');
    }

    get bg_path() {
        return this.background.path;
    }
}

class BaseProductSecondOption extends Model {
    static get tableName() {
        return 'base_product_second_options';
    }
}

class BaseProductVariant extends Model {
    static get tableName() {
        return 'base_product_variants';
    }

    static get relationMappings() {
        return {
            base_product: {
                relation: Model.BelongsToOneRelation,
                modelClass: BaseProduct,
                join: {
                    from: 'base_product_variants.base_product_id',
                    to: 'base_products.id'
                }
            },
            base_product_color: {
                relation: Model.BelongsToOneRelation,
                modelClass: BaseProductColor,
                join: {
                    from: 'base_product_variants.base_product_color_id',
                    to: 'base_product_colors.id'
                }
            },
            base_product_second_option: {
                relation: Model.BelongsToOneRelation,
                modelClass: BaseProductSecondOption,
                join: {
                    from: 'base_product_variants.base_product_second_option_id',
                    to: 'base_product_second_options.id'
                }
            }
        };
    }

}

class BaseProduct extends Model {
    static get tableName() {
        return 'base_products';
    }

    static get relationMappings() {
        return {
            base_product_colors: {
                relation: Model.HasManyRelation,
                modelClass: BaseProductColor,
                join: {
                    from: 'base_products.id',
                    to: 'base_product_colors.base_product_id'
                }
            },
            base_product_second_options: {
                relation: Model.HasManyRelation,
                modelClass: BaseProductSecondOption,
                join: {
                    from: 'base_products.id',
                    to: 'base_product_second_options.base_product_id'
                }
            },
            base_product_variants: {
                relation: Model.HasManyRelation,
                modelClass: BaseProductVariant,
                join: {
                    from: 'base_products.id',
                    to: 'base_product_variants.base_product_id'
                }
            }
        };
    }
}

class ArtworkQueryBuilder extends QueryBuilder {
    // visible to anyone in public gallery
    publicVisible() {
        return this.where({
            visible: true,
            blocked: false,
            deleted: false
        });
    }

    // visible to their owners.
    // artworks that authors delete should disappear
    authorVisible() {
        return this.where({
            deleted: false
        });
    }
}

class Artwork extends Model {
    static get tableName() {
        return 'artworks';
    }

    static get QueryBuilder() {
        return ArtworkQueryBuilder;
    }

    // fix use custom querybuilder instead e.g. Artwork.query().wherePublicVisible()
    // .where(Artwork.visibleToPublic)
    static visibleToPublic = {
        fake: true,
        visible: true,
        blocked: false
    };

    get path() {
        return this.image.path;
    }
    get px_width() {
        return this.image.width;
    }
    get px_height() {
        return this.image.height;
    }

    static get relationMappings() {
        return {
            author: {
                relation: Model.BelongsToOneRelation,
                modelClass: User,
                join: {
                    from: 'artworks.author_id',
                    to: 'users.id'
                }
            },
            image: {
                relation: Model.BelongsToOneRelation,
                modelClass: UploadImg,
                join: {
                    from: 'artworks.image_id',
                    to: 'upload_imgs.id'
                }
            },
            tags: {
                relation: Model.ManyToManyRelation,
                modelClass: Tag,
                join: {
                    from: 'artworks.id',
                    through: {
                        from: 'artworks_to_tags.artwork_id',
                        to: 'artworks_to_tags.tag_id'
                    },
                    to: 'tags.id'
                }
            },
            comments: {
                relation: Model.HasManyRelation,
                modelClass: Comment,
                join: {
                    from: 'artworks.id',
                    to: 'comments.artwork_id'
                }
            }
        };
    }
}

class ArtworkLike extends Model {
    static get tableName() {
        return 'artwork_likes';
    }
}

class Tag extends Model {
    static get tableName() {
        return 'tags';
    }

}

class ArtworkTag extends Model {
    static get tableName() {
        return 'artworks_to_tags';
    }
}

class Comment extends Model {
    static get tableName() {
        return 'comments';
    }

    static get relationMappings() {
        return {
            user: {
                relation: Model.BelongsToOneRelation,
                modelClass: User,
                join: {
                    from: 'comments.user_id',
                    to: 'users.id'
                }
            }
        };
    }
}

class OverlaidProduct extends Model {
    static get tableName() {
        return 'overlaid_products';
    }

    // fix use custom querybuilder instead e.g. Artwork.query().wherePublicVisible()
    // .where(OverlaidProduct.visibleToPublic)
    static visibleToPublic = {
        visible: true,
        blocked: false
    };

    static get relationMappings() {
        return {
            base_product: {
                relation: Model.BelongsToOneRelation,
                modelClass: BaseProduct,
                join: {
                    from: 'overlaid_products.base_product_id',
                    to: 'base_products.id'
                }
            },
            artwork: {
                relation: Model.BelongsToOneRelation,
                modelClass: Artwork,
                join: {
                    from: 'overlaid_products.artwork_id',
                    to: 'artworks.id'
                }
            },
            author: {
                relation: Model.BelongsToOneRelation,
                modelClass: User,
                join: {
                    from: 'overlaid_products.author_id',
                    to: 'users.id'
                }
            },
            default_base_product_color: {
                relation: Model.BelongsToOneRelation,
                modelClass: BaseProductColor,
                join: {
                    from: 'overlaid_products.default_base_product_color_id',
                    to: 'base_product_colors.id'
                }
            }
        };
    }

    get imgParams() {
        return {
            artwrkPath: this.artwork.path,
            artwrkMilWidth: this.artwork_mm_width,
            artwrkMilTop: this.artwork_mm_top,
            artwrkMilLeft: this.artwork_mm_left,
            bgPath: this.default_base_product_color.bg_path,
            frmPrcntLeft: this.base_product.frame_percent_left,
            frmPrcntTop: this.base_product.frame_percent_top,
            frmPrcntWidth: this.base_product.frame_percent_width,
            frmMilWidth: this.base_product.frame_mm_width
            //size
        };
    }
}

class CartLine extends Model {
    static get tableName() {
        return 'cart_lines';
    }

    static get relationMappings() {
        return {
            overlaid_product: {
                relation: Model.BelongsToOneRelation,
                modelClass: OverlaidProduct,
                join: {
                    from: 'cart_lines.overlaid_product_id',
                    to: 'overlaid_products.id'
                }
            },
            base_product_color: {
                relation: Model.BelongsToOneRelation,
                modelClass: BaseProductColor,
                join: {
                    from: 'cart_lines.base_product_color_id',
                    to: 'base_product_colors.id'
                }
            },
            base_product_second_option: {
                relation: Model.BelongsToOneRelation,
                modelClass: BaseProductSecondOption,
                join: {
                    from: 'cart_lines.base_product_second_option_id',
                    to: 'base_product_second_options.id'
                }
            },
            base_product_variant: {
                relation: Model.BelongsToOneRelation,
                modelClass: BaseProductVariant,
                join: {
                    from: [
                        'cart_lines.base_product_id',
                        'cart_lines.base_product_color_id',
                        'cart_lines.base_product_second_option_id',
                    ],
                    to: [
                        'base_product_variants.base_product_id',
                        'base_product_variants.base_product_color_id',
                        'base_product_variants.base_product_second_option_id',
                    ]
                }
            },
        };
    }
}

// fix
// add partial index for searching orders
// e.g. orders where paid and not fullfilled
class Order extends Model {
    static get tableName() {
        return 'orders';
    }

    static get relationMappings() {
        return {
            lines: {
                relation: Model.HasManyRelation,
                modelClass: OrderLine,
                join: {
                    from: 'orders.id',
                    to: 'order_lines.order_id'
                }
            },
            logs: {
                relation: Model.HasManyRelation,
                modelClass: OrderLog,
                join: {
                    from: 'orders.id',
                    to: 'order_logs.order_id'
                }
            },
        };
    }

    get payment_status() {
        return this.paid_at ? 'paid' : 'unpaid';
    }
}

class OrderLine extends Model {
    static get tableName() {
        return 'order_lines';
    }

    static get relationMappings() {
        return {
            base_product: {
                relation: Model.BelongsToOneRelation,
                modelClass: BaseProduct,
                join: {
                    from: 'order_lines.base_product_id',
                    to: 'base_products.id'
                }
            },
            base_product_color: {
                relation: Model.BelongsToOneRelation,
                modelClass: BaseProductColor,
                join: {
                    from: 'order_lines.base_product_color_id',
                    to: 'base_product_colors.id'
                }
            },
            base_product_second_option: {
                relation: Model.BelongsToOneRelation,
                modelClass: BaseProductSecondOption,
                join: {
                    from: 'order_lines.base_product_second_option_id',
                    to: 'base_product_second_options.id'
                }
            },
            artwork: {
                relation: Model.BelongsToOneRelation,
                modelClass: Artwork,
                join: {
                    from: 'order_lines.artwork_id',
                    to: 'artworks.id'
                }
            },
            order: {
                relation: Model.BelongsToOneRelation,
                modelClass: Order,
                join: {
                    from: 'order_lines.order_id',
                    to: 'orders.id'
                }
            }
        };
    }
}

class OrderLog extends Model {
    static get tableName() {
        return 'order_logs';
    }

    static get relationMappings() {
        return {
            user: {
                relation: Model.BelongsToOneRelation,
                modelClass: User,
                join: {
                    from: 'order_logs.user_id',
                    to: 'users.id'
                }
            }
        };
    }
}

class Payout extends Model {
    static get tableName() {
        return 'payouts';
    }
}

class MoneyTransaction extends Model {
    static get tableName() {
        return 'money_transactions';
    }

    static get relationMappings() {
        return {
            user: {
                relation: Model.BelongsToOneRelation,
                modelClass: User,
                join: {
                    from: 'money_transactions.user_id',
                    to: 'users.id'
                }
            }
        };
    }
}

module.exports = {
    User,
    UploadImg,
    BaseProduct,
    BaseProductColor,
    BaseProductSecondOption,
    BaseProductVariant,
    Artwork,
    ArtworkLike,
    Tag,
    ArtworkTag,
    Comment,
    OverlaidProduct,
    CartLine,
    Order,
    OrderLine,
    OrderLog,
    MoneyTransaction,
    Payout
};
