const tap = require('tap');
const Knex = require('knex');
const objection = require('objection');

const knexConfigs = require('../../knexfile.js');
const {
    User,
    BaseProduct,
    BaseProductColor,
    BaseProductSecondOption,
    BaseProductVariant,
    Artwork,
    ArtworkLike,
    Tag,
    ArtworkTag,
    Comment,
    OverlaidProduct,
    CartLine,
    UploadImg,
    Order,
    OrderLine,
    OrderLog
} = require('../index.js');
const SampleRecords = require('../sample');
const TestHelper = require('../../services/testHelper');


async function main() {
    tap.test('insert each model without any errors being thrown', async (t) => {
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });

        await User.query().insert({
            ...SampleRecords.User
        });

        await BaseProduct.query().insert({
            ...SampleRecords.BaseProduct
        });

        await BaseProductColor.query().insert({
            ...SampleRecords.BaseProductColor
        });

        await BaseProductSecondOption.query().insert({
            ...SampleRecords.BaseProductSecondOption
        });

        await BaseProductVariant.query().insert({
            ...SampleRecords.BaseProductVariant
        });

        await Artwork.query().insert({
            ...SampleRecords.Artwork
        });

        await ArtworkLike.query().insert({
            ...SampleRecords.ArtworkLike
        });

        await Tag.query().insert({
            ...SampleRecords.Tag
        });

        await ArtworkTag.query().insert({
            ...SampleRecords.ArtworkTag
        });

        await Comment.query().insert({
            ...SampleRecords.Comment
        });

        await OverlaidProduct.query().insert({
            ...SampleRecords.OverlaidProduct
        });

        await UploadImg.query().insert({
            ...SampleRecords.UploadImg
        });

        await CartLine.query().insert({
            ...SampleRecords.CartLine
        });

        await Order.query().insert({
            ...SampleRecords.Order
        });

        await OrderLine.query().insert({
            ...SampleRecords.OrderLine
        });

        await OrderLog.query().insert({
            ...SampleRecords.OrderLog
        });
    });

    tap.test('OverlaidProduct ok', async (t) => {
        t.test('getter imgParams works as expected', async (t) => {
            // given
            const teardown = await TestHelper.setupDB();
            t.teardown(async () => {
                await teardown();
            });

            await BaseProduct.query().insert({
                ...SampleRecords.BaseProduct,
                id: 1,
                name: 'product_1',
                can_choose_color: true,
                has_second_option: true,
                bg_px_side_length: 0,
                frame_percent_left: 0,
                frame_percent_top: 0,
                frame_percent_width: 0,
                frame_mm_width: 0,
                frame_mm_height: 0,
            });


            await UploadImg.query().insert({
                ...SampleRecords.UploadImg,
                id: 1,
                original_filename: 'hello.jpg',
                filesize: 999999,
                path: 'path/to/img',
                width: 1000,
                height: 1000,
                type: 'jpg',
                mime: 'image/jpeg',
                owner_id: 1
            });
            await BaseProductColor.query().insert({
                ...SampleRecords.BaseProductColor,
                base_product_id: 1,
                name: 'blue',
                hex: '#0000ff',
                background_id: 1
            });

            await Artwork.query().insert({
                ...SampleRecords.Artwork,
                author_id: 1,
                title: 'artwork_title',
                image_id: 1
            });

            await OverlaidProduct.query().insert({
                ...SampleRecords.OverlaidProduct,
                id: 1,
                base_product_id: 1,
                artwork_id: 1,
                author_id: 1,
                artwork_mm_width: 0,
                artwork_mm_top: 0,
                artwork_mm_left: 0,
                default_base_product_color_id: 1,
                profit: 0,
            });

            // when
            const overlaidProrduct = await OverlaidProduct.query()
                  .withGraphFetched('[artwork.[image], default_base_product_color, base_product]')
                  .findById(1);
            const imgParams = overlaidProrduct.imgParams;

            // then
            t.notEqual(imgParams.artwrkPath, undefined, 'artwrkPath is not undefined');
            t.notEqual(imgParams.artwrkMilWidth, undefined, 'artwrkMilWidth is not undefined');
            t.notEqual(imgParams.artwrkMilTop, undefined, 'artwrkMilTop is not undefined');
            t.notEqual(imgParams.artwrkMilLeft, undefined, 'artwrkMilLeft is not undefined');
            t.notEqual(imgParams.bgPath, undefined, 'bgPath is not undefined');
            t.notEqual(imgParams.frmPrcntLeft, undefined, 'frmPrcntLeft is not undefined');
            t.notEqual(imgParams.frmPrcntTop, undefined, 'frmPrcntTop is not undefined');
            t.notEqual(imgParams.frmPrcntWidth, undefined, 'frmPrcntWidth is not undefined');
            t.notEqual(imgParams.frmMilWidth, undefined, 'frmMilWidth is not undefined');
        });
    });
}

main();
