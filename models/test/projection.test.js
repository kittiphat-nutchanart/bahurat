const tap = require('tap');

const { project } = require('../projection.js');

tap.test('project() can projects', async (t) => {
    // given
    const input = {
        item: 'journal',
        status: 'A',
        size: { h: 14, w: 21, uom: 'cm' },
        instock: [ { warehouse: 'A', qty: 5, bad: [{a: 1, b: 2}] } ],
        owner: null
    };
    const schema = {
        item: 1,
        size: {
            w: 1
        },
        instock: {
            warehouse: 1,
            dog: 1,
            bad: {
                a: 1
            }
        },
        owner: {
            name: 1
        },
        size_uom(input) {
            return input.size.uom;
        }
    };


    // when
    const output = project(input, schema);
    const arrOutput = project([input], schema);


    // then
    const expected = {
        item: 'journal',
        size: {
            w: 21
        },
        instock: [
            {
                warehouse: 'A',
                dog: undefined,
                bad: [
                    {
                        a: 1
                    }
                ]
            }
        ],
        owner: null,
        size_uom: 'cm'
    };
    t.same(output, expected); // works with single object input
    t.same(arrOutput, [expected]); // also works with arry of object input
});
