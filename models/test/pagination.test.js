const Knex = require('knex');
const objection = require('objection');
const tap = require('tap');

const { Artwork, UploadImg } = require('..');
const SampleRecords = require('../sample');
const Pagination = require('../pagination.js');
const knexConfigs = require('../../knexfile.js');
const TestHelper = require('../../services/testHelper');


async function main() {
    tap.test('getPagination() returns paginaiton', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });

        const query = Artwork.query().withGraphFetched('image').toKnexQuery().toString();

        for (let i = 0; i < 11; i++) {
            const img = await UploadImg.query().insert({
                ...SampleRecords.UploadImg
            });

            await Artwork.query().insert({
                ...SampleRecords.Artwork,
                image_id: img.id
            });
        }

        for (let i = 0; i < 6; i++) {
            const img = await UploadImg.query().insert({
                ...SampleRecords.UploadImg
            });

            await Artwork.query().insert({
                ...SampleRecords.Artwork,
                image_id: img.id,
                visible: true
            });
        }


        // when
        const case1_page = 1;
        const case1_size = 4;
        const case1_queryBuilder = Artwork.query();
        const case1_applyWhere = (q) => {
            return q;
        };
        const case1_applyGraph = (q) => {
            return q.withGraphFetched('image');
        };
        const case1_opts = {
            page: case1_page,
            size: case1_size,
            queryBuilder: case1_queryBuilder, // should only have orderBy i.e. no where, no graph
            applyWhere: case1_applyWhere,
            applyGraph: case1_applyGraph,
        };
        const case1_pagination = await Pagination.getPagination(case1_opts);


        const case2_page = 2;
        const case2_size = 5;
        const case2_queryBuilder = Artwork.query();
        const case2_applyWhere = (q) => {
            return q.where({ visible: true });
        };
        const case2_applyGraph = (q) => {
            return q;
        };
        const case2_opts = {
            page: case2_page,
            size: case2_size,
            queryBuilder: case2_queryBuilder, // should only have orderBy i.e. no where, no graph
            applyWhere: case2_applyWhere,
            applyGraph: case2_applyGraph,
        };
        const case2_pagination = await Pagination.getPagination(case2_opts);


        // then
        t.equal(case1_pagination.page_count, 5);
        t.equal(case1_pagination.results.length, 4);
        t.ok(case1_pagination.results[0].image);

        t.equal(case2_pagination.page_count, 2);
        t.equal(case2_pagination.results.length, 1);
        t.notOk(case2_pagination.results[0].image);
    });
}

main();
