exports.User = {
    username: 'hello',
    email: 'mail@email.com',
};

exports.BaseProduct = {
    name: 'product_1',
    can_choose_color: true,
    has_second_option: true,
    bg_px_side_length: 0,
    frame_percent_left: 0,
    frame_percent_top: 0,
    frame_percent_width: 0,
    frame_mm_width: 0,
    frame_mm_height: 0,
};

exports.BaseProductColor = {
    base_product_id: 1,
    name: 'blue',
    hex: '#0000ff',
    background_id: 1
};

exports.BaseProductSecondOption = {
    base_product_id: 1,
    value: 'second_option',
};

exports.BaseProductVariant = {
    base_product_id: 1,
    base_product_color_id: 1,
    base_product_second_option_id: 1,
    base_cost: 10000,
    quantity: 999
};

exports.Artwork = {
    author_id: 1,
    title: 'artwork_title',
    image_id: 1
};

exports.ArtworkLike = {
    user_id: 1,
    artwork_id: 1
};

exports.Tag = {
    name: 'fake tag'
};

exports.ArtworkTag = {
    artwork_id: 1,
    tag_id: 1
};

exports.Comment = {
    artwork_id: 1,
    user_id: 1,
    body: 'this is a comment.'
};

exports.OverlaidProduct = {
    base_product_id: 1,
    artwork_id: 1,
    author_id: 1,
    artwork_mm_width: 0,
    artwork_mm_top: 0,
    artwork_mm_left: 0,
    default_base_product_color_id: 1
};

exports.UploadImg = {
    original_filename: 'hello.jpg',
    filesize: 999999,
    path: 'path/to/img',
    width: 1000,
    height: 1000,
    type: 'jpg',
    mime: 'image/jpeg',
    owner_id: 1
};

exports.CartLine = {
    session_id: 'abcdefg',
    overlaid_product_id: 1,
    base_product_id: 1,
    base_product_color_id: 1,
    base_product_second_option_id: 1,
    quantity: 1
};

exports.Order = {
    session_id: '16fd2706-8baf-433b-82eb-8c7fada847da',
    access_token: 'keijr3idki',
    line_total: 100,
    total: 100
};

exports.OrderLine = {
    author_id: 1,
    author_unit_profit: 100,
    author_unit_tax: 100,
    author_total_profit: 100,
    author_total_tax: 100,
    author_total_earnings: 100,
    unit_price: 100,
    total_price: 100,
    base_product_id: 1,
    base_product_color_id: 1,
    base_product_second_option_id: 1,
    artwork_id: 100,
    artwork_mm_width: 100,
    artwork_mm_top: 100,
    artwork_mm_left: 100,
    quantity: 1,
    order_id: 100
};

exports.OrderLog = {
    order_id: 1,
    description: 'first_log'
};
