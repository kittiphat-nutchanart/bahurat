const crypto = require('crypto');

const R = require('ramda');

module.exports = class Pagination {
    static redis = {};

    static async getPagination({
        page,
        size,
        queryBuilder, // should only have orderBy i.e. no where, no graph
        applyWhere,
        applyGraph,
        useCache,
        cachePrefix,
        cacheTime // seconds
    }) {
        let select_id_hash;
        let expire_at;
        if (useCache) {
            const select_id = R.pipe(applyWhere)(queryBuilder.clone())
                  .select('id');
            select_id_hash = crypto.createHash('md5')
                .update(cachePrefix + select_id.toKnexQuery().toString())
                .digest('hex');
            const ids = await select_id;
            const hasCacheKey = await this.redis.EXISTSAsync(select_id_hash);
            if (!hasCacheKey) {
                await this.redis.RPUSHAsync(select_id_hash, ...ids);
            }
        }

        let rowCount;
        if (useCache) {
            rowCount = await this.redis.LLENAsync(select_id_hash);
        } else {
            rowCount = await R.pipe(applyWhere)(queryBuilder.clone())
                .resultSize();
        }
        const page_count = Math.ceil(rowCount / size);

        let results;
        if (useCache) {
            const ids = this.redis.get(select_id_hash).offset(size * (page - 1)).limit(size);
            const select_results = R.pipe(applyWhere)(queryBuilder.clone())
                  .whereIn(ids);
            const select_results_hash = crypto.createHash('md5')
                  .update(cachePrefix + select_results.toKnexQuery().toString())
                  .digest('hex');

            const cache_results = await this.redis.get(select_results_hash);
            if (!cache_results) {
                results = await R.pipe(applyGraph)(select_results.clone());
                await this.redis.SETAsync(select_results_hash, JSON.stringify(results));
            } else {
                results = JSON.parse(cache_results);
            }
        } else {
            results = await R.pipe(applyWhere, applyGraph)(queryBuilder.clone())
                  .offset(size * (page - 1))
                  .limit(size);
        }

        const pagination = {
            results,
            page_count
        };

        return pagination;
    }
}
