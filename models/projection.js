/**
 * project/serialize an object
 * @param {*} input - input
 * @param {*} schema - schema
 * @returns {*}
 */
function project(input, schema) {

    if (input === null) return input;

    if (Array.isArray(input)) return input.map(i => project(i, schema));

    const result = {};
    Object.entries(schema).forEach(([key, value]) => {
        if (typeof value === 'object') {
            result[key] = project(input[key], value);
        } else if (typeof value === 'function') {
            result[key] = value(input);
        } else if (value === 1) {
            result[key] = input[key];
        }
    });

    return result;
}

exports.project = project;
