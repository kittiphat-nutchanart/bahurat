
exports.up = function(knex) {
    return knex.schema
        .createTable('artwork_likes', function (table) {
            table.increments('id');
            table.integer('artwork_id').notNullable();
            table.integer('user_id').notNullable();

            table.timestamp('created_at').notNullable().defaultTo(knex.fn.now());

            table.foreign('artwork_id').references('artworks.id');
            table.foreign('user_id').references('users.id');

            table.unique(['user_id', 'artwork_id']);
        });
};

exports.down = function(knex) {
    throw new Error('Don\'t do down migration');
};
