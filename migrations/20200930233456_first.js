
exports.up = function(knex) {
    return knex.schema
        .createTable('users', function (table) {
            table.increments('id');
            table.string('email').unique().notNullable();
            table.string('username').unique().notNullable();
            table.integer('banner_id');
            table.integer('avatar_id');
            table.string('about', 10000).notNullable().defaultTo('');
            table.string('bank_account').notNullable().defaultTo('');
            table.string('bcrypt_hash').notNullable().defaultTo('');
            table.string('i_am').notNullable().defaultTo('');
            table.integer('recent_artwork_uploads').notNullable().defaultTo(0);
            table.integer('account_balance').notNullable().defaultTo(0);
            table.timestamp('created_at').notNullable().defaultTo(knex.fn.now());
            table.uuid('confirmation_token');
            table.timestamp('confirmed_at');

            table.uuid('password_reset_token').comment('should expire 7 days after issued');
            table.timestamp('password_reset_requested_at');

        })
        .createTable('upload_imgs', function (table) {
            table.increments('id');
            table.string('original_filename').notNullable();
            table.integer('filesize').notNullable();
            table.string('path').notNullable();
            table.integer('width').notNullable();
            table.integer('height').notNullable();
            table.string('type').notNullable();
            table.string('mime').notNullable();
            table.integer('owner_id');
            table.boolean('deleted').notNullable().defaultTo(false);

            table.timestamp('created_at').notNullable().defaultTo(knex.fn.now());

            table.foreign('owner_id').references('users.id');
        })
        .alterTable('users', function (table) {
            table.foreign('banner_id').references('upload_imgs.id');
            table.foreign('avatar_id').references('upload_imgs.id');
        })
        .createTable('base_products', function (table) {
            table.increments('id');
            table.string('name').notNullable();
            table.boolean('can_choose_color').notNullable();
            table.boolean('has_second_option').notNullable();
            table.integer('bg_px_side_length').notNullable();
            table.float('frame_percent_left').notNullable();
            table.float('frame_percent_top').notNullable();
            table.float('frame_percent_width').notNullable();
            table.integer('frame_mm_width').notNullable();
            table.integer('frame_mm_height').notNullable();
            table.boolean('enabled').notNullable().defaultTo(false);
            table.timestamp('created_at').notNullable().defaultTo(knex.fn.now());
        })
        .createTable('base_product_colors', function (table) {
            table.increments('id');
            table.integer('base_product_id').notNullable();
            table.string('name').notNullable();
            table.string('hex').notNullable();
            table.integer('background_id').notNullable();
            table.boolean('enabled').notNullable().defaultTo(false);
            table.boolean('is_default').notNullable().defaultTo(false);
            table.timestamp('created_at').notNullable().defaultTo(knex.fn.now());

            table.foreign('base_product_id').references('base_products.id');
            table.foreign('background_id').references('upload_imgs.id');
        })
        .createTable('base_product_second_options', function (table) {
            table.increments('id');
            table.integer('base_product_id').notNullable();
            table.string('value').notNullable();
            table.boolean('enabled').notNullable().defaultTo(false);
            table.timestamp('created_at').notNullable().defaultTo(knex.fn.now());

            table.foreign('base_product_id').references('base_products.id');
        })
        .createTable('base_product_variants', function (table) {
            table.increments('id');
            table.integer('base_product_id').notNullable();
            table.integer('base_product_color_id').notNullable();
            table.integer('base_product_second_option_id').notNullable();
            table.integer('base_cost').notNullable().defaultTo(0);
            table.integer('quantity').notNullable().defaultTo(0);
            table.timestamp('created_at').notNullable().defaultTo(knex.fn.now());

            table.unique(['base_product_id', 'base_product_color_id', 'base_product_second_option_id']);
        })
        .createTable('artworks', function (table) {
            table.increments('id');
            table.integer('author_id').notNullable();
            table.integer('image_id').notNullable();
            table.string('title').notNullable();
            table.string('description', 10000).notNullable().defaultTo('');
            table.integer('score').notNullable().defaultTo(0);
            table.boolean('visible').notNullable().defaultTo(false);
            table.boolean('blocked').notNullable().defaultTo(false);
            table.timestamp('created_at').notNullable().defaultTo(knex.fn.now());

            table.foreign('author_id').references('users.id');
            table.foreign('image_id').references('upload_imgs.id');
        })
        .createTable('comments', function(table) {
            table.increments('id');
            table.integer('artwork_id').notNullable();
            table.integer('user_id').notNullable();
            table.string('body').notNullable();
            table.timestamp('created_at').notNullable().defaultTo(knex.fn.now());

            table.foreign('artwork_id').references('artworks.id');
            table.foreign('user_id').references('users.id');
        })
        .createTable('overlaid_products', function (table) {
            table.increments('id');
            table.integer('base_product_id').notNullable();
            table.integer('artwork_id').notNullable();
            table.integer('author_id').notNullable();
            table.integer('artwork_mm_width').notNullable();
            table.integer('artwork_mm_top').notNullable();
            table.integer('artwork_mm_left').notNullable();
            table.integer('default_base_product_color_id').notNullable();
            table.integer('profit').notNullable().defaultTo(0);
            table.integer('score').notNullable().defaultTo(0);
            table.integer('sales').notNullable().defaultTo(0);
            table.boolean('visible').notNullable().defaultTo(false);
            table.boolean('blocked').notNullable().defaultTo(false);
            table.boolean('base_product_disabled').notNullable().defaultTo(false);
            table.timestamp('created_at').notNullable().defaultTo(knex.fn.now());

            table.foreign('base_product_id').references('base_products.id');
            table.foreign('artwork_id').references('artworks.id');
            table.foreign('author_id').references('users.id');
            table.foreign('default_base_product_color_id').references('base_product_colors.id');
        })
        .createTable('orders', function (table) {
            table.increments('id');

            table.boolean('can_complete_checkout').notNullable().defaultTo(false);
            table.boolean('checkout_complete').notNullable().defaultTo(false)
                .comment('to indicate whether an order in checkout state');

            table.uuid('session_id').notNullable();
            table.string('access_token').notNullable()
                .comment('buyers use this to view their orders');

            table.enu(
                'order_status',
                ['open', 'archived'],
                { useNative: true, enumName: 'order_status' }
            ).notNullable().defaultTo('open');
            table.enu(
                'payment_method',
                ['credit_card', 'bank_transfer'],
                { useNative: true, enumName: 'payment_method' }
            );
            table.enu(
                'fulfillment_status',
                ['unfulfilled', 'fulfilled'],
                { useNative: true, enumName: 'fulfillment_status' }
            ).notNullable().defaultTo('unfulfilled');

            table.string('payment_gateway').notNullable().defaultTo('');
            table.string('payment_card_id').notNullable().defaultTo('')
                .comment('aka token or nonce');
            table.jsonb('payment_card_data').notNullable().defaultTo('{}')
                .comment('data retrieved from card_id e.g. last 4 digits');
            table.string('payment_return_uri').notNullable().defaultTo('');
            table.jsonb('payment_charge_data').notNullable().defaultTo('{}')
                .comment('data returned after calling charge endpoint on card_id');

            table.string('shipping_full_name').notNullable().defaultTo('');
            table.string('shipping_address').notNullable().defaultTo('');
            table.string('shipping_district').notNullable().defaultTo('');
            table.string('shipping_province').notNullable().defaultTo('');
            table.string('shipping_postal_code').notNullable().defaultTo('');
            table.string('shipping_country_code').notNullable().defaultTo('');

            table.string('phone').notNullable().defaultTo('');
            table.string('email').notNullable().defaultTo('');

            table.integer('line_total').notNullable();
            table.integer('shipping_cost').notNullable().defaultTo(0);
            table.integer('total').notNullable();

            table.timestamp('created_at').notNullable().defaultTo(knex.fn.now());
            table.timestamp('paid_at');
            table.timestamp('fulfilled_at');
        })
        .createTable('order_lines', function (table) {
            table.increments('id');

            table.integer('author_id').notNullable().comment('user who created the item');
            table.integer('author_unit_profit').notNullable();
            table.integer('author_unit_fee').notNullable().defaultTo(0);
            table.integer('author_unit_tax').notNullable().defaultTo(0);
            table.integer('author_total_profit').notNullable();
            table.integer('author_total_fee').notNullable().defaultTo(0);
            table.integer('author_total_tax').notNullable().defaultTo(0);
            table.integer('author_total_earnings').notNullable()
                .comment('(profit/unit - fee/unit - tax/unit) * quantity');
            // determine earnings eligibility from order.payment_status and created_at
            table.boolean('added_to_money_transactions').notNullable().defaultTo(false);

            table.integer('unit_price').notNullable();
            table.integer('total_price').notNullable();
            table.integer('base_product_id').notNullable();
            table.integer('base_product_color_id').notNullable();
            table.integer('base_product_second_option_id').notNullable();
            table.integer('artwork_id').notNullable();
            table.integer('artwork_mm_width').notNullable();
            table.integer('artwork_mm_top').notNullable();
            table.integer('artwork_mm_left').notNullable();
            table.integer('quantity').notNullable();
            table.integer('returns').notNullable().defaultTo(0);

            table.integer('order_id').notNullable();

            table.timestamp('created_at').notNullable().defaultTo(knex.fn.now());

            table.foreign('artwork_id').references('artworks.id');
            table.foreign('order_id').references('orders.id');
            table.foreign('author_id').references('users.id');
            table.foreign('base_product_id').references('base_products.id');
            table.foreign('base_product_color_id').references('base_product_colors.id');
            table.foreign('base_product_second_option_id').references('base_product_second_options.id');
        })
        .createTable('order_logs', function (table) {
            table.increments('id');
            table.string('description', 10000).notNullable();
            table.integer('user_id');
            table.integer('order_id').notNullable();
            table.timestamp('created_at').notNullable().defaultTo(knex.fn.now());

            table.foreign('user_id').references('users.id');
            table.foreign('order_id').references('orders.id');
        })
        .createTable('refunds', function(table) {
            table.increments('id');
            table.integer('order_line_id').notNullable();
            table.integer('amount').notNullable();
            table.boolean('added_to_money_transactions').notNullable().defaultTo(false);

            table.timestamp('created_at').notNullable().defaultTo(knex.fn.now());

            table.foreign('order_line_id').references('order_lines.id');
        })
        .createTable('payouts', function (table) {
            table.increments('id');
            table.integer('user_id').notNullable();
            table.integer('amount').notNullable();
            table.string('bank_account').notNullable();
            table.boolean('success').notNullable();
            table.string('message', 10000).notNullable();

            table.timestamp('created_at').notNullable().defaultTo(knex.fn.now());

            table.foreign('user_id').references('users.id');
        })
        .createTable('money_transactions', function (table) {
            table.increments('id');
            table.integer('user_id').notNullable();
            table.string('description', 10000).notNullable();
            table.integer('amount').notNullable();
            table.integer('balance').notNullable();

            table.timestamp('created_at').notNullable().defaultTo(knex.fn.now());

            table.foreign('user_id').references('users.id');
        })
        .createTable('cart_lines', function (table) {
            table.increments('id');
            table.string('session_id').notNullable();
            table.integer('overlaid_product_id').notNullable();
            table.integer('base_product_id').notNullable();
            table.integer('base_product_color_id').notNullable();
            table.integer('base_product_second_option_id').notNullable();
            table.integer('quantity').notNullable();
            table.timestamp('created_at').notNullable().defaultTo(knex.fn.now());

            table.foreign('overlaid_product_id').references('overlaid_products.id');
            table.foreign('base_product_id').references('base_products.id');
            table.foreign('base_product_color_id').references('base_product_colors.id');
            table.foreign('base_product_second_option_id').references('base_product_second_options.id');
        });
};

exports.down = function(knex) {
};
