
exports.up = function(knex) {
    return knex.schema
        .createTable('tags', function (table) {
            table.increments('id');
            table.string('name').notNullable();

            table.timestamp('created_at').notNullable().defaultTo(knex.fn.now());

            table.unique('name');
        })
        .createTable('artworks_to_tags', function (table) {
            table.increments('id');
            table.integer('artwork_id').notNullable();
            table.integer('tag_id').notNullable();

            table.timestamp('created_at').notNullable().defaultTo(knex.fn.now());

            table.foreign('artwork_id').references('artworks.id');
            table.foreign('tag_id').references('tags.id');
        });
};

exports.down = function(knex) {
    throw new Error('Don\'t do down migration');
};
