
exports.up = function(knex) {
    return knex.schema
        .table('users', function (table) {
            table.timestamp('logout_all_at');
        });
};

exports.down = function(knex) {
    throw new Error('Don\'t do down migration');
};
