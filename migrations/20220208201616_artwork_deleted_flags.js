
exports.up = function(knex) {
    return knex.schema
        .table('artworks', function (table) {
            table.boolean('deleted').notNullable().defaultTo(false);
        });
};

exports.down = function(knex) {
    throw new Error('Don\'t do down migration');
};
