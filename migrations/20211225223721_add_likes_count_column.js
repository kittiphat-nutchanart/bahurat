
exports.up = function(knex) {
    return knex.schema
        .table('artworks', function (table) {
            table.integer('likes_count').notNullable().defaultTo(0);
        });
};

exports.down = function(knex) {
    throw new Error('Don\'t do down migration');
};
