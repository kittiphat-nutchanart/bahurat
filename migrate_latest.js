require('dotenv').config({ path: require('path').join(__dirname, './.env') });

const Knex = require('knex');

const knexConfigs = require('./knexfile.js');

async function main() {
    const knex = Knex(knexConfigs.default);
    await knex.migrate.latest();
    await knex.destroy();
}

main().catch((err) => console.error(err) || process.exit(1));
