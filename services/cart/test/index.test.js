const tap = require('tap');
const Joi = require('@hapi/joi');
const Knex = require('knex');
const objection = require('objection');

const CartService = require('../index.js');
const schemas = require('../schemas.js');
const knexConfigs = require('../../../knexfile.js');
const {
    BaseProduct,
    BaseProductColor,
    BaseProductSecondOption,
    Artwork,
    OverlaidProduct,
    BaseProductVariant,
    CartLine,
    Order,
    OrderLine,
    UploadImg
} = require('../../../models');
const SampleRecords = require('../../../models/sample.js');
const TestHelper = require('../../testHelper');


async function main() {
    tap.test('getCart() returns correctly without an error on any items', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });

        await BaseProduct.query().insert({
            ...SampleRecords.BaseProduct,
            id: 1,
            name: 'product_1',
            can_choose_color: true,
            has_second_option: true,
            bg_px_side_length: 0,
            frame_percent_left: 0,
            frame_percent_top: 0,
            frame_percent_width: 0,
            frame_mm_width: 0,
            frame_mm_height: 0,
        });

        await BaseProductColor.query().insert({
            ...SampleRecords.BaseProductColor,
            id: 1,
            base_product_id: 1,
            name: 'blue',
            hex: '#0000ff',
            background_id: (await UploadImg.query().insert({
                ...SampleRecords.UploadImg,
            }).then(i => i.id))
        });

        await BaseProductSecondOption.query().insert({
            ...SampleRecords.BaseProductSecondOption,
            id: 1,
            base_product_id: 1,
            value: 'product_1_option_1',
        });

        await BaseProductVariant.query().insert({
            ...SampleRecords.BaseProductVariant,
            base_product_id: 1,
            base_product_color_id: 1,
            base_product_second_option_id: 1,
            base_cost: 10000,
            quantity: 999
        });

        await Artwork.query().insert({
            ...SampleRecords.Artwork,
            id: 1,
            author_id: 1,
            title: 'artwork_1',
            image_id: 1
        });

        await OverlaidProduct.query().insert({
            ...SampleRecords.OverlaidProduct,
            id: 1,
            base_product_id: 1,
            artwork_id: 1,
            author_id: 1,
            artwork_mm_width: 0,
            artwork_mm_top: 0,
            artwork_mm_left: 0,
            default_base_product_color_id: 1,
            profit: 10000,
        });

        await CartLine.query().insert({
            ...SampleRecords.CartLine,
            session_id: 'abcdef',
            overlaid_product_id: 1,
            base_product_id: 1,
            base_product_color_id: 1,
            base_product_second_option_id: 1,
            quantity: 1
        });

        // when
        const cart = await CartService.getCart('abcdef');


        // then
        Joi.assert(
            cart,
            schemas.cart,
            'it obeys schema',
            { presence: 'required' }
        );
        t.equal(cart.cart.can_checkout, true, 'can checkout');
    });

    tap.test('getCheckout() returns correctly', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });


        await BaseProduct.query().insert({
            ...SampleRecords.BaseProduct,
            id: 1,
            name: 'product_1',
            can_choose_color: true,
            has_second_option: true,
            bg_px_side_length: 0,
            frame_percent_left: 0,
            frame_percent_top: 0,
            frame_percent_width: 0,
            frame_mm_width: 0,
            frame_mm_height: 0,
        });

        await BaseProductColor.query().insert({
            ...SampleRecords.BaseProductColor,
            id: 1,
            base_product_id: 1,
            name: 'blue',
            hex: '#0000ff',
            background_id: (await UploadImg.query().insert({
                ...SampleRecords.UploadImg,
            }).then(i => i.id))
        });

        await BaseProductSecondOption.query().insert({
            ...SampleRecords.BaseProductSecondOption,
            id: 1,
            base_product_id: 1,
            value: 'product_1_option_1',
        });

        await BaseProductVariant.query().insert({
            ...SampleRecords.BaseProductVariant,
            base_product_id: 1,
            base_product_color_id: 1,
            base_product_second_option_id: 1,
            base_cost: 10000,
            quantity: 999
        });

        await Artwork.query().insert({
            ...SampleRecords.Artwork,
            id: 1,
            author_id: 1,
            title: 'artwork_1',
            image_id: 1
        });

        await OverlaidProduct.query().insert({
            ...SampleRecords.OverlaidProduct,
            id: 1,
            base_product_id: 1,
            artwork_id: 1,
            author_id: 1,
            artwork_mm_width: 0,
            artwork_mm_top: 0,
            artwork_mm_left: 0,
            default_base_product_color_id: 1,
            profit: 10000,
        });

        await Order.query().insert({
            ...SampleRecords.Order,
            id: 1,
            session_id: '123e4567-e89b-12d3-a456-426614174000',
            line_total: 100,
            total: 100
        });

        await OrderLine.query().insert({
            ...SampleRecords.OrderLine,
            author_id: 1,
            author_unit_profit: 100,
            author_unit_tax: 100,
            author_total_profit: 100,
            author_total_tax: 100,
            author_total_earnings: 100,
            unit_price: 100,
            total_price: 100,
            base_product_id: 1,
            base_product_color_id: 1,
            base_product_second_option_id: 1,
            artwork_id: 1,
            artwork_mm_width: 100,
            artwork_mm_top: 100,
            artwork_mm_left: 100,
            quantity: 1,
            order_id: 1
        });

        // when
        const checkout = await CartService.getCheckout(1);


        // then
        Joi.assert(
            checkout,
            schemas.checkout,
            'it obeys schema',
            { presence: 'required' }
        );
    });

}


main();
