const Joi = require('@hapi/joi');

const FakeCartService = require("../fake.js");
const schemas = require("../schemas.js");

const tap = require('tap');


function main() {
    tap.test("getCart() obeys the schema", async (t) => {
        Joi.assert(
            FakeCartService.getCart(),
            schemas.cart,
            "obeys the schema",
            { presence: "required" }
        );
    });

    tap.test("getCheckout() obeys the schema", async t => {
        Joi.assert(
            FakeCartService.getCheckout(),
            schemas.checkout,
            "obeys the schema",
            { presence: "required" }
        );

    });
}

main();
