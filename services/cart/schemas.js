const Joi = require('@hapi/joi');

const cart = Joi.object({
    cart: Joi.object({

        // computed
        line_total: Joi.number(),

        // computed
        can_checkout: Joi.boolean().allow(0, 1),

        lines: Joi.array().items(
            Joi.object({
                id: Joi.number(),

                overlaid_product: Joi.object({
                    id: Joi.number(),

                    artwork: Joi.object({
                        title: Joi.string()
                    }),

                    base_product: Joi.object({
                        name: Joi.string()
                    }),
                }),

                base_product_color: Joi.object({
                    name: Joi.string()
                }),
                base_product_second_option: Joi.object({
                    value: Joi.string()
                }),

                quantity: Joi.number(),

                // computed
                unit_price: Joi.number(),

                // computed
                total_price: Joi.number(),

                // computed
                variant_name: Joi.string(),

                // computed
                overlaid_product_img_url: Joi.string(),

                // computed
                error: Joi.string().allow('')
            })
        )
    })
});

const checkout = Joi.object({
    checkout: Joi.object({
        id: Joi.number(),
        line_total: Joi.number(),
        shipping_cost: Joi.number(),
        total: Joi.number(),
        lines: Joi.array().items(
            Joi.object({

                base_product: Joi.object({
                    name: Joi.string()
                }),
                base_product_color: Joi.object({
                    name: Joi.string()
                }),
                base_product_second_option: Joi.object({
                    value: Joi.string()
                }),
                artwork: Joi.object({
                    title: Joi.string()
                }),

                unit_price: Joi.number(),
                total_price: Joi.number(),
                quantity: Joi.number(),

                // computed
                variant_name: Joi.string(),

                // computed
                overlaid_product_img_url: Joi.string()
            })
        )
    })
});

const order = Joi.object({
    order: Joi.object({
        id: Joi.number(),

        view_token: Joi.string(),

        payment_method: Joi.string(),
        payment_status: Joi.string(),
        fulfillment_status: Joi.string(),

        line_total: Joi.number(),
        shipping_cost: Joi.number(),
        total: Joi.number(),

        shipping_full_name: Joi.string(),
        shipping_address: Joi.string(),
        shipping_district: Joi.string(),
        shipping_province: Joi.string(),
        shipping_postal_code: Joi.string(),
        shipping_country_code: Joi.string(),

        phone: Joi.string(),
        email: Joi.string(),

        lines: Joi.array().items(
            Joi.object({

                base_product: Joi.object({
                    name: Joi.string()
                }),
                base_product_color: Joi.object({
                    name: Joi.string()
                }),
                base_product_second_option: Joi.object({
                    value: Joi.string()
                }),
                artwork: Joi.object({
                    title: Joi.string()
                }),

                unit_price: Joi.number(),
                total_price: Joi.number(),
                quantity: Joi.number(),

                // computed
                variant_name: Joi.string(),

                // computed
                overlaid_product_img_url: Joi.string()
            })
        )
    })
});

module.exports = { cart, checkout, order };
