const Joi = require('@hapi/joi');
const Boom = require('@hapi/boom');
const { raw } = require('objection');

const Payment = require('../../payment');
const {
    Artwork,
    CartLine,
    Order,
    OrderLine
} = require('../../models');
const ProductService = require('../../services/product');
const Storage = require('../../storage');
const schemas = require('./schemas.js');
const Mailer = require('../../mailer');

module.exports = class {
    static VAT = 0.07;

    static CARD_PROCESSING_FEE = 0;

    static async getCart(session_id) {

        const lines = await CartLine.query()
              .where({
                  session_id
              })
              .withGraphFetched(`[
                  overlaid_product.[
                      base_product,
                      artwork.[image]
                  ],
                  base_product_color.[background],
                  base_product_second_option,
                  base_product_variant,
              ]`);


        lines.forEach((line) => {
            line.variant_name = ProductService.computeVariantName(
                line.base_product_color.name,
                line.base_product_second_option.value
            ),

            line.overlaid_product_img_url = Storage.getOPImgURLFromCartLine(
                line,
                { size: 200 }
            );

            let unit_price = 0;
            let total_price = 0;
            let error = '';
            if (line.base_product_variant && line.base_product_variant.quantity) {
                unit_price = line.overlaid_product.profit + line.base_product_variant.base_cost;

                total_price = unit_price * line.quantity;

            } else {
                // fix
                error = 'out_of_stock';

            };
            line.unit_price = unit_price;
            line.total_price = total_price;
            line.error = error;
        });

        const line_total = lines.reduce((acc, line) => line.total_price + acc, 0);
        const can_checkout = (lines.length > 0) &&
              lines.every((line) => !line.error);

        const cart = {
            cart: {
                line_total,
                can_checkout,
                lines
            }
        };

        return Joi.attempt(
            cart,
            schemas.cart,
            { stripUnknown: true, presence: 'required' }
        );
    }

    static async getCheckout(id) {
        const checkout = await Order.query()
              .findById(id)
              .withGraphFetched(`lines.[
                  artwork.[image],
                  base_product,
                  base_product_color.[background],
                  base_product_second_option,
              ]`)
              .throwIfNotFound();

        const lines = checkout.lines.map((line) => {

            const variant_name = ProductService
                  .computeVariantName(
                      line.base_product_color.name,
                      line.base_product_second_option.value
                  );

            const overlaid_product_img_url = Storage.getOPImgURLFromOrderLine(
                line,
                { size: 200 }
            );

            return {
                base_product: {
                    name: line.base_product.name
                },
                base_product_color: {
                    name: line.base_product_color.name
                },
                base_product_second_option: {
                    value: line.base_product_second_option.value
                },
                artwork: {
                    title: line.artwork.title
                },
                unit_price: line.unit_price,
                total_price: line.total_price,
                quantity: line.quantity,

                variant_name,
                overlaid_product_img_url
            };
        });

        return {
            checkout: {
                id: checkout.id,
                line_total: checkout.line_total,
                shipping_cost: checkout.shipping_cost,
                total: checkout.total,
                lines
            }
        };
    }

    static async sendOrderConfirmationEmail(orderId) {
        // fix
        // implement the real logic
        const to = 'kangarthurjr@gmail.com';
        const subject = 'order confirmation';
        const data = {};
        await Mailer.send('order-confirmation', data, to, subject);
    }

    static async imposeFeesOnAuthors(order_id) {
        const feeRate = this.CARD_PROCESSING_FEE;
        await OrderLine.query()
            .where({ order_id })
            .patch({
                author_unit_fee: raw('author_unit_profit * ?', feeRate),
                author_total_fee: raw('author_total_profit * ?', feeRate),
                author_total_earnings: raw(
                    '(author_total_profit * (1 - ?)) - author_total_tax', feeRate)
            });
    }
};
