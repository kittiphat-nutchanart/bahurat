// Load and instantiate Chance
const chance = require('chance').Chance();
const R = require('ramda');

module.exports = class FakeCartService {
    static getCart() {
        const makeLine = () => {
            return {
                id: chance.integer(),

                overlaid_product: {
                    id: chance.integer(),

                    artwork: {
                        title: chance.word()
                    },
                    base_product: {
                        name: chance.word()
                    },
                },
                base_product_color: {
                    name: chance.word()
                },
                base_product_second_option: {
                    value: chance.word()
                },

                quantity: chance.integer({ min: 1, max: 5 }),

                unit_price: chance.integer({ min: 1000, max: 10000 }),
                total_price: chance.integer({ min: 1000, max: 10000 }),
                variant_name: chance.word(),
                overlaid_product_img_url: 'https://picsum.photos/200?random=' + chance.integer(),
                error: chance.bool() ? chance.word() : ''
            };
        };

        const lines = chance.bool({ likelihood: 80 }) ?
              R.range(1, chance.integer({ min: 2, max: 6 })).map(makeLine) :
              [] ;

        return {
            cart: {
                line_total: chance.integer({ min: 1000, max: 10000 }),
                can_checkout: chance.bool(),
                lines
            }
        };
    }

    static getCheckout() {
        const makeLine = () => {
            return {
                base_product: {
                    name: chance.word()
                },
                base_product_color: {
                    name: chance.word()
                },
                base_product_second_option: {
                    value: chance.word()
                },
                artwork: {
                    title: chance.word()
                },

                unit_price: chance.integer({ min: 1000, max: 10000 }),
                total_price: chance.integer({ min: 1000, max: 10000 }),
                quantity: chance.integer({ min: 1, max: 5 }),

                variant_name: chance.word(),
                overlaid_product_img_url: 'https://picsum.photos/200?random=' + chance.integer()
            };
        };

        const lines = R.range(1, chance.integer({ min: 2, max: 6 }))
              .map(makeLine);

        return {
            checkout: {
                id: chance.integer(),
                line_total: chance.integer({ min: 1000, max: 10000 }),
                shipping_cost: chance.integer({ min: 1000, max: 10000 }),
                total: chance.integer({ min: 1000, max: 10000 }),
                lines
            }
        };
    }
};
