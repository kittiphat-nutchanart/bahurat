const {
    BaseProduct,
    BaseProductColor,
    Artwork,
    OverlaidProduct
} = require('../../models');
const Storage = require('../../storage');

module.exports = class ProductService {

    /**
     * determine if a base_product is ready
     * @param {number} id - id of base_product
     * @param {*} transaction - knex transaction. can be ignored by passing falsy value
     * @returns {Promise<boolean>} true -> ready, false -> not ready
     */
    static async baseProductHasValidState(id, trx) {

        // the check for numbers of colors and options is ignored
        // rule of thumb
        // every enabled base_product must
        //    1) has one or more enabled colors
        //    2) has one or more enabled second_options
        //    3) has one default color
        //    4) the default color must be enabled

        const base_product = await BaseProduct.query(trx)
              .withGraphFetched(`[
                  base_product_colors,
                  base_product_second_options
              ]`)
              .findById(id)
              .throwIfNotFound();

        // if it's disabled there's nothing to consider
        // it's automatically in a valid state
        if (!base_product.enabled) return true;

        const enabledColors = base_product.base_product_colors
              .filter(c => c.enabled);
        const hasEnabledColors = enabledColors.length > 0;
        if (!hasEnabledColors) return false;

        const defaultColors = base_product.base_product_colors
              .filter(c => c.is_default);
        const hasOneDefaultColor = defaultColors.length === 1;
        if (!hasOneDefaultColor) return false;

        const defaultColorIsEnabled = defaultColors[0].enabled;
        if (!defaultColorIsEnabled) return false;

        const hasEnabledSecondOpts = base_product.base_product_second_options
              .some(opt => opt.enabled);
        if (!hasEnabledSecondOpts) return false;

        return true;
    }

    /**
     * rejects if invalid state
     * @param {*} args - the args should the same as args of this.baseProductHasValidState
     */
    static async assertBaseProductHasValidState(...args) {
        const validState = await this.baseProductHasValidState(...args);
        if (!validState) {
            throw Error('base_product_has_invalid_state');
        }
    }

    // fix
    // need test
    static computeVariantName(colorName, secondOptionVal) {
        const names = [];

        // make sure that colorName is not ''
        if (colorName) names.push(colorName);

        // secondOption is optional thus can be empty
        if (secondOptionVal) names.push(secondOptionVal);

        const name = names.join(', ');

        return name;
    }

    /**
     * @param {*} artworkId
     * @param {*} baseProductId - optional if not specify add all enabled baseProducts
     */
    static async addOverlaidProduct(artworkId, baseProductId) {
        const artwork = await Artwork.query()
              .findById(artworkId)
              .throwIfNotFound()
              .withGraphFetched('image');

        const defaultColors = await BaseProductColor.query()
              .joinRelated('base_product')
              .where({
                  is_default: true,
                  'base_product.enabled': true
              })
              .andWhere((builder) => {
                  if (baseProductId) {
                      builder.where({ base_product_id: baseProductId });
                  }
              })
              .throwIfNotFound()
              .withGraphFetched('base_product');

        // fix
        // use triger when set is_default -> true
        // probably use $beforeInsert, $beforeUpdate hooks
        const overlaidProducts = defaultColors.map((color) => {
            const artworkPlacement = this.calcDefaultArtworkPlacement(
                artwork.image,
                color.base_product
            );

            const author_id = artwork.author_id;
            const artwork_id = artwork.id;
            const base_product_id = color.base_product.id;
            const default_base_product_color_id = color.id;

            const overlaidProduct = {
                ...artworkPlacement,
                base_product_id,
                author_id,
                default_base_product_color_id,
                artwork_id
            };

            return overlaidProduct;
        });

        await OverlaidProduct.knexQuery().insert(overlaidProducts);
    }

    /**
     * @private
     * @param {*} uploadImg - Models.UploadImg's instance
     * @param {*} baseProduct - Models.BaseProduct's instance
     */
    static calcDefaultArtworkPlacement(uploadImg, baseProduct) {
        const { frame_mm_width, frame_mm_height } = baseProduct;
        const { width, height } = uploadImg;

        let artwork_mm_width = frame_mm_width;
        while (true) {
            const artwork_mm_height = (artwork_mm_width * height) / width;
            const artwork_in_width = artwork_mm_width / 25.4;
            const dpi = width / artwork_in_width;

            if ((artwork_mm_height > frame_mm_height) || (dpi < Storage.MIN_DPI)) {
                artwork_mm_width = Math.round(artwork_mm_width * 0.5); // keep it integer
                continue;
            }

            break;
        }

        let artwork_mm_top = 0;
        let artwork_mm_left = Math.round((frame_mm_width - artwork_mm_width) / 2);

        const artworkPlacement = {
            artwork_mm_width,
            artwork_mm_top,
            artwork_mm_left
        };

        return artworkPlacement;
    }
};
