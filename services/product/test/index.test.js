const tap = require('tap');
const Knex = require('knex');
const objection = require('objection');
const sinon = require('sinon');

const knexConfigs = require('../../../knexfile.js');
const {
    BaseProduct,
    BaseProductColor,
    BaseProductSecondOption,
    Artwork,
    OverlaidProduct,
    UploadImg
} = require('../../../models');
const SampleRecords = require('../../../models/sample.js');
const ProductService = require('../index.js');
const TestHelper = require('../../testHelper');


async function main() {
    tap.test('baseProductHasValidState() works as expected', async (t) => {
        t.test('base_product is enabled', async (t) => {
            t.test('resolves to true when everything\'s ok', async (t) => {

                // given
                const teardown = await TestHelper.setupDB();
                t.teardown(async () => {
                    await teardown();
                });

                await BaseProduct.query().insert({
                    ...SampleRecords.BaseProduct,
                    id: 1,
                    name: 'product_1',
                    can_choose_color: true,
                    has_second_option: true,
                    bg_px_side_length: 0,
                    frame_percent_left: 0,
                    frame_percent_top: 0,
                    frame_percent_width: 0,

                    frame_mm_width: 0,
                    frame_mm_height: 0,
                });

                await BaseProductColor.query().insert({
                    ...SampleRecords.BaseProductColor,
                    base_product_id: 1,
                    name: 'blue',
                    hex: '#0000ff',
                    bg_path: 'fake/path/to/bg',
                    enabled: true,
                    is_default: true,
                    background_id: 1
                });

                await BaseProductSecondOption.query().insert({
                    ...SampleRecords.BaseProductSecondOption,
                    base_product_id: 1,
                    value: 'second_option',
                    enabled: true
                });

                // when
                const isReady = await ProductService.baseProductHasValidState(1);

                // then
                t.ok(isReady);
            });

            t.test('resolves to false when no enabled colors', async (t) => {

                // given
                const teardown = await TestHelper.setupDB();
                t.teardown(async () => {
                    await teardown();
                });

                await BaseProduct.query().insert({
                    ...SampleRecords.BaseProduct,
                    id: 1,
                    name: 'product_1',
                    can_choose_color: true,
                    has_second_option: true,
                    bg_px_side_length: 0,
                    frame_percent_left: 0,
                    frame_percent_top: 0,
                    frame_percent_width: 0,

                    frame_mm_width: 0,
                    frame_mm_height: 0,
                    enabled: true
                });

                await BaseProductColor.query().insert({
                    ...SampleRecords.BaseProductColor,
                    base_product_id: 1,
                    name: 'blue',
                    hex: '#0000ff',
                    bg_path: 'fake/path/to/bg',
                    enabled: false,
                    is_default: true,
                    background_id: 1
                });

                await BaseProductSecondOption.query().insert({
                    ...SampleRecords.BaseProductSecondOption,
                    base_product_id: 1,
                    value: 'second_option',
                    enabled: true
                });

                // when
                const isReady = await ProductService.baseProductHasValidState(1);

                // then
                t.notOk(isReady);
            });

            t.test('resolves to false when-not number of enabled default colors is 1', async (t) => {

                // given
                const teardown = await TestHelper.setupDB();
                t.teardown(async () => {
                    await teardown();
                });

                await BaseProduct.query().insert({
                    ...SampleRecords.BaseProduct,
                    id: 1,
                    name: 'product_1',
                    can_choose_color: true,
                    has_second_option: true,
                    bg_px_side_length: 0,
                    frame_percent_left: 0,
                    frame_percent_top: 0,
                    frame_percent_width: 0,

                    frame_mm_width: 0,
                    frame_mm_height: 0,
                    enabled: true
                });

                await BaseProductColor.query().insert({
                    ...SampleRecords.BaseProductColor,
                    base_product_id: 1,
                    name: 'blue',
                    hex: '#0000ff',
                    bg_path: 'fake/path/to/bg',
                    enabled: true,
                    is_default: true,
                    background_id: 1
                });

                await BaseProductColor.query().insert({
                    ...SampleRecords.BaseProductColor,
                    base_product_id: 1,
                    name: 'blue',
                    hex: '#0000ff',
                    bg_path: 'fake/path/to/bg',
                    enabled: false,
                    is_default: true,
                    background_id: 1
                });

                await BaseProductSecondOption.query().insert({
                    ...SampleRecords.BaseProductSecondOption,
                    base_product_id: 1,
                    value: 'second_option',
                    enabled: true
                });

                // when
                const isReady = await ProductService.baseProductHasValidState(1);

                // then
                t.notOk(isReady);
            });

            t.test('resolves to false when default color is not enabled', async (t) => {

                // given
                const teardown = await TestHelper.setupDB();
                t.teardown(async () => {
                    await teardown();
                });

                await BaseProduct.query().insert({
                    ...SampleRecords.BaseProduct,
                    id: 1,
                    name: 'product_1',
                    can_choose_color: true,
                    has_second_option: true,
                    bg_px_side_length: 0,
                    frame_percent_left: 0,
                    frame_percent_top: 0,
                    frame_percent_width: 0,

                    frame_mm_width: 0,
                    frame_mm_height: 0,
                    enabled: true
                });

                await BaseProductColor.query().insert({
                    ...SampleRecords.BaseProductColor,
                    base_product_id: 1,
                    name: 'blue',
                    hex: '#0000ff',
                    bg_path: 'fake/path/to/bg',
                    enabled: true,
                    is_default: false,
                    background_id: 1
                });

                await BaseProductColor.query().insert({
                    ...SampleRecords.BaseProductColor,
                    base_product_id: 1,
                    name: 'blue',
                    hex: '#0000ff',
                    bg_path: 'fake/path/to/bg',
                    enabled: false,
                    is_default: true,
                    background_id: 1
                });

                await BaseProductSecondOption.query().insert({
                    ...SampleRecords.BaseProductSecondOption,
                    base_product_id: 1,
                    value: 'second_option',
                    enabled: true
                });

                // when
                const isReady = await ProductService.baseProductHasValidState(1);

                // then
                t.notOk(isReady);
            });

            t.test('resolves to false when missing enabled second_opts', async (t) => {

                // given
                const teardown = await TestHelper.setupDB();
                t.teardown(async () => {
                    await teardown();
                });

                await BaseProduct.query().insert({
                    ...SampleRecords.BaseProduct,
                    id: 1,
                    name: 'product_1',
                    can_choose_color: true,
                    has_second_option: true,
                    bg_px_side_length: 0,
                    frame_percent_left: 0,
                    frame_percent_top: 0,
                    frame_percent_width: 0,

                    frame_mm_width: 0,
                    frame_mm_height: 0,
                    enabled: true
                });

                await BaseProductColor.query().insert({
                    ...SampleRecords.BaseProductColor,
                    base_product_id: 1,
                    name: 'blue',
                    hex: '#0000ff',
                    bg_path: 'fake/path/to/bg',
                    enabled: true,
                    is_default: true,
                    background_id: 1
                });

                await BaseProductSecondOption.query().insert({
                    ...SampleRecords.BaseProductSecondOption,
                    base_product_id: 1,
                    value: 'second_option',
                    enabled: false
                });

                // when
                const isReady = await ProductService.baseProductHasValidState(1);

                // then
                t.notOk(isReady);
            });
        });

        t.test('when base_product is disabled, resolves to true automatically', async (t) => {
            // given
            const teardown = await TestHelper.setupDB();
            t.teardown(async () => {
                await teardown();
            });

            await BaseProduct.query().insert({
                ...SampleRecords.BaseProduct,
                id: 1,
                name: 'product_1',
                can_choose_color: true,
                has_second_option: true,
                bg_px_side_length: 0,
                frame_percent_left: 0,
                frame_percent_top: 0,
                frame_percent_width: 0,

                frame_mm_width: 0,
                frame_mm_height: 0,
                enabled: false
            });

            // when
            const isReady = await ProductService.baseProductHasValidState(1);

            // then
            t.ok(isReady);
        });

        t.test('rejects if product with given id doesn\'t exist', async (t) => {

            // given
            const teardown = await TestHelper.setupDB();
            t.teardown(async () => {
                await teardown();
            });

            // when
            const resPromise = ProductService.baseProductHasValidState(1);

            // then
            t.rejects(resPromise, 'rejects when product doesn\'t exist');
        });

    });

    tap.test('assertBaseProductHasValidState() works as expected', async (t) => {
        // given
        const sinonSandbox = sinon.createSandbox();
        t.teardown(async () => {
            sinonSandbox.restore();
        });

        const stub = sinonSandbox.stub(ProductService, 'assertBaseProductHasValidState')
              .returns(Promise.reject(false));

        // when
        const res = ProductService.assertBaseProductHasValidState(1, null);


        // then
        t.rejects(res);
        stub.calledOnceWithExactly(1, null);
    });

    tap.test('addOverlaidProduct()', async (t) => {
        t.test('works as expected', async (t) => {
            t.test('without providing baseProductId (2nd arg)', async (t) => {
                // given
                const teardown = await TestHelper.setupDB();
                t.teardown(async () => {
                    await teardown();
                });

                await BaseProduct.query().insert({
                    ...SampleRecords.BaseProduct,
                    id: 1,
                    name: 'product_1',
                    can_choose_color: true,
                    has_second_option: true,
                    bg_px_side_length: 1000,
                    frame_percent_left: .324,
                    frame_percent_top: .192,
                    frame_percent_width: .352,
                    frame_mm_width: 250,
                    frame_mm_height: 300,
                    enabled: true
                });

                await BaseProductColor.query().insert({
                    ...SampleRecords.BaseProductColor,
                    base_product_id: 1,
                    name: 'blue',
                    hex: '#0000ff',
                    background_id: 1,
                    is_default: true
                });

                await BaseProduct.query().insert({
                    ...SampleRecords.BaseProduct,
                    id: 2,
                    name: 'product_2',
                    can_choose_color: true,
                    has_second_option: true,
                    bg_px_side_length: 1000,
                    frame_percent_left: .324,
                    frame_percent_top: .192,
                    frame_percent_width: .352,
                    frame_mm_width: 250,
                    frame_mm_height: 300,
                    enabled: true
                });

                await BaseProductColor.query().insert({
                    ...SampleRecords.BaseProductColor,
                    base_product_id: 2,
                    name: 'black',
                    hex: '#000000',
                    background_id: 2,
                    is_default: true
                });

                const artwork = await Artwork.query().insert({
                    ...SampleRecords.Artwork,
                    author_id: 1,
                    title: 'artwork_title',
                    image_id: (await UploadImg.query().insert({
                        ...SampleRecords.UploadImg,
                    }).then(i => i.id))
                });

                // when
                await ProductService.addOverlaidProduct(artwork.id);

                const overlaidProducts = await OverlaidProduct.query();

                // then
                t.equal(overlaidProducts.length, 2);
            });

            t.test('with providing baseProductId (2nd arg)', async (t) => {
                // given
                const teardown = await TestHelper.setupDB();
                t.teardown(async () => {
                    await teardown();
                });

                await BaseProduct.query().insert({
                    ...SampleRecords.BaseProduct,
                    id: 1,
                    name: 'product_1',
                    can_choose_color: true,
                    has_second_option: true,
                    bg_px_side_length: 1000,
                    frame_percent_left: .324,
                    frame_percent_top: .192,
                    frame_percent_width: .352,
                    frame_mm_width: 250,
                    frame_mm_height: 300,
                    enabled: true
                });

                await BaseProductColor.query().insert({
                    ...SampleRecords.BaseProductColor,
                    base_product_id: 1,
                    name: 'blue',
                    hex: '#0000ff',
                    background_id: 1,
                    is_default: true
                });

                const selectedBaseProduct = await BaseProduct.query().insert({
                    ...SampleRecords.BaseProduct,
                    id: 2,
                    name: 'product_2',
                    can_choose_color: true,
                    has_second_option: true,
                    bg_px_side_length: 1000,
                    frame_percent_left: .324,
                    frame_percent_top: .192,
                    frame_percent_width: .352,
                    frame_mm_width: 250,
                    frame_mm_height: 300,
                    enabled: true
                });

                await BaseProductColor.query().insert({
                    ...SampleRecords.BaseProductColor,
                    base_product_id: 2,
                    name: 'black',
                    hex: '#000000',
                    background_id: 2,
                    is_default: true
                });

                const artwork = await Artwork.query().insert({
                    ...SampleRecords.Artwork,
                    author_id: 1,
                    title: 'artwork_title',
                    image_id: (await UploadImg.query().insert({
                        ...SampleRecords.UploadImg,
                    }).then(i => i.id))
                });

                // when
                await ProductService.addOverlaidProduct(artwork.id, selectedBaseProduct.id);

                const overlaidProducts = await OverlaidProduct.query();

                // then
                t.equal(overlaidProducts.length, 1);
                t.equal(overlaidProducts[0].base_product_id, selectedBaseProduct.id);
            });
        });
    });

    tap.test('calcDefaultArtworkPlacement() works as expected', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });

        const baseProduct = await BaseProduct.query().insert({
            ...SampleRecords.BaseProduct,
            id: 1,
            name: 'product_1',
            can_choose_color: true,
            has_second_option: true,
            bg_px_side_length: 1000,
            frame_percent_left: .324,
            frame_percent_top: .192,
            frame_percent_width: .352,
            frame_mm_width: 250,
            frame_mm_height: 300,
            enabled: true
        });

        const uploadImgLarge = await UploadImg.query().insert({
            ...SampleRecords.UploadImg,
            original_filename: 'hello.jpg',
            filesize: 999999,
            path: 'path/to/img',
            width: 10000,
            height: 10000,
            type: 'jpg',
            mime: 'image/jpeg',
            owner_id: 1
        });

        const uploadImgLargeAndVeryTall = await UploadImg.query().insert({
            ...SampleRecords.UploadImg,
            original_filename: 'hello.jpg',
            filesize: 999999,
            path: 'path/to/img',
            width: 10000,
            height: 50000,
            type: 'jpg',
            mime: 'image/jpeg',
            owner_id: 1
        });

        const uploadImgTiny = await UploadImg.query().insert({
            ...SampleRecords.UploadImg,
            original_filename: 'hello.jpg',
            filesize: 999999,
            path: 'path/to/img',
            width: 100,
            height: 100,
            type: 'jpg',
            mime: 'image/jpeg',
            owner_id: 1
        });

        // when
        const placementLargeImg = await ProductService
              .calcDefaultArtworkPlacement(uploadImgLarge, baseProduct);

        const placementLargeAndVeryTallImg = await ProductService
              .calcDefaultArtworkPlacement(uploadImgLargeAndVeryTall, baseProduct);

        const placementTinyImg = await ProductService
              .calcDefaultArtworkPlacement(uploadImgTiny, baseProduct);

        // then
        t.equal(placementLargeImg.artwork_mm_width, baseProduct.frame_mm_width);
        t.ok(placementLargeAndVeryTallImg.artwork_mm_width < baseProduct.frame_mm_width);
        t.ok(placementTinyImg.artwork_mm_width < baseProduct.frame_mm_width);
    });
}

main();
