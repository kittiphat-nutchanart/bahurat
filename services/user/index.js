const bcrypt = require('bcrypt');

const Mailer = require('../../mailer');

module.exports = class UserService {
    /**
     * encrypt a password
     * @param {sring} password - raw password e.g. '1234abc'
     */
    static async encryptPassword(password) {
        const saltRounds = 10;
        const bcrypt_hash = await bcrypt.hash(password, saltRounds);

        return bcrypt_hash;
    }

    /**
     * send an account-confirmation email
     * @param {sring} token - confirmation token
     * @param {sring} toEmail - recipient's email
     */
    static async sendAccountConfirmationEmail(token, toEmail) {
        const activateAccountUrl = process.env.ROOT_URL +
              '/activate-account?token=' + token;
        const data = { url: activateAccountUrl };
        const subject = 'ยืนยันบัญชีใหม่';
        await Mailer.send('account-confirmation', data, toEmail, subject);
    }

    /**
     * send a password-reset email
     * @param {sring} confirmUrl - e.g. 'mysite.com/user/password-reset?token=ajwidkwjjq'
     * @param {sring} toEmail - recipient's email
     */
    static async sendPasswordResetEmail(resetUrl, toEmail) {
        const data = { url: resetUrl };
        const subject = 'เปลี่ยนรหัสผ่าน';
        await Mailer.send('password-reset', data, toEmail, subject);
    }
};
