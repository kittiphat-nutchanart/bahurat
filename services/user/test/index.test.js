const tap = require('tap');
const bcrypt = require('bcrypt');
const sinon = require('sinon');

const UserService = require('..');
const Mailer = require('../../../mailer');


async function main() {
    tap.test('encryptPassword() can encrypt pasword', async (t) => {
        // given
        const password = 'password';

        // when
        const encryptedPassword = await UserService.encryptPassword(password);

        const passwordMatchesHash = await bcrypt.compare(
            password,
            encryptedPassword
        );

        // then
        t.equal(typeof encryptedPassword, 'string');
        t.notEqual(encryptedPassword, password);
        t.equal(passwordMatchesHash, true);
    });

    tap.test('sendAccountConfirmationEmail() do send mail', async (t) => {
        // given
        const sinonSandbox = sinon.createSandbox();
        t.teardown(async () => {
            sinonSandbox.restore();
        });

        const token = 'some-fake-token';
        const recipient = 'someone@mail.com';

        const sendMailStub = sinonSandbox.stub(Mailer.transporter, 'sendMail');

        // when
        await UserService.sendAccountConfirmationEmail(token, recipient);

        const mailOpts = sendMailStub.firstCall.args[0];


        //then
        t.equal(mailOpts.to, recipient);
        t.equal(typeof mailOpts.subject, 'string');
        t.ok(mailOpts.text.includes(token));
    });

    tap.test('sendPasswordResetEmail() do send mail', async (t) => {
        // given
        const sinonSandbox = sinon.createSandbox();
        t.teardown(async () => {
            sinonSandbox.restore();
        });

        const resetUrl = 'path/to/reset/url';
        const recipient = 'someone@mail.com';

        const sendMailStub = sinonSandbox.stub(Mailer.transporter, 'sendMail');

        // when
        await UserService.sendPasswordResetEmail(resetUrl, recipient);
        const mailOpts = sendMailStub.firstCall.args[0];

        //then
        t.equal(mailOpts.to, recipient);
        t.equal(typeof mailOpts.subject, 'string');
        t.ok(mailOpts.text.includes(resetUrl));
    });

}

main();
