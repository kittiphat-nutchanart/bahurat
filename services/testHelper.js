const { nanoid } = require('nanoid');
const objection = require('objection');
const Knex = require('knex');

const knexConfigs = require('../knexfile');

module.exports = class TestHelper {
    static async setupDB() {
        const knexMaster = Knex(knexConfigs.pgTesting);

        const database = `test_${nanoid()}`;

        const knexOpts = {
            ...knexConfigs.pgTesting,
            connection: {
                ...knexConfigs.pgTesting.connection,
                database
            }
        };
        const knex = Knex(knexOpts);
        objection.Model.knex(knex);

        await knexMaster.raw(`create database "${database}"`);
        await knex.initialize();
        await knex.migrate.latest();
        await knex.raw(`SET session_replication_role = replica`); // disable triggers/constraints

        async function teardown() {
            await knex.destroy();
            await knexMaster.raw(`drop database "${database}"`);
            await knexMaster.destroy();
        }

        return teardown;
    }
};
