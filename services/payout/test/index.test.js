const fs = require('fs');
const path = require('path');

const tap = require('tap');
const Knex = require('knex');
const objection = require('objection');
const { nanoid } = require('nanoid');

const knexConfigs = require('../../../knexfile.js');
const {
    User,
    Order,
    OrderLine,
    MoneyTransaction,
    Payout
} = require('../../../models');
const SampleRecords = require('../../../models/sample.js');
const PayoutService = require('..');
const TestHelper = require('../../testHelper');


async function main() {
    tap.test('updateBalances() adds new trasactions correctly', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });

        const userA = await User.query().insert({
            ...SampleRecords.User,
            username: 'uA',
            email: 'uA@email.com'
        });
        await MoneyTransaction.query().insert({
            user_id: userA.id,
            amount: '9999',
            balance: '9999',
            description: 'payout'
        });

        const userB = await User.query().insert({
            ...SampleRecords.User,
            username: 'uB',
            email: 'uB@email.com'
        });

        const userC = await User.query().insert({
            ...SampleRecords.User,
            username: 'uC',
            email: 'uC@email.com'
        });

        // old order paid > 30 days ago
        const orderA = await Order.query().insert({
            ...SampleRecords.Order,
            payment_method: 'credit_card',
            session_id: '16fd2706-8baf-433b-82eb-8c7fada847da',
            email: 'hello@mail.com',
            line_total: 100,
            shipping_cost: 0,
            total: 100,
            paid_at: '1970-01-01T00:00:00.000Z'
        });
        await OrderLine.query().insert({
            ...SampleRecords.OrderLine,
            author_id: userA.id,
            author_unit_profit: 100,
            author_unit_tax: 100,
            author_total_profit: 100,
            author_total_tax: 100,
            author_total_earnings: 9696,
            unit_price: 100,
            total_price: 100,
            base_product_id: 1,
            base_product_color_id: 1,
            base_product_second_option_id: 1,
            artwork_id: 100,
            artwork_mm_width: 100,
            artwork_mm_top: 100,
            artwork_mm_left: 100,
            quantity: 1,
            order_id: orderA.id
        });
        await OrderLine.query().insert({
            ...SampleRecords.OrderLine,
            author_id: userA.id,
            author_unit_profit: 100,
            author_unit_tax: 100,
            author_total_profit: 100,
            author_total_tax: 100,
            author_total_earnings: 1113,
            unit_price: 100,
            total_price: 100,
            base_product_id: 1,
            base_product_color_id: 1,
            base_product_second_option_id: 1,
            artwork_id: 100,
            artwork_mm_width: 100,
            artwork_mm_top: 100,
            artwork_mm_left: 100,
            quantity: 1,
            order_id: orderA.id
        });

        // another old order paid > 30 days ago
        const orderB = await Order.query().insert({
            ...SampleRecords.Order,
            payment_method: 'credit_card',
            session_id: '16fd2706-8baf-433b-82eb-8c7fada847da',
            email: 'hello@mail.com',
            line_total: 100,
            shipping_cost: 0,
            total: 100,
            paid_at: '1970-01-01T00:00:00.000Z'
        });
        await OrderLine.query().insert({
            ...SampleRecords.OrderLine,
            author_id: userA.id,
            author_unit_profit: 100,
            author_unit_tax: 100,
            author_total_profit: 100,
            author_total_tax: 100,
            author_total_earnings: 1696,
            unit_price: 100,
            total_price: 100,
            base_product_id: 1,
            base_product_color_id: 1,
            base_product_second_option_id: 1,
            artwork_id: 100,
            artwork_mm_width: 100,
            artwork_mm_top: 100,
            artwork_mm_left: 100,
            quantity: 1,
            order_id: orderB.id
        });
        await OrderLine.query().insert({
            ...SampleRecords.OrderLine,
            author_id: userB.id,
            author_unit_profit: 100,
            author_unit_tax: 100,
            author_total_profit: 100,
            author_total_tax: 100,
            author_total_earnings: 259,
            unit_price: 100,
            total_price: 100,
            base_product_id: 1,
            base_product_color_id: 1,
            base_product_second_option_id: 1,
            artwork_id: 100,
            artwork_mm_width: 100,
            artwork_mm_top: 100,
            artwork_mm_left: 100,
            quantity: 1,
            order_id: orderB.id
        });

        // new order paid just now
        const orderC = await Order.query().insert({
            ...SampleRecords.Order,
            payment_method: 'credit_card',
            session_id: '16fd2706-8baf-433b-82eb-8c7fada847da',
            email: 'hello@mail.com',
            line_total: 100,
            shipping_cost: 0,
            total: 100,
            paid_at: new Date()
        });
        await OrderLine.query().insert({
            ...SampleRecords.OrderLine,
            author_id: userA.id,
            author_unit_profit: 100,
            author_unit_tax: 100,
            author_total_profit: 100,
            author_total_tax: 100,
            author_total_earnings: 3524,
            unit_price: 100,
            total_price: 100,
            base_product_id: 1,
            base_product_color_id: 1,
            base_product_second_option_id: 1,
            artwork_id: 100,
            artwork_mm_width: 100,
            artwork_mm_top: 100,
            artwork_mm_left: 100,
            quantity: 1,
            order_id: orderC.id
        });

        // newer order not paid yet
        const orderD = await Order.query().insert({
            ...SampleRecords.Order,
            payment_method: 'credit_card',
            session_id: '16fd2706-8baf-433b-82eb-8c7fada847da',
            email: 'hello@mail.com',
            line_total: 100,
            shipping_cost: 0,
            total: 100,
            paid_at: null
        });
        await OrderLine.query().insert({
            ...SampleRecords.OrderLine,
            author_id: userA.id,
            author_unit_profit: 100,
            author_unit_tax: 100,
            author_total_profit: 100,
            author_total_tax: 100,
            author_total_earnings: 1878,
            unit_price: 100,
            total_price: 100,
            base_product_id: 1,
            base_product_color_id: 1,
            base_product_second_option_id: 1,
            artwork_id: 100,
            artwork_mm_width: 100,
            artwork_mm_top: 100,
            artwork_mm_left: 100,
            quantity: 1,
            order_id: orderD.id
        });


        // when
        await PayoutService.updateBalances();


        // then
        const userA_trx = await userA.$relatedQuery('money_transactions')
              .orderBy('id', 'desc');
        const userB_trx = await userB.$relatedQuery('money_transactions')
              .orderBy('id', 'desc');
        const userC_trx = await userC.$relatedQuery('money_transactions')
              .orderBy('id', 'desc');

        // can sum multiple orderlines
        // can deal with unpaid order
        // can deal with recently paid order < 30 days ago
        // can deal with user without previous trx
        t.equal(userA_trx.length, 2);
        t.equal(userB_trx.length, 1);
        t.equal(userC_trx.length, 0);

        t.equal(userA_trx[0].balance, userA_trx[0].amount + userA_trx[1].balance);
        t.equal(userA_trx[0].amount, 12505);
        t.equal(userA_trx[0].description, 'earnings');

        t.equal(userB_trx[0].balance, userB_trx[0].amount);
        t.equal(userB_trx[0].amount, 259);
        t.equal(userB_trx[0].description, 'earnings');
    });

    tap.test('exportPayableBalances() exports balances to given location', async (t) => {
        // given

        // set up database
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });

        const userA = await User.query().insert({
            ...SampleRecords.User,
            username: 'uA',
            email: 'uA@email.com',
            bank_account: 'a_account'
        });
        await MoneyTransaction.query().insert({
            user_id: userA.id,
            amount: 9999,
            balance: 10000,
            description: 'payout'
        });

        const userB = await User.query().insert({
            ...SampleRecords.User,
            username: 'uB',
            email: 'uB@email.com',
        });
        await MoneyTransaction.query().insert({
            user_id: userB.id,
            amount: 9900,
            balance: 9900,
            description: 'payout'
        });

        await User.query().insert({
            ...SampleRecords.User,
            username: 'uC',
            email: 'uC@email.com'
        });

        const userD = await User.query().insert({
            ...SampleRecords.User,
            username: 'uD',
            email: 'uD@email.com',
            bank_account: 'd_account'
        });
        await MoneyTransaction.query().insert({
            user_id: userD.id,
            amount: 80000,
            balance: 80000,
            description: 'payout'
        });


        // when
        const exportPath = await PayoutService.exportPayableBalances();


        // then
        const csv = fs.readFileSync(exportPath).toString()
              .trim()
              .split('\n')
              .map(l => l.split(','));

        // headers row
        t.equal(csv[0][0], 'user_id');
        t.equal(csv[0][1], 'balance (amount-to-pay)');
        t.equal(csv[0][2], 'bank_account');
        t.equal(csv[0][3], 'success');
        t.equal(csv[0][4], 'message');

        t.equal(csv[1][0], userA.id.toString());
        t.equal(csv[1][1], '10000');
        t.equal(csv[1][2], 'a_account');
        t.equal(csv[1][3], '""');
        t.equal(csv[1][4], '""');

        t.equal(csv[2][0], userD.id.toString());
        t.equal(csv[2][1], '80000');
        t.equal(csv[2][2], 'd_account');
        t.equal(csv[2][3], '""');
        t.equal(csv[2][4], '""');

        // should not include user b and c because their balances don't meet the minimum of 10000

        t.equal(csv.length, 3); // headers, userA, userD
    });

    tap.test('recordPayouts() records new payouts', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });

        const userA = await User.query().insert({
            ...SampleRecords.User,
            id: 1,
            username: 'uA',
            email: 'uA@email.com'
        });
        await MoneyTransaction.query().insert({
            user_id: userA.id,
            amount: 9999,
            balance: 9999,
            description: 'payout'
        });

        const userB = await User.query().insert({
            ...SampleRecords.User,
            id: 2,
            username: 'uB',
            email: 'uB@email.com'
        });

        await Payout.query().insert({
            user_id: userA.id,
            amount: 10000,
            bank_account: 'some_acc',
            success: true,
            message: ''
        });


        // when
        const payoutsPath = path.join(__dirname, 'fixtures/payouts.csv');
        await PayoutService.recordPayouts(payoutsPath);


        // then
        const a_transactions = await userA.$relatedQuery('money_transactions')
              .orderBy('id', 'desc');

        const b_transactions = await userB.$relatedQuery('money_transactions')
              .orderBy('id', 'desc');

        const transactions = await MoneyTransaction.query();

        const payouts = await Payout.query();

        t.equal(transactions.length, 3);

        t.equal(a_transactions[0].amount, 50000);
        t.equal(a_transactions[0].balance, 9999 - 50000);
        t.equal(a_transactions[0].description, 'payout');

        t.equal(b_transactions[0].amount, 0);
        t.equal(b_transactions[0].balance, 0);
        t.equal(b_transactions[0].description, 'payout');

        t.equal(payouts.length, 3);

        t.equal(payouts[1].user_id, userA.id);
        t.equal(payouts[1].amount, 50000);
        t.equal(payouts[1].bank_account, 'a_account');
        t.equal(payouts[1].success, true);
        t.equal(payouts[1].message, '');

        t.equal(payouts[2].user_id, userB.id);
        t.equal(payouts[2].amount, 70000);
        t.equal(payouts[2].bank_account, 'b_account');
        t.equal(payouts[2].success, false);
        t.equal(payouts[2].message, 'invalid bank account');
    });
}

main();
