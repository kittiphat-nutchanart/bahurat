const os = require('os');
const path = require('path');

const {
    MoneyTransaction,
    Payout
} = require('../../models');

module.exports = class PayoutService {
    static async updateBalances() {
        await MoneyTransaction.transaction(async (trx) => {
            const rawQuery = `
CREATE TEMP TABLE LINES_TO_GET_PAID ON
COMMIT
DROP AS
SELECT ID,
    AUTHOR_ID,
    AUTHOR_TOTAL_EARNINGS
FROM ORDER_LINES OL
WHERE ADDED_TO_MONEY_TRANSACTIONS = FALSE
    AND
        (SELECT (PAID_AT IS NOT NULL)
            AND ((NOW() - interval '30 day') > PAID_AT)
            FROM ORDERS
            WHERE ID = OL.ORDER_ID);


INSERT INTO MONEY_TRANSACTIONS(USER_ID,

                                                    DESCRIPTION,
                                                    AMOUNT,
                                                    BALANCE)
SELECT AUTHOR_ID USER_ID,
    'earnings' DESCRIPTION,
    AMOUNT,
    (COALESCE(
                                                (SELECT BALANCE
                                                    FROM MONEY_TRANSACTIONS
                                                    WHERE USER_ID = E.AUTHOR_ID
                                                    ORDER BY ID DESC
                                                    LIMIT 1), 0) + AMOUNT) BALANCE
FROM
    (SELECT AUTHOR_ID,
            SUM(AUTHOR_TOTAL_EARNINGS) AMOUNT
        FROM LINES_TO_GET_PAID
        GROUP BY AUTHOR_ID) E;


UPDATE ORDER_LINES
SET ADDED_TO_MONEY_TRANSACTIONS = TRUE
WHERE ID in
        (SELECT ID
            FROM LINES_TO_GET_PAID);
`;
            await trx.raw(rawQuery);
        });
    }

    static async exportPayableBalances() {
        const filename = 'bahurat-balances-' + new Date().toISOString();
        const exportPath = path.join(os.tmpdir(), filename);
        const knex = MoneyTransaction.knex();
        const rawQuery = `
COPY
    (SELECT S.USER_ID,
            S.BALANCE "balance (amount-to-pay)",
            U.BANK_ACCOUNT,
            '' SUCCESS,
            '' MESSAGE
        FROM
            (SELECT DISTINCT ON (USER_ID) *
                FROM MONEY_TRANSACTIONS
                ORDER BY USER_ID ASC, ID DESC) S
        LEFT JOIN USERS U ON S.USER_ID = U.ID
        WHERE S.BALANCE >= 10000) TO '${exportPath}' (FORMAT CSV,
                                                                                                                                                                                                                                                                    HEADER);
`;
        await knex.raw(rawQuery);

        return exportPath;
    }

    static async recordPayouts(payoutsPath) {
        await MoneyTransaction.transaction(async (trx) => {
            const lastPayout = await Payout.query(trx)
                  .orderBy('id', 'desc')
                  .limit(1)
                  .first();
            const rawQuery = `
COPY PAYOUTS(USER_ID,

                        AMOUNT,
                        BANK_ACCOUNT,
                        SUCCESS,
                        MESSAGE)
FROM '${payoutsPath}'
DELIMITER ',' CSV HEADER;


INSERT INTO MONEY_TRANSACTIONS(USER_ID,

                                                    DESCRIPTION,
                                                    AMOUNT,
                                                    BALANCE)
SELECT USER_ID,
    'payout' DESCRIPTION,
    ADJUSTED_AMOUNT AMOUNT,
    (COALESCE(
                                                (SELECT BALANCE
                                                    FROM MONEY_TRANSACTIONS
                                                    WHERE USER_ID = P.USER_ID
                                                    ORDER BY ID DESC
                                                    LIMIT 1), 0) - ADJUSTED_AMOUNT) BALANCE
FROM
    (SELECT *,
            (CASE SUCCESS
                                WHEN TRUE THEN AMOUNT
                                ELSE 0
                END) ADJUSTED_AMOUNT
        FROM PAYOUTS
        ${lastPayout ? ('WHERE ID > ' + lastPayout.id) : ''}) P;
`;
            await trx.raw(rawQuery);
        });
    }
};
