const tap = require('tap');
const Knex = require('knex');
const objection = require('objection');

const knexConfigs = require('../../../knexfile');
const SampleRecords = require('../../../models/sample');
const { Artwork, Tag, ArtworkTag } = require('../../../models');
const Auth = require('../../../authentication');
const ArtworkService = require('..');
const TestHelper = require('../../testHelper');


async function main() {

    tap.test('updateTags()', async (t) => {
        // given
        const teardown = await TestHelper.setupDB();
        t.teardown(async () => {
            await teardown();
        });

        const artwork = await Artwork.query().insert({
            ...SampleRecords.Artwork,
            author_id: 1,
            title: 'artwork_title',
            image_id: 1
        });

        // when
        await Artwork.transaction(async (trx) => {
            const tags = ['awesome'];
            await ArtworkService.updateTags(artwork.id, tags, trx);
        });

        const tag = await Tag.query().first();
        const artworkTag = await ArtworkTag.query().first();

        //then
        t.equal(tag.name, 'awesome');
        t.equal(artworkTag.artwork_id, artwork.id);
        t.equal(artworkTag.tag_id, tag.id);
    });
}


main();
