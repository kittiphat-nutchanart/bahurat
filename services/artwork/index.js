const R = require('ramda');

const { Tag, ArtworkTag } = require('../../models');

module.exports = class ArtworkService {

    /**
     * set an artwork's tags to updateTags
     * @param {number} artworkId - artwork's id
     * @param {string[]} updateTags - array of tags to set to. use empty array to delete all tags
     * @param {Object} trx - knex/objection transaction object
     */
    static async updateTags(artworkId, updateTags, trx) {
        await ArtworkTag.query(trx)
            .delete()
            .where('artwork_id', artworkId);

        if (updateTags.length) {
            let tags = await Tag.query(trx)
                .whereIn('name', updateTags);

            const _tags = tags.map(i => i.name);
            const newTags = R.difference(updateTags, _tags);
            if (newTags.length) {
                const _newTags = newTags.map(i => ({ name: i }));
                await Tag.query(trx).insert(_newTags);
                tags = await Tag.query(trx)
                    .whereIn('name', updateTags);
            }

            const artworksTags = tags.map(i => {
                return {
                    artwork_id: artworkId,
                    tag_id: i.id
                };
            });
            await ArtworkTag.query(trx).insert(artworksTags);
        }
    }
};
